var faqs = null;
var pont = [".", "!", "?", ";", ":", ","];

function initFaqs(target) {
    servGet(target, [], popularfaqs);
}

function parse_faq_links(s) {
    return s.replace(/\[([^|]+)\|([^\]]+)\]/gi, "<a target=\"#$1\" "
        + "href=\"javascript:void(0)\" onclick=\"irfaq_nome('$1')\">$2</a>");
}

function popularfaqs(json) {
    faqs = json;
    categorias = [];
    for (var index in faqs) {
        var faq = faqs[index];
        var categoria = faq["categoria"];
        faqs[index]["texto"] = parse_faq_links(faqs[index]["texto"]);
        var catIndex = categorias.indexOf(faq["categoria"]);
        if (catIndex < 0) {
            catIndex = categorias.length;
            categorias.push(faq["categoria"]);
            var dp = ":";
            if (pont.indexOf(categoria[categoria.length-1]) > -1) {
                dp = "";
            }
            $("#cats").append("<b>" + faq["categoria"] + dp + "</b><br><span "
                + "id=\"cat_" + catIndex + "\"></span><br>");
        }
        $("#cat_" + catIndex).append("<a href=\"javascript:void(0)\" "
            + "onclick=\"irfaq(" + index + ")\">" + faq["titulo"]
                + "</a><br>");
    }
    irfaq_nome(window.location.hash.substr(1));
}

function irfaq(index) {
    faq = faqs[index];
    window.location.hash = faq["fname"];
    $("#inicial").hide();
    $("#holder").hide();
    $("#titulo").text(faq["titulo"]);
    $("#conteudo").html(faq["texto"]);
    $("#holder").fadeIn();
}

function irfaq_nome(nome) {
    for (var index in faqs) {
        var faq = faqs[index];
        if (faq["fname"] == nome) {
            irfaq(index);
        }
    }
}

function voltarInicial() {
    window.location.hash = "";
    $("#holder").hide();
    $("#inicial").fadeIn();
}
