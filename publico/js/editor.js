/* requer:
 *  - jquery.hotkeys.js
 *  - brunoml.js
 *  - editor.css
 *  - resumo.css
 */ 
 
var editor;
var textarea;
var toolbar;
var foco;

String.prototype.insertAt = function(index, string) {
    return this.substr(0, index) + string + this.substr(index);
}

function initEditor(elem) {
    editor = elem;
    editor.append("<div id=\"toolbar\"></div>");
    toolbar = $("#toolbar");
    editor.append("<div contenteditable=\"true\" id=\"editor_textarea\""
        + " class=\"form-control\" placeholder=\"Digite aqui\"></div>");
    textarea = $("#editor_textarea");
    addButton("Negrito", "bold", "bold", "Ctrl+B");
    addButton("Itálico", "italic", "italic", "Ctrl+I");
    addButton("Sublinhado", "text-color", "underline", "Ctrl+U");
    addButton("Superscrito", "superscript", "superscript", "Ctrl+S");
    addButton("Subscrito", "subscript", "subscript", "Ctrl+Shift+S");
    //addButton("Texto grande", "text-size", "increaseFontSize", "Ctrl+G");
    //addButton("Texto pequeno", "text-size rev", "decreaseFontSize", "Ctrl+P")
    addButton("Imagem", "picture", imagem, "Ctrl+J");
    addButton("Link", "link", linkify, "Ctrl+L");
    addButton("Lista", "option-vertical", "insertUnorderedList",
        "Ctrl+H");
    addButton("Lista numerada", "option-vertical", "insertOrderedList",
        "Ctrl+G");
    addButton("Remover formatação", "erase", "removeFormat", "Ctrl+R");
    textarea.on("change", saveText);
    textarea.on("input", saveText);
    toolbar.append("<br><br><small>Você pode colar diretamente "
        + "textos do Word e outros programas, mas sugerimos escrever diretamente aqui.<br>"
        + "Se tiver feito imagens próprias, coloque-as no <a href=\"//imgur"
        + ".com\" target=\"blank\">Imgur</a> ou algum site assim.<br>"
        + "Pro pessoal antigo... os códigos antigos continuam funcionando.<br>"
        + "Tudo que você digitar aqui fica salvo, você pode sair e voltar."
    );
    var saved = safeLS("editor_save");
    if (saved) {
        textarea.html(saved);
    }
    //textarea.on("input", textChange);
    //textarea.on("change", textChange);
}

function saveText() {
    safeLS("editor_save", textarea.html()); 
}

function eventEater(ev) {
    ev.preventDefault();
    ev.stopPropagation();
}

function addButton(texto, glyphicon, command, shortcut) {
    var icon = "<span class=\"glyphicon glyphicon-" + glyphicon + "\"></span><br>";
    var keys = ":<br><span class=\"shortcut\">" + shortcut + "</span>";
    var func = function(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        document.execCommand(command);
        console.log("calling", command);
    };
    if (typeof(command) == "function") func = function(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        command(ev);
    }
    var btn = $("<span></span>").html(icon + texto + keys)
        .toggleClass("editor_btn").on("mousedown", func).on("focus", eventEater);
    toolbar.append(btn);
    textarea.bind("keydown", shortcut, func);
}

function insert(text) {
    document.execCommand("insertHTML", false, text);
}

function imagem() {
    var url = prompt("Digite a URL da imagem:");
    if (!url) return;
    document.execCommand("insertImage", false, encodeURI(url));
}

function linkify() {
    var url = prompt("Digite a URL do link:");
    if (!url) return;
    document.execCommand("createLink", false, encodeURI(url));
}

function getHTML() {
    return textarea.html();
}

function setHTML(code) {
    textarea.html(code);
}
