String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp("\\{" + i + "\\}", "gm"), arguments[i]);
    }
    return s;
};

nicegreen = "#319C0C";

function randomFrom(array) {
    return array[Math.floor(Math.random()*array.length)];
}

function setCookie(cname, cvalue, exdays) {
    if (exdays === undefined) {
        exdays = 360;
    }
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    var finalcookie = cname + "=" + cvalue + ";domain=" + location.host + ";path=/;" + expires;
    console.log("cookie \"" + cname + "\" setado para \"" + cvalue + "\"");
    document.cookie = finalcookie
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(";");
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
    setCookie(cname, "", -1);
}

function getToken() {
    return getCookie("mendelyx_token_aluno");
}

function deleteToken() {
    deleteCookie("mendelyx_token_aluno");
}

base = "//" + location.host + "/mendelyx_serv/";

function servGet(servico, urlargs, callback) {
    var url = base + "get/" + servico + "/" + encodeURI(urlargs.join(","));
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 500) {
                var crash_report = {
                    "Classe do erro": "Erro 500 ao executar requisição GET ("
                        + servico + ")",
                    "Dados da requisição": {
                        "Serviço requisitado": servico,
                        "Argumentos da requisição": urlargs,
                        "Callback da requisição": callback.name
                    }
                };
                oops(crash_report);
                return;
            }
            json = JSON.parse(this.responseText);
            callback(json);
            //console.log(this.responseText);
        }
    }
    request.open("GET", url);
    request.send(null);
}

function servPost(servico, args, callback) {
    var url = base + "post/" + servico + "/";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 500) {
                var crash_report = {
                    "Classe do erro": "Erro 500 ao executar requisição POST ("
                        + servico + ")",
                    "Dados da requisição": {
                        "Serviço requisitado": servico,
                        "Argumentos da requisição": args,
                        "Callback da requisição": callback.name
                    }
                };
                if (servico == "crash") {
                    document.write("Parece que um erro foi encontrado ao... "
                        + "reportar o erro. Avise o Bruno imediatamente. "
                            + "Inclua o relatório abaixo.<br><br>"
                            + JSON.stringify(crash_report, null, 4));
                    return;
                }
                oops(crash_report);
                return;
            }
            json = JSON.parse(this.responseText);
            callback(json);
            //console.log(this.responseText);
        }
    }
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type",
        "application/x-www-form-urlencoded; charset=UTF-8");
    var poststr = "";
    for (var key in args) {
        if (!args.hasOwnProperty(key)) continue;
        poststr += key + "=" + encodeURIComponent(args[key]) + "&";
    }
    request.send(encodeURI(poststr));
}

function oops(crash_report) {
    var specs = {
        "Data e hora do erro": (new Date()).toString(),
        "Cookie": document.cookie,
        "URL": location.href,
        "UserAgent": navigator.userAgent
    };
    crash_report["Contexto do erro"] = specs;
    var full_report_json = JSON.stringify(crash_report, null, 4);
    servPost("crash", {"crash_json": full_report_json}, crashed);
}

function crashed(json) {
    $("body").hide();
    document.body.innerHTML = json["crash_template"];
    document.title = "Deu ruim...";
    $("body").css("background", "url(\"/img/vish.jpg\") center no-repeat");
    if (json["emailed"])
        $("#emailed").show();
    else
        $("#nem").show();
    $("#crash").val(json["pretty_error"]);
    $("body").fadeIn();
}

function debuggerCallback(json) {
    console.log(json);
}

function debugGet(servico, urlargs) {
    servGet(servico, urlargs, debuggerCallback);
}

function debugPost(servico, args) {
    servPost(servico, args, debuggerCallback);
}

function morreu(json) {
    alert("ATENÇÃO!\n\nParece que o servidor do site está fora do ar!"
        + "\n\nVerifique sua conexão!\n\nSe o erro persistir, avise o Bruno!");
}

/*try {
    var url = base + "get/alive/";
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                var json = JSON.parse(this.responseText);
                if (!json["alive"]) {
                    morreu();
                }
            } else {
                morreu();
            }
        }
    }
    request.open("GET", url);
    request.send(null);
} catch(err) {
    morreu();
}*/

function slowRedir(url) {
    setTimeout(function(){location.assign(url)}, 1000);
}

function nomeSala(id) {
    if (id === undefined)
        id = getCookie("ano") + getCookie("sala");
    return id[0] + "º " + id[1];
}

validsalas = [
    "1A", "1B", "1C", "1D", "1E", "1F", "1G", "1H", "1I", "1J",
    "2A", "2B", "2C", "2D", "2E", "2F", "2G", "2H",
    "3A", "3B", "3C", "3D", "3E", "3F", "3G"
];

dias_semana = [
    "domingo",
    "segunda",
    "terça",
    "quarta",
    "quinta",
    "sexta",
    "sábado"
];

meses = [
    "janeiro",
    "fevereiro",
    "março",
    "abril",
    "maio",
    "junho",
    "julho",
    "agosto",
    "setembro",
    "outubro",
    "novembro",
    "dezembro"
];

function salaExists(id) {
    return validsalas.indexOf(id) > -1;
}

function err(msg) {
    $("#success").hide();
    $("#err").hide();
    $("#err").html(msg);
    $("#err").fadeIn();
}

function success(msg) {
    $("#err").hide();
    $("#success").hide();
    $("#success").html(msg);
    $("#success").fadeIn();
}

function padzeroes(n) {
    if (n < 10) {
        return "0" + n;
    }
    return n;
}

function toIso(date) {
    var dia = padzeroes(date.getDate());
    var mes = padzeroes(date.getMonth()+1);
    var ano = date.getFullYear();
    return [ano, mes, dia].join("-");
}

function isoToDate(iso) {
    return new Date(iso.replace(/-/g, "/"));
}

function dateToBarras(date) {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
}

function fullISOToDate(str) {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    return new Date(Date.parse(str) + tzoffset)
}

function loginAqui(depth) {
    var relative = "login_aluno/#goto:" + encodeURI(location.pathname + location.hash);
    for (var i = 0; i < depth; i++) relative = "../" + relative;
    location.assign(relative);
}

function loginQuickie(depth) {
    var relative = "login_aluno/login_quickie.html";
    for (var i = 0; i < depth; i++) relative = "../" + relative;
    window.open(relative);
}

function logoff() {
    deleteCookie("mendelyx_token_aluno");
    deleteCookie("meu_id");
    location.reload();
}

function checkLoggedIn(callback) {
    var token = getToken();
    if (token) {
        servGet("token_check", [token], callback);
    } else {
        callback({
            "status": "FAILED",
            "message": "Faça login!"
        });
    }
}

function firstLoginCheck(response) {
    if (response === false) {
        deleteAlunoCookies();
        $("#loginbutton").show();
        $("#logoffbutton").hide();
    } else {
        if (response["status"] == "OK") {
            $("#logoffbutton").show();
            $("#loginbutton").hide();
            setCookie("meu_id", json["seu_id"]);
            setCookie("meu_numero", json["seu_numero"]);
            setCookie("meu_ano", json["seu_ano"]);
            setCookie("minha_sala", json["sua_sala"]);
            setCookie("ano", json["seu_ano"]);
            setCookie("sala", json["sua_sala"]);
            setCookie("meu_nome", json["seu_nome"]);
        } else {
            deleteAlunoCookies();
            $("#loginbutton").show();
            $("#logoffbutton").hide();
        }
    }
}

function deleteAlunoCookies() {
    deleteToken();
    deleteCookie("meu_id");
    deleteCookie("meu_numero");
}

function safeLS(index, value) {
    index = "mendelyx_" + index;
    if (localStorage) {
        if (value !== undefined) localStorage[index] = value;
        return localStorage[index];
    }
}

var waitingForLogin = false;
function doLogin(n) {
    waitingForLogin = true;
    window.addEventListener("focus", function() {
        if (waitingForLogin && getToken()) {
            location.reload();
        };
    });
    loginQuickie(n);
}

function getMateriaById(materias, id) {
    for (var index in materias) {
        if (materias[index]["id_materia"] == id) {
            return materias[index];
        }
    }
    return false;
}

function tempoMaximo(meiasprovas, ano, totdias) {
    if (meiasprovas == 5) return "11:20";
    if (ano == 1) {
        if (meiasprovas == 4) return "11:05";
    } else {
        if (meiasprovas == 4) return "10:35";
    }
    if (totdias == 5 && meiasprovas == 3) {
        return "9:20";
    }
    return "?????";
}

function scrollTo(elem_id) {
    $("html, body").animate({
        scrollTop: $("#" + elem_id).offset().top
    }, 1000);
}

function scrollToBottom() {
    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
}

function escapeHTML(str) {
    return str.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
}

function parse_links(str) {
    return str.replace(/\[([^|]+)\|([^\]]+)\]/gi, "$1")
        .replace(/(http(?:s|):\/\/([^\/]+)[^ \n]*)/gi,
        "<a target=\"_blank\" href=\"$1\">[Link em $2]</a>");
}

function comparadorEnvio(a, b) {
    return Date.parse(b["enviado_em"]) - Date.parse(a["enviado_em"]);
}

function comparadorVotos(a, b) {
    return b["votos"] - a["votos"];
}

function stripFullISO(str) {
    var reg = /(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)/;
    var arr = reg.exec(str);
    return {
        "ano": arr[1],
        "mes": arr[2],
        "dia": arr[3],
        "horas": arr[4],
        "minutos": arr[5],
        "segundos": arr[6],
    }
}

function timestr(iso) {
    var obj = stripFullISO(iso);
    return obj["dia"] + "/" + obj["mes"] + "/" + obj["ano"] + ", às "
        + obj["horas"] + ":" + obj["minutos"];
}

function noop() {}

function resetFormElement(e) {
    e.wrap("<form>").closest("form").get(0).reset();
    e.unwrap();
    if (e.stopPropagation)
        e.stopPropagation();
    if (e.preventDefault)
        e.preventDefault();
}

function makeBase64URLSafe(str) {
    return str.replace(/\+/g, "-").replace(/\//g, "_");
}

function jslink(func, text) {
    return "<a href=\"javascript:void(0)\" onclick=\"" + func + "\">"
        + text + "</a>";
}

function mkoption(val, text) {
    return "<option value=\"" + val + "\">" + text + "</option>";
}

function motd(message) {
    if (!motd) return false;
    $($("h1")[0]).after("<i class=\"niver\">" + parse_links(message) + "<br></i>");
}

function basic_html(title, contents) {
    return "<html><head><meta charset=\"utf-8\"><title>" + title + "</title>"
        + "</head><body>" + contents + "</body></html>";
}

function sn(boo) {
    if (boo) return "Sim"
    else return "Não"
}

servGet("motd", [], motd);

var acheck = new Date();
if (acheck.getDate() == 18 && acheck.getMonth() == 2) {
    motd("Feliz aniversário, Bruno!");
    $(".agradecimento").text("Feliz aniversário, Bruno!");
}

checkLoggedIn(firstLoginCheck);
