function brunoml_to_html(code, reverse) {
    var others = [
        ["[table]", "<table class=\"table restable\">"],
        ["[/table]", "</table>"],
        ["[/cor]", "</span>"],
        ["[/big]", "</div>"],
        ["[/code]", "</span>"],
        ["[big]", "<div class=\"oldbig\">"],
        ["[code]", "<span class=\"mono\">"],
        ["[$1|$2]", "<a target=\"_blank\" href=\"$1\">$2</a>"],
        ["[cor:$1]", "<span class=\"colored\" style=\"color: $1;\">"],
        [
            "[imagem:$1]",
            "<a target=\"_blank\" title=\"Clique para ver o tamanho completo\""
                + " href=\"$1\"><img src=\"$1\"></a>"
        ],
        ["\\{l\\}", "ℓ"],
        ["\\{g\\}", "<sup>↗</sup>"]
    ];

    var tags = ["b", "i", "u", "tr", "td", "sub", "sup", "hr"];

    if (reverse == undefined) reverse = false;
    for (var index in others) {
        var duple = others[index];
        if (reverse) duple.reverse();
        duple[0] = duple[0].replace(/([[\]|\/])/g, "\\$1")
            .replace(/\$\d/g, "([^\\]]+)");
        var regex = new RegExp(duple[0], "ig");
        code = code.replace(regex, duple[1])
    }
    for (var index in tags) {
        var tag = tags[index];
        var brackets = [["\\[", "\\]"], ["<", ">"]];
        if (reverse) brackets.reverse();
        var regex = new RegExp(brackets[0][0] + "(\\/|)" + tag
            + brackets[0][1], "ig");
        code = code.replace(regex, brackets[1][0] + "$1" + tag
            + brackets[1][1]);
    }
    return code;
}

function html_to_brunoml(code) {
    return brunoml_to_html(code, true);
}
