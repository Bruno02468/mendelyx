function ocultarSeletivo(json) {
    for (index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var inicial = json[index];
        var id_link = inicial["id_link"];
        var link = inicial["link"];
        var visivel = !!inicial["visivel"];
        if (!visivel && $("#" + link)) {
            $("#" + link).hide();
        }
    }
    $(".links").show();
}

servGet("get_iniciais", [], ocultarSeletivo);
