var professores;
servGet("get_professores", [], tenhoProfessores);

function tenhoProfessores(json) {
    professores = json;
    servGet("get_all_materias", [], tenhoMaterias);
}

var materias;
function tenhoMaterias(json) {
    materias = json;
    var ano = 0;
    if (getCookie("ano")) {
        ano = getCookie("ano");
    }
    $("#ano").val(ano);
    updateAvisos(ano);
}

var avisos;

function updateAvisos(ano) {
    servGet("get_avisos_ano", [ano], tenhoAvisos);
}

function noms(id) {
    var ret = [null, null];
    for (var index in professores) {
        var professor = professores[index];
        if (professor["id_professor"] == id) {
            ret[0] = professor["nome"];
            ret[1] = professor["id_materia"];
            break;
        }
    }
    for (var index in materias) {
        var materia = materias[index];
        if (materia["id_materia"] == ret[1]) {
            ret[1] = materia["nome"];
            break;
        }
    }
    return ret;
}

function nomeMaterias(id) {
}

function tenhoAvisos(json) {
    avisos = json;
    $("#avisos").hide();
    $("#avisos").html("");
    for (var index in avisos) {
        var aviso = avisos[index];
        var n = noms(aviso["id_professor"]);
        var nomeprof = n[0];
        var nomemat = n[1];
        var anos = "todos os anos";
        if (aviso["ano"] != 0) {
            anos = "o " + aviso["ano"] + "º ano";
        }
        var heading = "<div class=\"panel-heading\"><h3 class=\"panel-title "
            + "licao-normal-text\">" + aviso["titulo"]
            + "<span class=\"paradia\"><br><br>Professor(a) " + nomeprof
            + " (" + nomemat + ") ~ para " + anos + "</span></h3></div>";
        var arquivo = "";
        if (aviso["id_arquivo"]) {
            arquivo = "<br><br><div class=\"aviso-arquivo\" id=\"arquivo_"
                + aviso["id_arquivo"] + "\">Carregando arquivo...</div>";
        }
        var pbody = "<div class=\"panel-body\">"
            + brunoml_to_html(parse_links(aviso["texto"])) + arquivo + "</div>";
        var component = "<div id=\"aviso_" + aviso["id_aviso"] + "\""
            + "class=\"licao panel panel-default\">" + heading + pbody
            + "</div><br><br>";
        $("#avisos").append(component);
        if (aviso["id_arquivo"]) {
            servGet("get_arquivo", [aviso["id_arquivo"]], tenhoArquivo);
        }
    }
    if (avisos.length == 0) {
        $("#avisos").html("<p class=\"lead trunc\">Parece que nada foi"
            + " adicionado por enquanto...</p><br><br>");
    }
    $("#avisos").fadeIn();
}

function tenhoArquivo(json) {
    if (json["status"] == "OK") {
        var id_arquivo = json["id_arquivo"];
        var hash = json["hash"];
        var filename = json["filename"];
        var newhtml = "<b>Inclui arquivo: <i>" + filename + ".<br>Clique <a "
            + "href=\"../uploads/" + hash + "/" + encodeURIComponent(filename)
            + "\" target=\"_blank\">aqui</a> para ver/baixar.</b>";
        $("#arquivo_" + id_arquivo).html(newhtml);
    }
}
