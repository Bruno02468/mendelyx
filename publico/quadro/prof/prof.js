var shownAcao;
var shownNome;
function showAcao(nome) {
    if (shownAcao)
        shownAcao.hide();
    if (shownNome == nome) {
        shownAcao = null;
        shownNome = null;
        return false;
    }
    shownAcao = $("#" + nome);
    shownNome = nome;
    shownAcao.fadeIn();
}

var professores = {};
servGet("get_professores", [], tenhoProfessores);

function tenhoProfessores(json) {
    professores = json;
}

function updateNomes() {
    var typed = $("#nome").val().toLowerCase();
    if (typed.length < 4) return false;
    var links = $("#profLinks");
    links.hide();
    links.html("");
    for (var key in professores) {
        if (!professores.hasOwnProperty(key)) continue;
        var professor = professores[key];
        if (professor["nome"].toLowerCase().indexOf(typed) > -1) {
            var component = "<a href=\"javascript:void(0)\" onclick=\"setProf("
                + "this)\">" + professor["nome"] + "</a>";
            links.append(component);
        }
    }
    links.fadeIn();
}

var nome;
var senha;
function setProf(elem) {
    if (!nome) $("#passHolder").fadeIn();
    nome = elem.innerText;
    $("#senha").focus();
}

function profLogin() {
    senha = $("#senha").val();
    var args = {
        "nome": nome,
        "senha": senha
    }
    servPost("check_professor", args, gotLogin);
    return false;
}

function gotLogin(json) {
    if (json["status"] == "OK") {
        $("#notlogado").hide();
        $("#logado").fadeIn();
        updateAvisos();
        $("#ano").val("0");
        err("");
    } else {
        err(json["message"]);
    }
}

var fname;
var ftype;

function novoAviso() {
    var args = {
        "nome": nome,
        "senha": senha,
        "titulo": $("#titulo").val(),
        "texto": $("#texto").val(),
        "tem_arquivo": false,
        "arquivo": null,
        "ano": $("#ano").val()
    };
    if ($("#file")[0].files[0]) {
        var file = $("#file")[0].files[0];
        fname = file["name"];
        ftype = file["type"];
        var reader = new FileReader();
        reader.onload = function(e) {
            var data = e.target.result;
            enviarComArquivo(args, data);
        };
        reader.readAsBinaryString(file);
    } else {
        servPost("criar_aviso", args, enviei);
    }
    return false;
}

function enviarComArquivo(args, dados) {
    args["tem_arquivo"] = true;
    args["arquivo"] = JSON.stringify({
        "name": fname,
        "type": ftype,
        "contents": makeBase64URLSafe(btoa(dados))
    });
    servPost("criar_aviso", args, enviei);
}

function enviei() {
    if (json["status"] == "OK") {
        success("Aviso criado com sucesso!");
        $("#titulo").val("");
        $("#texto").val("");
        resetFormElement($("#file"));
    } else {
        err(json["message"]);
    }
}

function updateAvisos() {
    var args = {
        "nome": nome,
        "senha": senha
    };
    servPost("get_meus_avisos", args, tenhoAvisos);
}

var avisos;
function tenhoAvisos(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        $("#avisos").html("");
        avisos = JSON.parse(json["avisos"]);
        for (var index in avisos) {
            var aviso = avisos[index];
            var titulo = aviso["titulo"];
            var id = aviso["id_aviso"];
            var editlink = jslink("showEditAviso(" + id + ")", "(Editar)");
            var deletelink = jslink("deleteAviso(" + id + ")", "(Deletar)");
            $("#avisos").append("<li class=\"list-group-item\">\"" + titulo
                + "\" " + editlink + " " + deletelink + "</li>");
        }
    }
}

function cancelarEdit() {
    $("#editarAviso").hide();
    $("#titulo_edit").val("");
    $("#texto_edit").val("");
    $("#ano_edit").val("0");
}

var editando = null;
function showEditAviso(id_aviso) {
    editando = null;
    cancelarEdit();
    for (var index in avisos) {
        var aviso = avisos[index];
        if (aviso["id_aviso"] == id_aviso) {
            editando = id_aviso;
            $("#titulo_edit").val(aviso["titulo"]);
            $("#texto_edit").val(aviso["texto"]);
            $("#ano_edit").val(aviso["ano"]);
            $("#editarAviso").fadeIn();
        }
    }
}

function enviarEdit() {
    if (!editando) return false;
    var args = {
        "nome": nome,
        "senha": senha,
        "id_aviso": editando,
        "titulo": $("#titulo_edit").val(),
        "texto": $("#texto_edit").val(),
        "ano": $("#ano_edit").val()
    };
    servPost("editar_aviso", args, envieiEdit);
}

function envieiEdit() {
    if (json["status"] == "OK") {
        success("Mudanças salvas!");
        cancelarEdit();
        updateAvisos();
    } else {
        err(json["message"]);
    }
}

function deleteAviso(id) {
    var args = {
        "nome": nome,
        "senha": senha,
        "id_aviso": id
    };
    servPost("deletar_aviso", args, envieiEdit);
}
