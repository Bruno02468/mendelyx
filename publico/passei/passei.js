servGet("ano_diretor", [], fillSelects);

var inputs = ["matricula", "turma", "chamada", "full_sala"];

var ano_diretor;
function fillSelects(json) {
    ano_diretor = json["ano_diretor"];
    if (ano_diretor == 0) {
        location.assign("..");
    }
    var podem = [
        [3, ano_diretor-1],
        [2, ano_diretor-1],
        [1, ano_diretor-1],
        [3, ano_diretor-2],
        [3, ano_diretor-3]
    ];
    for (var index in podem) {
        var a = podem[index][0];
        var d = podem[index][1];
        $("#turma").append("<option value=\"" + a + "_" + d + "\">"
            + a + "º ano de " + d + "</option>");
    }
    $("#mda_sel").append("<option value=\"v\">Vestibular " + ano_diretor 
        + "</option>");
    $("#mda_sel").append("<option value=\"mda\">Meio de ano " + (ano_diretor-1) 
        + "</option>");
    update_salas();
    if (getCookie("aprov_valido")) {
        var cred = JSON.parse(getCookie("aprov_valido"));
        for (var key in cred) {
            if (!cred.hasOwnProperty(key)) continue;
            $("#" + key).val(cred[key]);
        }
    }
}

function update_salas(k) {
    var ano = $("#turma").val().split("_")[0];
    $("#full_sala").html("");
    for (var index in validsalas) {
        var sala = validsalas[index];
        if (sala[0] != ano) continue;
        $("#full_sala").append("<option value=\"" + sala + "\">" + nomeSala(sala)
            + "</option>");
    }
}

var categorias = [
    "Saúde e Bem Estar",
    "Engenharia e Produção",
    "Ciências Sociais e Humanas",
    "Administração e Negócios",
    "Comunicação e Informação",
    "Ciências Exatas e Informática",
    "Artes e Design",
    "Ciências Biológicas e da Terra",
    "Carreira Militar"
];

for (var i in categorias) {
    $("#categoria").append("<option value=\"" + categorias[i] + "\">"
        + categorias[i] + "</option>");
}

var credenciais = null;
var credenciais_json = null;
function verificarAluno() {
    credenciais = {
        "matricula": $("#matricula").val(),
        "em": $("#turma").val().split("_")[1],
        "full_sala": $("#full_sala").val(),
        "chamada": $("#chamada").val()
    };
    for (var key in credenciais) {
        if (!credenciais.hasOwnProperty(key)) continue;
        if (!credenciais[key]) {
            err("Não deixe nenhum campo em branco!");
            return false;
        }
    }
    credenciais_json = JSON.stringify(credenciais)
    var args = {
        "credenciais_json": credenciais_json
    };
    servPost("verificar_aluno_aprov", args, alunoVerificado);
    return false;
}


function alunoVerificado(json) {
    if (json["status"] == "OK") {
        $("#num").text(json["seu_numero"]);
        $("#ano").text(json["seu_ano"]);
        $("#sala").text(json["sua_sala"]);
        $("#em").text(json["seu_em"]);
        if (json["seu_nome_corrigido"]) {
            $("#correc_nome").val(json["seu_nome_corrigido"]);
        } else {
            $("#correc_nome").val(json["seu_nome"]);
        }
        $("#nao_logado").hide();
        $("#corrigir_nome").fadeIn();
        aprovacoes = json["aprovacoes"];
        resetarLista(aprovacoes);
        credenciais_json = JSON.stringify(credenciais);
        var datas = {};
        for (var index in inputs) {
            var name = inputs[index];
            datas[name] = $("#" + name).val();
        }
        setCookie("aprov_valido", JSON.stringify(datas));
        err("");
    } else {
        deleteCookie("aprov_valido");
        err(json["message"]);
    }
}

function sair() {
    deleteCookie("aprov_valido");
    location.reload();
}

function corrigiNome() {
    var args = {
        "credenciais_json": credenciais_json,
        "correcao": $("#correc_nome").val()
    };
    servPost("enviar_correcao_nome", args, correcaoFeita);
    return false;
}

function correcaoFeita(json) {
    if (json["status"] == "OK") {
        $("#nome").text($("#correc_nome").val());
        $("#corrigir_nome").hide();
        $("#full_logado").fadeIn();
        $("#sair").fadeIn();;
        err("");
    } else {
        err(json["message"]);
    }
}

/*function makeAprovacaoLine(aprovacao) {
    var td_uni = "<td>" + aprovacao["nome_uni"] + "</td>";
    var td_curso = "<td>" + aprovacao["nome_curso"] + "</td>";
    var td_col = "<td>" + aprovacao["rank"] + "º</td>";
    if (aprovacao["rank"] == 0) {
        td_col = "<td>N/A</td>";
    }
    var td_pub = "<td>" + (aprovacao["publica"] ? "Sim" : "Não") + "</td>";
    var td_trei = "<td>" + (aprovacao["treineiro"] ? "Sim" : "Não")
        + "</td>";
    var td_mda = "<td>" + (aprovacao["meio_de_ano"] ? "Sim" : "Não") + "</td>";
    var td_sisu = "<td>" + (aprovacao["sisu"] ? "Sim" : "Não") + "</td>";
    var td_edit = "<td><span class=\"glyphicon glyphicon-pencil imgbtn\""
        + "onclick=\"editar(" + aprovacao["id_aprovacao"] + ")\"></span></td>";
    var td_remove = "<td><span class=\"glyphicon glyphicon-trash imgbtn\""
        + "onclick=\"remask(" + aprovacao["id_aprovacao"] + ")\"></span></td>";
    return td_uni + td_curso + td_col + td_pub + td_trei + td_mda + td_sisu
        + td_edit + td_remove;
}*/


function makeAprovacaoBox(aprovacao) {
    var makeEditButton = function(att, texto, type) {
        var typer = "";
        if (type) {
            typer = " list-group-item-" + type;
        }
        return "<button type=\"button\" class=\"list-group-item" + typer
            + "\" onclick=\""
            + "editarAtributo(" + aprovacao["id_aprovacao"] + ", '" + att
            + "', this)\">" + texto + "</button>";
    }
    var uni_btn = makeEditButton("nome_uni", aprovacao["nome_uni"], "success");
    var curso_btn = makeEditButton("nome_curso", aprovacao["nome_curso"], "success");
    var categoria_btn = makeEditButton("categoria", aprovacao["categoria"], "info");
    var col = "[Sem colocação]";
    if (aprovacao["rank"] != 0) {
        col = aprovacao["rank"] + "º";
    } 
    var colocacao_btn = makeEditButton("rank", col, "info");
    var pub_btn = makeEditButton("publica", aprovacao["publica"]
        ? "Pública" : "Particular");
    var mda_btn = makeEditButton("meio_de_ano", "Meio de ano: "
        + sn(aprovacao["meio_de_ano"]));
    var sisu_btn = makeEditButton("sisu", "Via Enem: " + sn(aprovacao["sisu"]));
    //var trei_btn = makeEditButton("treineiro", "Treineiro: " + sn(aprovacao["treineiro"]));
    var trei_btn = "";
    var remove_btn = "<button type=\"button\" class=\"list-group-item "
        + "list-group-item-danger\" onclick=\"remask(" +
        aprovacao["id_aprovacao"] + ")\">Remover</button>";
    return uni_btn + curso_btn + categoria_btn + colocacao_btn + pub_btn
        + mda_btn + sisu_btn + trei_btn + remove_btn; 
}

var lista = $("#lista");
var aprovacoes = null;
function resetarLista(json) {
    lista.html("");
    aprovacoes = json;
    aprovacoes.reverse();
    for (var index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var aprovacao = json[index];
        lista.append("<div class=\"list-group\" id=\"aprovacao_"
                + aprovacao["id_aprovacao"] + "\">"
                + makeAprovacaoBox(aprovacao)
                +"</div><br>");
    }
    $("#nome_uni").val("");
    $("#nome_curso").val("");
    $(":checkbox").prop("checked", false);
    $("#rank").val(0);
}

var esperandoAdd = false;
function enviarAprovacao() {
    var args = {
        "credenciais_json": credenciais_json,
        "nome_uni": $("#nome_uni").val(),
        "nome_curso": $("#nome_curso").val(),
        "publica": 0+$("#publica").is(":checked"),
        "treineiro": 0,
        "rank": $("#rank").val(),
        "meio_de_ano": 0+($("#mda_sel").val() == "mda"),
        "sisu": 0+$("#sisu").is(":checked"),
        "categoria": $("#categoria").val()
    };
    esperandoAdd = true;
    servPost("add_aprovacao", args, jsonReset);
    return false;
}

function jsonReset(json) {
    if (json["status"] == "OK") {
        err("");
        if (esperandoAdd) {
            esperandoAdd = false;
            $("#success").text("Aprovação adicionada!");
            $("#success").fadeIn();
        }
        resetarLista(json["nova_lista"]);
    } else {
        err(json["message"]);
    }
}

function remask(id) {
    var args = {
        "credenciais_json": credenciais_json,
        "id_aprovacao": id
    };
    servPost("remove_aprovacao", args, jsonReset);
}

function chk(b) {
    if (b) return " checked";
    else return "";
}
    
/*function makeEditar(id) {
    var aprovacao = null;
    for (var index in aprovacoes) {
        if (!aprovacoes.hasOwnProperty(index)) continue;
        if (aprovacoes[index]["id_aprovacao"] == id) {
            aprovacao = aprovacoes[index];
        }
    }
    var id_aprovacao = aprovacao["id_aprovacao"];
    var nome_uni = aprovacao["nome_uni"];
    var nome_curso = aprovacao["nome_curso"];
    var rank = aprovacao["rank"];
    var publica = !!aprovacao["publica"];
    var treineiro = !!aprovacao["treineiro"];
    var meio_de_ano = !!aprovacao["meio_de_ano"];
    var sisu = !!aprovacao["sisu"];
    var baseid = "editando_" + id_aprovacao + "_";
    if (!aprovacao) return false;
    var edit_uni = "<td><input type=\"text\" class=\"form-control\" id=\""
        + baseid + "nome_uni\" value=\"" +  nome_uni + "\"></td>";
    var edit_curso = "<td><input type=\"text\" class=\"form-control\" id=\""
        + baseid + "nome_curso\" value=\"" +  nome_curso + "\"></td>";
    var edit_rank = "<td><input type=\"number\" min=\"0\" class=\"form-control\""
        + "id=\"" + baseid + "rank\" value=\"" + rank + "\"></td>";
    var edit_pub = "<td><label><input type=\"checkbox\" id=\"" + baseid
        + "publica\"" + chk(publica) + "></label></td>";
    var edit_trei = "<td><label><input type=\"checkbox\" id=\"" + baseid
        + "treineiro\"" + chk(treineiro) + "></label></td>";
    var edit_mda = "<td><label><input type=\"checkbox\" id=\"" + baseid
        + "meio_de_ano\"" + chk(meio_de_ano) + "></label></td>";
    var edit_sisu = "<td><label><input type=\"checkbox\" id=\"" + baseid
        + "sisu\"" + chk(sisu) + "></label></td>";
    var save = "<td><span class=\"glyphicon glyphicon-floppy-disk imgbtn\" "
        + "onclick=\"sendEdit(" + id_aprovacao + ")\"></span></td>";
    var cancel = "<td><span class=\"glyphicon glyphicon-remove imgbtn\" "
        + "onclick=\"restore(" + id_aprovacao + ")\"></span></td>";
    return edit_uni + edit_curso + edit_rank + edit_pub + edit_trei + edit_mda
        + edit_sisu + save + cancel;
}

function editar(id) {
    $("#aprovacao_" + id).html(makeEditar(id));
}

function sendEdit(id) {
    var baseid = "#editando_" + id + "_";
    var args = {
        "credenciais_json": credenciais_json,
        "id_aprovacao": id,
        "nome_uni": $(baseid + "nome_uni").val(),
        "nome_curso": $(baseid + "nome_curso").val(),
        "publica": 0+$(baseid + "publica").is(":checked"),
        "treineiro": 0+$(baseid + "treineiro").is(":checked"),
        "rank": $(baseid + "rank").val(),
        "meio_de_ano": 0+$(baseid + "meio_de_ano").is(":checked"),
        "sisu": 0+$(baseid + "sisu").is(":checked")
    };
    servPost("edit_aprovacao", args, editei);
}*/

function getAprovacaoById(id) {
    for (var index in aprovacoes) {
        if (aprovacoes[index]["id_aprovacao"] == id) {
            return aprovacoes[index];
        }
    }
}

var booleanas = ["publica", "meio_de_ano", "sisu"];
function editarAtributo(id, atributo, btn) {
    btn = $(btn);
    var aprovacao = getAprovacaoById(id);
    if (booleanas.indexOf(atributo) > -1) {
        var args = aprovacao;
        args["credenciais_json"] = credenciais_json;
        args[atributo] = 0+(!args[atributo]);
        servPost("edit_aprovacao", args, editei);
        return false;
    }
    var newContent = "<div class=\"list-group-item\" id=\"aprovacao_" + id
        + "\">"
    if (atributo == "categoria") {
        newContent += "<select id=\"editando_" + id + "_" + atributo
            + "\" class=\"form-control\">" + $("#categoria").html()
            + "</select>";
    } else {
        newContent += "<input id=\"editando_" + id + "_" + atributo
        + "\" class=\"form-control\" type=\"";
        if (atributo == "nome_uni" || atributo == "nome_curso") {
            newContent += "text\"";
        } else {
            newContent += "number\" min=\"0\"";
        }
        newContent += " value=\"" + aprovacao[atributo] + "\">";
    }
    newContent += "<button class=\""
        + "btn btn-success\" onclick=\"enviarEdit(" + id + ", '"
        + atributo + "')\">Salvar</button></div>";
    btn.replaceWith(newContent);
    if (atributo == "categoria") {
        $("#editando_" + id + "_categoria").val(aprovacao[atributo]);
    }
}

function enviarEdit(id, atributo) {
    var aprovacao = getAprovacaoById(id);
    var args = aprovacao;
    args["credenciais_json"] = credenciais_json;
    args[atributo] = $("#editando_" + id + "_" + atributo).val();
    servPost("edit_aprovacao", args, editei);
}


function restore(id) {
    for (var index in aprovacoes) {
        if (!aprovacoes.hasOwnProperty(index)) continue;
        if (aprovacoes[index]["id_aprovacao"] == id) {
            $("#aprovacao_" + id).html(makeAprovacaoBox(aprovacoes[index]));
        }
    }
}

function editei(json) {
    if (json["status"] == "OK") {
        aprovacoes = json["nova_lista"];
        restore(json["voce_editou"]);
    } else {
        err(json["message"]);
    }
}

