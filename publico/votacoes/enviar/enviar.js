if (location.hash.length < 4 || location.hash.indexOf(",") < 0) {
    location.assign("..");
}

var arr = location.hash.slice(1).split(",")
var id_votacao = arr[0];
var ano = arr[1];
var totdias = 4;

function loginVerified(val) {
    if (val === false) {
        $("#logado").hide();
        $("#nao_logado").fadeIn();
    } else {
        if (val["status"] === "OK") {
            $("#nome").html(val["seu_nome"].split(" ")[0]);
            $("#nao_logado").hide();
            $("#logado").fadeIn();
        } else {
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        }
    }
}

checkLoggedIn(loginVerified);
window.addEventListener("focus", function() {
    checkLoggedIn(loginVerified);
});

function voltar() {
    location.assign("../ver/" + location.hash);
}

var neodeus = false;
function verifiquei_se_posso_votar(json) {
    if (json["status"] != "OK") voltar();
    else {
        if (!json["pode"]) voltar();
        else {
            neodeus = true;
            $("#neo").fadeIn();
        }
    }

}

servGet("votacao_full", [id_votacao, ano], fillVotacao);

var selectors = [];
var excluirArtes = false;
function fillVotacao(json) {
    if (json["status"] == "FAILED") {
        location.assign("..");
    } else {
        var votacao = json["data"];
        totdias = votacao["dias"].length;
        if (votacao["neo"]) {
            if (!getToken()) location.assign("..");
            else servGet("posso_enviar_calendario", [getToken(), id_votacao],
                         verifiquei_se_posso_votar);
        }
        if (votacao["acabou"]) {
            location.assign("..");
        }
        if (ano == "1" && votacao["etapa"] == "1") {
            excluirArtes = true;
        }
        var blocks = "";
        for (var dia = 1; dia <= votacao["dias"].length; dia++) {
            var data = isoToDate(votacao["dias"][dia-1]);
            var barras = dateToBarras(data);
            var semana = dias_semana[data.getDay()];
            blocks += "<div class=\"maker_block\"><div class=\"maker_dia\">"
                + "Dia " + barras + " (" + semana + ")</div><div class=\"maker_sliders\">";
            for (var slider = 1; slider <= 3; slider++) {
                var id = "dia_" + dia + "_slider_" + slider;
                blocks += "<select class=\"form-control\" id=\"" + id + "\">"
                    + emptyOption + "</select><br>";
            }
            blocks += "</div><div class=\"maker_tempo\" id=\"tempo_" + dia
                + "\"></div></div><br><br>";
        }
        $("#calen").append(blocks);
        for (var dia = 1; dia <= votacao["dias"].length; dia++) {
            for (var slider = 1; slider <= 3; slider++) {
                var id = "dia_" + dia + "_slider_" + slider;
                var sel = $("#" + id);
                sel.attr("onchange", "updateSelectors()");
                sel.attr("oninput", "updateSelectors()");
                selectors.push(sel);
            }
        }
    }
    servGet("materias", [ano], fillMaterias);
}

function makeOption(materia) {
    return "<option value=\"" + materia["id_materia"] + "\">" + materia["nome"] + "</option>";
}

function getMateriaById(id) {
    for (var i = 0; i < materias.length; i++) {
        if (materias[i]["id_materia"] == id) return i;
    }
}

var emptyOption = "<option value=\"none\">- - - - - - - - - - - - - -</option>";
var dias = [[], [], [], []];
var materias = [];
function fillMaterias(json) {
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        if (json[key]["artes"] && excluirArtes) continue;
        json[key]["selected"] = false;
        materias.push(json[key]);
    }
    for (var i = 0; i < selectors.length; i++) {
        var selector = selectors[i];
        selector.html(makeOptionsWithLeft());
        selector.val("none");
        if (window.localStorage) {
            if (localStorage["calendario_salvo_unfail"]) {
                var unfailed = localStorage["calendario_salvo_unfail"].split(",");
                if (unfailed.length == selectors.length) {
                    var val = unfailed[i];
                    if ((getMateriaById(val) !== undefined) || (val == "none")) {
                        selector.val(val);
                    }
                }
            }
        }
    }
    updateSelectors();
}

function makeOptionsWithLeft() {
    var res = "";
    for (var i = 0; i < materias.length; i++) {
        var materia = materias[i];
        if (materia["selected"]) continue;
        res += makeOption(materia);
    }
    return emptyOption + res;
}

var strvals;
function updateSelectors() {
    for (var i = 0; i < materias.length; i++) {
        materias[i].selected = false;
    }
    dias = [];
    for (var i = 0; i < totdias; i++) {
        dias.push([]);
    }
    for (var j = 0; j < selectors.length; j++) {
        var selector = selectors[j];
        if (selector.val() == "none") continue;
        var mat_index = getMateriaById(selector.val());
        materias[mat_index]["selected"] = true;
        var dia = selector.attr("id")[4];
        dias[dia-1].push(materias[mat_index]);
    }
    for (var k = 0; k < selectors.length; k++) {
        var selector = selectors[k];
        var prevVal = selector.val();
        if (prevVal !== "none") {
            selector.html(makeOption(materias[getMateriaById(selector.val())])
                + makeOptionsWithLeft());
        } else {
            selector.html(makeOptionsWithLeft());
        }
        selector.val(prevVal);
    }
    var vals = [];
    for (var i = 0; i < selectors.length; i++) {
        vals.push(selectors[i].val());
    }
    for (var dia = 1; dia <= totdias; dia++) {
        var comp = $("#tempo_" + dia);
        var provas = dias[dia-1];
        var meiasprovas = 0;
        for (var k in provas) {
            var materia = provas[k];
            meiasprovas++;
            if (!materia["curta"]) {
                if (ano == 3) {
                    if (materia["nome"] != "Inglês") {
                         meiasprovas++;
                    }
                } else {
                    meiasprovas++;
                }
            }
        }
        comp.html("Horário: 07:05 - " + tempoMaximo(meiasprovas, ano, totdias));
    }
    strvals = vals.join(",");
    if (window.localStorage) {
        localStorage["calendario_salvo_unfail"] = strvals;
    }
}

function enviarCalendario() {
    updateSelectors();
    if (!neodeus && totdias == 4) { 
        for (var i = 0; i < materias.length; i++) {
            if (!materias[i]["selected"]) {
                err("Cadê a prova de " + materias[i]["nome"] + "?");
                return false;
            }
        }
        for (var i = 0; i < dias.length; i++) {
            var dia = dias[i];
            if (dia.length == 0) {
                err("Nenhuma prova no " + (i+1) + "º dia?");
                return false;
            }
            if (dia.length == 1) {
                err("Só uma prova no " + (i+1) + "º dia?");
                return false;
            }
            var humanas = 0;
            var exatas = 0;
            var curtas = 0;
            var ingles = false;
            var bio = false;
            var meiasprovas = 0;
            for (var j = 0; j < dia.length; j++) {
                if (dia[j]["curta"]) {
                    curtas++;
                } else {
                    switch (dia[j]["tipo"]) {
                        case "HUMANAS": humanas++; break;
                        case "BIOLOGICAS": bio = true; break;
                        case "INGLES": ingles = true; break;
                        case "EXATAS": exatas++; break;
                    }
                }
                meiasprovas++;
                if (!dia[j]["curta"]) {
                    if (ano == 3) {
                        if (dia[j]["nome"] != "Inglês") {
                             meiasprovas++;
                        }
                    } else {
                        meiasprovas++;
                    }
                }
            }
            if ((meiasprovas < 4 || meiasprovas > 5) && (totdias == 5 && meiasprovas < 3)) {
                err("Verifique o número e o tempo das provas no " + (i+1) + "º dia!");
                return false;
            } 
            if (humanas > 1) {
                err("Muitas humanas no " + (i+1) + "º dia...");
                return false;
            }
            if (exatas > 1) {
                err("Muitas exatas no " + (i+1) + "º dia...");
                return false;
            }
            if (curtas > 2) {
                err("Três provas curtas " + (i+1) + "º dia? Sério?");
                return false;
            }
            if (curtas == 2 && exatas + humanas == 0) {
                err("Só provas curtas " + (i+1) + "º dia? Sério?");
                return false;
            }
            if (exatas == 0 && !bio) {
                err("Sem exatas no " + (i+1) + "º dia?");
                return false;
            }
            if (humanas == 0 && !ingles && curtas < 2) {
                err("Sem humanas no " + (i+1) + "º dia?");
                return false;
            }
        }
        if (!getToken()) {
            err("Faça login acima!");
            return false;
        }
    }
    err("");
    var args = {
        "token": getToken(),
        "id_votacao": id_votacao,
        "calendario": strvals,
        "voting_as": ano
    };
    if (neodeus) {
        args["voting_as"] = $("#neo_ano").val();
    }
    servPost("enviar_calendario", args, enviei);
    return false;
}

function enviei(json) {
    if (json["status"] == "FATAL") location.assign("..");
    else if (json["status"] == "FAILED") err(json["message"]);
    else location.assign("../ver/#" + id_votacao + "," + ano + "," + json["id_calendario"]);
}

function limpartudo() {
    for (var index in selectors) {
        var selector = selectors[index];
        selector.val("none");
    }
    updateSelectors();
}