if (location.hash.length < 4 || location.hash.indexOf(",") < 0) {
    location.assign("..");
}

var arr = location.hash.slice(1).split(",")
var id_votacao = arr[0];
var ano = arr[1];

var amLoggedIn = false;
var voteiEm = null;
var enviei = false;

function gotVoto(json) {
    amLoggedIn = !!json["logado"];
    enviei = !!json["enviou"];
    if (enviei || !amLoggedIn) $("#enviarBtn").hide();
    if (json["votou"]) voteiEm = json["id"];
    servGet("materias", [ano], gotMaterias);
}
servGet("meu_voto", [getToken(), id_votacao], gotVoto);

var materias;
function gotMaterias(json) {
    materias = json;
    servGet("votacao_full", [id_votacao, ano], fillVotacao);
}

function fillVotacao(json) {
    if (json["status"] == "FAILED") {
        location.assign("..");
    } else {
        var votacao = json["data"];
        var acabou = votacao["acabou"];
        setCookie("last_etapa", votacao["etapa"]);
        setCookie("last_periodo", votacao["periodo"]);
        $("span[name=etapa]").html(votacao["etapa"]);
        $("span[name=periodo]").html(votacao["periodo"]);
        $("span[name=voters]").html(votacao["totalvotos"]);
        $("span[name=senders]").html(votacao["calendarios"].length);
        $("span[name=totalalunos]").html(votacao["totalalunos"]);
        $("span[name=ano]").html(ano);
        var calendarios = votacao["calendarios"];
        if (acabou) {
            calendarios.sort(comparadorVotos);
        } else {
            calendarios.sort(comparadorEnvio);
        }
        var cals = "";
        for (var index in calendarios) {
            if (!calendarios.hasOwnProperty(index)) continue;
            var calendario = calendarios[index];
            var id_calendario = calendario["id_calendario"];
            var id_autor = calendario["id_autor"];
            var header = "Sugestão de um aluno aleatório:";
            if (acabou) {
                header = "De: <i>" + calendario["nome_autor"] + "</i> ("
                    + calendario["ano_autor"] + "º " + calendario["sala_autor"]
                    + ")<br><b>Votos: " + calendario["votos"] + "</b>";
            }
            cals += "<div class=\"panel panel-primary cal\" id=\""
                + "cal_" + id_calendario + "\">"
                + "<div class=\"panel-heading panel-title cal-header\">"
                + header + "</div><div class=\"" + "cal-blocks\">";
            var dias = JSON.parse(calendario["calendario"]);
            for (var index in dias) {
                var dia = dias[index];
                var data = isoToDate(votacao["dias"][index]);
                var barras = dateToBarras(data);
                var semana = dias_semana[data.getDay()];
                cals += "<div class=\"list-group cal-block\"><div "
                    +   "class=\"list-group-item cal-dia\">"
                    + "Dia " + barras + " (" + semana + "):</div>";
                var meiasprovas = 0;
                for (var j in dia) {
                    if (dia[j] == "none") continue;
                    var materia = getMateriaById(materias, dia[j]);
                    meiasprovas++;
                    if (!materia["curta"]) {
                        if (ano == 3) {
                            if (materia["nome"] != "Inglês") {
                                 meiasprovas++;
                            }
                        } else {
                            meiasprovas++;
                        }
                    }
                    var nomeMateria = materia["nome"];
                    cals += "<div class=\"cal-materia list-group-item\">"
                        + nomeMateria + "</div>";
                }
                cals += "<div class=\"list-group-item cal-tempo\">"
                    + "Término: <b>" + tempoMaximo(meiasprovas, ano)
                    + "</b></div></div>";
                if (index == 1 || index == 3) {
                    cals += "<br>";
                }
            }
            if (amLoggedIn && !acabou) {
                cals += "<a class=\"list-group-item cal-votar\""
                    + " href=\"javascript:void(0)\""
                    + " id=\"votar_em_" + id_calendario + "\""
                    + " onclick=\"votar_em(" + id_calendario + ")\">";
                if (voteiEm === id_calendario) {
                    cals += "Você votou neste calendário!</a>";
                } else {
                    cals += "Votar nesse calendário</a>";
                }
            }
            cals += "</div></div></div><br><br>";
        }
        $("#calendarios").html(cals);
        
        if (!votacao["neo"]) {
            if (votacao["calendarios"].length == 0) {
                $("#calendarios").html("<br>Nenhum calendário foi sugerido ainda,"
                    + " seja o primeiro!");
                $("#calendarios").toggleClass("lead");
            }
            $(".com_neo").hide();
        } else {
            $(".sem_neo").hide();
            if (getToken())
                servGet("posso_enviar_calendario", [getToken(), id_votacao],
                    verifiquei_se_posso_votar);
            else
                naoPosso();
        }

        $("#carregando").hide();

        if (votacao["acabou"]) {
            $("#acabou").fadeIn();
        } else {
            $("#naoacabou").fadeIn();
            if (!enviei && !votacao["neo"]) $("#ngostou").show();
        }

        if (arr.length > 2) {
            scrollTo("cal_" + arr[2]);
            location.hash = "#" + arr[0] + "," + arr[1];
        }
    }
}

function verifiquei_se_posso_votar(json) {
    if (json["status"] != "OK") naoPosso();
    else {
        if (!json["pode"]) naoPosso();
        else $("#enviarBtn").show();
    }
}

function naoPosso() {
    $("#ngostou").hide();
    $("#enviarBtn").hide();
    $("#neo").show();
}

function enviar() {
    location.assign("../enviar/" + location.hash);
}

var votando_id;
function votar_em(id) {
    var args = {
        "token": getToken(),
        "id_calendario": id,
        "id_votacao": id_votacao
    }
    if (id == voteiEm) return false;
    votando_id = id;
    servPost("votar_em", args, votei);
}

function votei(json) {
    $(".cal-votar").html("Votar nesse calendário");
    $("#votar_em_" + votando_id).html(json["message"]);
    if (json["status"] == "OK") voteiEm = votando_id;
}