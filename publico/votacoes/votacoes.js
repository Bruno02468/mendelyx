var ano;
if (getCookie("ano")) {
    ano = getCookie("ano");
    $("#ano").val(ano);
}

function setAno() {
    ano = $("#ano").val();
    setCookie("ano", ano);
}

servGet("votacoes_basic", [], tenhoVotacoes);

function tenhoVotacoes(json) {
    $("#carregando").hide();
    json.reverse();
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        var votacao = json[key]
        var acabou = votacao["acabou"]
        var etapa = votacao["etapa"]
        var periodo = votacao["periodo"]
        var id_votacao = votacao["id_votacao"]
        var texto = etapa + "ª Etapa - " + periodo + "º Período";
        var link = "<a href=\"javascript:void(0)\" onclick=\"irVotacao('"
            + id_votacao + "')\">" + texto + "</a>";
        if (!acabou) {
            $("#ativa").fadeIn();
            $("#ativa").append(link);
        } else {
            $("#resultados").fadeIn();
            $("#resultados").append(link + "<br>");
        }
    }
}

function irVotacao(id) {
    location.assign("ver/#" + id + "," + ano);
}