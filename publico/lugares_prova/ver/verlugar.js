var hasharr = location.hash.slice(1).split(",");
var ano, sala, chamada;
var tenhoAluno = false;
if (hasharr.length < 1) {
    location.assign("..");
} else if (hasharr.length == 4) {
    ano = parseInt(hasharr[1]);
    sala = hasharr[2];
    chamada = parseInt(hasharr[3]);
    tenhoAluno = true;
    location.hash = "#" + hasharr[0];
}
var salaid = hasharr[0];

servGet("get_full_sala_prova", [salaid], tenhoSalaProva);

function tenhoSalaProva(json) {
    if (!json) location.assign("..");
    var fileiras = [{}];
    fileiras = fileiras.concat(JSON.parse(json["fileiras"]));
    fileiras.push({});
    console.log(fileiras);
    var linhas = 0;
    for (var fin in fileiras) {
        var fileira = fileiras[fin];
        if (!fileira["ano"]) {
            $("#anos").append("<th></th>");
            continue;
        }
        $("#anos").append("<th class=\"ano" + fileira["ano"] + "\">"
            + fileira["ano"] + "º</th>");
        linhas = Math.max(linhas, fileira["lugares"].length);
    }
    for (var linha = 0; linha < linhas; linha++) {
        var tr = "<tr>";
        for (var fin in fileiras) {
            var fileira = fileiras[fin];
            var id = " id=\"linha_" + linha + "_fileira_" + fin + "\"";
            if (!fileira["ano"]) {
                tr += "<td class=\"empty\"" + id + "></td>";
                continue;
            }
            if (fileira["lugares"].length <= linha) {
                tr += "<td class=\"vazio ano" + fileira["ano"] + "\"" + id
                    + "></td>";
            } else {
                var lugar = fileira["lugares"][linha];
                if (lugar === null) {
                    tr += "<td class=\"vazio ano" + fileira["ano"] + "\"" + id
                        + "></td>";
                } else {
                    var al = fileira["lugares"][linha];
                    var s = "";
                    if (tenhoAluno) {
                        if (fileira["ano"] == ano && al["chamada"] == chamada
                            && al["sala"] == sala) {
                            s = " eu";
                        }
                    }
                    tr += "<td class=\"aluno ano" + fileira["ano"] + s + "\""
                        + id + ">" + al["chamada"] + " " + al["sala"] + "</td>";
                }
            }
        }
        tr += "</tr>";
        $("#lugares").append(tr);
    }
    $(".lousa").attr("colspan", fileiras.length-2);
    if (json["lousa_acima"]) {
        $("#lousa_acima").fadeIn();
    } else {
        $("#lousa_abaixo").fadeIn();
    }
    $("#se").prependTo("#linha_0_fileira_0");
    $("#sd").prependTo("#linha_0_fileira_" + (fileiras.length-1));
    $("#ie").prependTo("#linha_" + (linhas-1) + "_fileira_0");
    $("#id").prependTo("#linha_" + (linhas-1) + "_fileira_" + (fileiras.length-1));
    $("#" + json["posicao_porta"]).css("visibility", "visible");
    $("#numsala").text(json["numsala"]);
    $("#periodo").text(json["periodo"]);
    $("h1").text("Sala de Prova " + json["numsala"]);
    document.title = "Sala de Prova " + json["numsala"];
    $("#carregando").hide();
    $("#carregado").fadeIn();
}
