for (var index in validsalas) {
    var a = validsalas[index][0];
    var s = validsalas[index][1];
    $("#sala").append(mkoption(a + s, a + "º " + s));
}

servGet("get_salas_prova_ep", [], tenhoEP);

function tenhoEP(json) {
    if (!json.length) {
        $("#naotemos").fadeIn();
        return;
    }
    for (var index in json) {
        var p = json[index]["periodo"];
        $("#ep").append(mkoption(p, p + "º período"));
    }
    if (getCookie("last_periodo")) {
        var probable = {
            "periodo": getCookie("last_periodo")
        };
        if (json.indexOf(probable) > -1) {
            $("#ep").val(probable["periodo"]);
        }
    }
    $("#temos").fadeIn();
    if (getToken()) checkLoggedIn(tenhoLogin);
}

function ep_update() {
    var arr = $("#ep").val().split("_");
    setCookie("last_etapa", arr[0]);
    setCookie("last_periodo", arr[1]);
}

function tenhoLogin(json) {
    if (json["status"] == "OK") {
        $("#sala").val(json["seu_ano"] + json["sua_sala"]);
        $("#chamada").val(json["seu_numero"]);
    } else {
        deleteToken();
        window.location.reload();
    }
}

var ano, sala, chamada;
function sovamo() {
    var periodo = $("#ep").val();
    ano = $("#sala").val()[0];
    sala = $("#sala").val()[1];
    chamada = $("#chamada").val();
    servGet("get_minha_sala_prova", [periodo, ano, sala, chamada],
        tenhoMinhaSala);
}

function tenhoMinhaSala(json) {
    if (json["status"] == "OK") {
        location.assign("ver/#" + [json["id_sala_prova"], ano, sala,
            chamada].join(","));
    } else {
        err(json["message"]);
    }
}
