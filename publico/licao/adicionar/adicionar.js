var aulas = {};
var ano = getCookie("ano");
var sala = getCookie("sala");

if (!ano || !sala) {
    location.assign("..");
} else {
    $("#toptitle").append(" (" + ano + "º " + sala + ")");
}

servGet("aulas", [ano], fillDropdown);

$("#nao_logado").hide();

function fillDropdown(json) {
    json.sort(function(a, b){
        return a["nome"].localeCompare(b["nome"]);
    });
    for (var index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var nome = json[index]["nome"];
        var id = json[index]["id_aula"];
        aulas[id] = nome;
        var option = "<option value=\"" + id + "\">" + nome + "</option>";
        $("#aula_select").append(option);
    }
    servGet("get_horario", [ano, sala], fillHorario);
}

function fillHorario(json) {
    if (json["has"]) {
        for (var day = 1; day <= 5; day++) {
            var data = getNext(day);
            var d = padzeroes(data.getDate());
            var m = padzeroes(data.getMonth()+1);
            $("#sem_" + day).append(" (" + d + "/" + m + ")");
        }
        var horario = JSON.parse(json["horario"]);
        var tbody = "";
        for (var aula = 1; aula <= 8; aula++) {
            tbody += "<tr>";
            for (var dia = 1; dia <= 5; dia++) {
                var id_aula = horario["aula_" + aula + "_dia_" + dia];
                var nome_aula = aulas[id_aula] || "";
                tbody += "<td onclick=\"horClick(" + id_aula + ", " + dia
                    + ")\">" + nome_aula + "</a></td>";
            }
            if ([2, 4, 6].indexOf(aula) > -1) {
                tbody += "<tr class=\"intervalo\">"
                    + "<td colspan=\"5\">INTERVALO</td></tr>";
            }
            tbody += "</tr>";
        }
        $("#horario").html(tbody);
        $("#horario_wrapper").fadeIn();
    } else {
        $("$sem_horario").fadeIn();
    }
}

function horClick(mat, day) {
    $("#aula_select").val(mat);
    prox(day);
}

function setDate(date) {
    var isos = toIso(date);
    $("#para").attr("value", isos);
    syncPara();
}

var now = new Date();
setDate(now);
$("#para").attr("min", toIso(now));

function syncPara() {
    $("#para").val($("#para").attr("value"));
}

function datadd(n) {
    var added = new Date();
    added.setDate(added.getDate() + n);
    setDate(added);
}

function getNext(n) {
    var date = new Date();
    do {
        date.setDate(date.getDate() + 1);
    } while (date.getDay() != n);
    return date;
}

function prox(n) {
    var date = getNext(n);
    setDate(date);
}

function enviar() {
    var post = {
        "id_aula": $("#aula_select").val(),
        "para": $("#para").val(),
        "info": $("#info").val(),
        "token": getToken()
    }
    if (!post["info"]) {
        err("Digite alguma coisa!");
        return;
    }
    servPost("adicionar_licao", post, enviei);
}

function enviei(json) {
    if (json["status"] == "OK") {
        $("#info").val("");
        success("Lição adicionada com sucesso!<br>Adicione mais, ou <a"
            + " href=\"..\">volte para a sua sala!</a>");
    } else {
        if (json["message"] == "TOKEN") {
            err("Faça login acima!");
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        } else if (json["message"] == "BANNED") {
            err("Você foi banido, pois lições suas receberam mais de 30 denúncias!");
        } else if (json["message"] == "EMPTY") {
            err("Escreva alguma coisa!");
        }
    }
}

function loginVerified(val) {
    if (val === false) {
        $("#logado").hide();
        $("#nao_logado").fadeIn();
    } else {
        if (val["status"] === "OK") {
            $("#nominho").html(val["seu_nome"].split(" ")[0]);
            if (json["seu_ano"] != ano || json["sua_sala"] != sala) {
                setCookie("ano", json["seu_ano"]);
                setCookie("sala", json["sua_sala"]);
                window.location.reload();
            }
            $("#nao_logado").hide();
            $("#logado").fadeIn();
        } else {
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        }
    }
}

function goSala() {
    if (ano && sala) {
        location.assign("../sala/#" + ano + sala);
    } else {
        location.assign("..");
    }
}

checkLoggedIn(loginVerified);
window.addEventListener("focus", function() {
    checkLoggedIn(loginVerified);
});