var ano;
var sala;

checkLoggedIn(gotLoginStatus);

function gotLoginStatus(json) {
    if (json["status"] == "FAILED") {
        $("#login").fadeIn();
    } else {
        ano = json["seu_ano"];
        sala = json["sua_sala"];
        servGet("get_msg_sala", [ano, sala], tenhoMsgSala);
        $("#toptitle").append(" (" + ano + "º " + sala + ")");
    }
}

function tenhoMsgSala(json) {
    if (json["has"]) {
        $("#texto").val(json["texto"]);
    }
    $("#logado").fadeIn();
}

function goSala() {
    if (ano && sala) {
        location.assign("../sala/#" + ano + sala);
    } else {
        window.history.back();
    }
}

function enviar() {
    var args = {
        "token": getToken(),
        "texto": makeBase64URLSafe(btoa(unescape(
            encodeURIComponent($("#texto").val())
        )))
    };
    servPost("set_msg_sala", args, enviei);
}

function enviei(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        goSala();
    }
}
