var ano = getCookie("ano");
var sala = getCookie("sala");
var aulas = {};
var meu_id = "";

if (location.hash.length == 3) {
    ano = location.hash[1];
    sala = location.hash[2];
    setCookie("ano", ano);
    setCookie("sala", sala);
} else {
    location.hash = "#" + ano + sala;
}

if (!salaExists(ano + sala)) {
    console.log(ano + sala + " NÃO EXISTE!");
    deleteCookie("ano");
    deleteCookie("sala");
    location.assign("..");
} else {
    servGet("aulas", [ano], readyAulas);
}

function readyAulas(json) {
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        aulas[json[key]["id_aula"]] = json[key]["nome"];
    }
    setCookie("ano", ano);
    setCookie("sala", sala);
    $("#toptitle").html("Lições do " + nomeSala(ano + sala));
    $("title").append(" do " + nomeSala(ano + sala));
    servGet("get_horario", [ano, sala], tenhoHorario);
}

var showHorAdd = false;

function tenhoHorario(json) {
    if (json["has"]) {
        var horario = JSON.parse(json["horario"]);
        var tbody = "";
        for (var aula = 1; aula <= 8; aula++) {
            tbody += "<tr>";
            for (var dia = 1; dia <= 5; dia++) {
                var id_aula = horario["aula_" + aula + "_dia_" + dia];
                var nome_aula = aulas[id_aula] || "";
                tbody += "<td>" + nome_aula + "</td>";
            }
            if ([2, 4, 6].indexOf(aula) > -1) {
                tbody += "<tr class=\"intervalo\">"
                    + "<td colspan=\"5\">INTERVALO</td></tr>";
            }
            tbody += "</tr>";
        }
        $("#horario").html(tbody);
        $("#horario_nome").text(json["last_editor_nome"]);
        $("#horario_chamada").text(json["last_editor_chamada"]);
        $("#horario_hora").text(timestr(json["last_edit_time"]));
        $("#tem_horario").fadeIn();
    } else {
        showHorAdd = true;
    }
    servGet("get_msg_sala", [ano, sala], tenhoMsgSala);
}

var showMsgAdd = false;
function tenhoMsgSala(json) {
    if (json["has"]) {
        $("#msg_sala").html(brunoml_to_html(parse_links(json["texto"])));
        $("#msg_nome").text(json["last_editor_nome"]);
        $("#msg_chamada").text(json["last_editor_chamada"]);
        $("#msg_hora").text(timestr(json["last_edit_time"]));
        $("#tem_msg_sala").fadeIn();
    } else {
        showMsgAdd = true;
        $("#sem_msg_sala").fadeIn();
    }
    checkLoggedIn(gotAlunoData);
}

function editMsgSala() {
    location.assign("../msg_sala");
}

function gotAlunoData(json_aluno) {
    if (json_aluno !== false) {
        if (json_aluno["status"] === "OK") {
            meu_id = json_aluno["seu_id"];
            if (showHorAdd) {
                $("#sem_horario").fadeIn();
            } else {
                $("#horedit").fadeIn();
            }
            if (showMsgAdd) {
                $("#sem_msg_sala").fadeIn();
            } else {
                $("#msgedit").fadeIn();
            }
        }
    }
    if (getToken()) {
        servGet("get_feitas", [getToken()], tenhoFeitas);
    } else {
        servGet("licoes_ativas_sala", [ano, sala], appendLicoes);
    }
}

function tenhoFeitas(json) {
    if (json["has"]) {
        setCookie("feitas", json["feitas"]);
    }
    servGet("licoes_ativas_sala", [ano, sala], appendLicoes);
}

function voltar() {
    deleteCookie("ano");
    deleteCookie("sala");
    location.assign("../");
}

var hoje = new Date();
var next = new Date();
do {
    next.setDate(next.getDate() + 1);
} while (next.getDay() == 0 || next.getDay() == 6);
var hojeIso = toIso(hoje);
var nextIso = toIso(next);

function comparadorEntrega(a, b) {
    return Date.parse(a["para"]) - Date.parse(b["para"]);
}

function appendLicoes(json) {
    var licoes = [];
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        licoes.push(json[key]);
    }
    licoes.sort(comparadorEntrega);

    if (licoes.length == 0) {
        $("#licoes").html("<big>Parece que nenhuma lição foi adicionada, "
            + "<i>por enquanto</i>.</big>");
        return;
    }
    var feitas = [];
    try {
        if (getCookie("feitas")) feitas = JSON.parse(getCookie("feitas"));
    } catch(err) {
        // deu ruim clan
    }
    if (!feitas["length"]) feitas = [];
    var licoes_html = "";
    for (var index = 0; index < licoes.length; index++) {
        var licao = licoes[index];
        var panelClass = "licao panel panel-";
        var panelTitle = "<b>" + aulas[licao["id_aula"]]
            + "</b><br><span class=\"paradia\">";

        if (licao["para"] == hojeIso) {
            panelClass += "success";
            panelTitle += " (para hoje)";
        } else if (licao["para"] == nextIso) {
            panelClass += "danger";
            if (next.getDay() == 1 && hoje.getDay != 0) {
                panelTitle += " (para segunda)"
            } else {
                panelTitle += " (para amanhã)";
            }
        } else {
            panelClass += "default";
            var para = isoToDate(licao["para"]);
            var dia = para.getDate();
            var mes = para.getMonth();
            panelTitle += " (para " + dias_semana[para.getDay()] + ", dia "
                + dia;
            if (para.getMonth() != hoje.getMonth()) {
                panelTitle += " de " + meses[para.getMonth()];
            }
            panelTitle += ")";
        }
        panelTitle += "</span>"
        
        var checked = "";
        if (feitas.indexOf(licao["id_licao"]) > -1) {
            checked = " checked=\"true\"";
        }
        var feita = "toggleFeita(" + licao["id_licao"] + ")";
        var panelBody = "<span class=\"licao-normal-text\">" 
            + brunoml_to_html(parse_links(licao["info"]))
            + "</span><br><br><div class=\"feita\"><input type=\"checkb"
            + "ox\" onclick=\"" + feita + "\""
            + " id=\"" + licao["id_licao"] + "\""
            + checked + "><label for=\"" + licao["id_licao"] + "\">&nbsp;Feita"
            + "</label></div><div class=\"autorlicao\">Adicionada por ";
        var autor = licao["id_aluno"].toString();
        var id_licao = licao["id_licao"].toString();

        if (autor == meu_id) {
            panelBody += "você.<br><button class=\"btn btn-xs btn-primary\" "
                + "onclick=\"editar('" + id_licao + "')\"><span class=\"glyphi"
                + "con glyphicon-edit iconbut\"></span> Editar</button> "
                + "<button class=\"btn btn-xs btn-danger\" onclick=\"apagar('"
                + id_licao + "', this)\"><span class=\"glyphicon "
                + "glyphicon-remove iconbut\"></span> Apagar</button></div>";

        } else {
            var den = "";
            if (licao["denuncias"] > 0) den = " (" + licao["denuncias"] + ")";
            panelBody += licao["nome_autor"].split(" ")[0] + " (nº "
                + licao["chamada_autor"] + ").<br>"
                + "<button class=\"btn btn-xs btn-danger\" onclick=\"reportar('"
                + id_licao + "', this)\"><span class=\"glyphicon "
                + "glyphicon-thumbs-down iconbut\"></span>" + den
                + "</button></div>";
        }

        var component = "<div id=\"licao-" + id_licao + "\" class=\""
            + panelClass + "\"><div class=\"panel-heading\">"
            + "<h3 class=\"panel-title licao-normal-text\">" + panelTitle
            + "</h3></div>" + "<div class=\"panel-body\">" + panelBody
            + "</div></div><br>";
        licoes_html += component;
    }
    $("#licoes").html(licoes_html);
}

function adicionar() {
    location.assign("../adicionar");
}

function editar(id) {
    location.assign("../editar/#" + id);
}

function apagar(id, button) {
    button.innerHTML = "Certeza?"
    button.setAttribute("onclick", "apagarConfirma('" + id + "')");
}

function apagarConfirma(id) {
    servGet("apagar_licao", [getToken(), id], apagarConfirmed);
}

function apagarConfirmed() {
    location.reload();
}

function reportar(id, button) {
    if (meu_id) {
        button.innerHTML = "Certeza?";
        button.setAttribute("onclick", "reportarConfirma('" + id + "')");
    } else {
        doLogin();
    }
}

function reportarConfirma(id) {
    function reportarConfirmed(json) {
        $("#licao-" + id).attr("class", json["status"] == "OK" ? "success"
            : "err");
        $("#licao-" + id).html(json["message"]);
    }
    servGet("reportar_licao", [getToken(), id], reportarConfirmed);
}

$("#horario_wrapper").hide();
var showingHorario = false;
function horario() {
    if (showingHorario) {
        $("#horario_wrapper").fadeOut();
    } else {
        $("#horario_wrapper").fadeIn();
    }
    showingHorario = !showingHorario;
}

function editHorario() {
    location.assign("../horario");
}

function toggleFeita(id) {
    if (!getCookie("feitas")) {
        setCookie("feitas", "[]");
    }
    var arr = JSON.parse(getCookie("feitas"));
    var index = arr.indexOf(id);
    if (index > -1) {
        arr.splice(index, 1);
    } else {
        arr.push(id);
    }
    setCookie("feitas", JSON.stringify(arr));
    if (getToken()) {
        var args = {
            "token": getToken(),
            "feitas": JSON.stringify(arr)
        };
        servGet("toggle_feita", [getToken(), id], tenhoFeita);
    }
}

function tenhoFeita(json) {
    if (json["status"] == "FAILED") {
        deleteToken();
        window.location.reload();
    }
}