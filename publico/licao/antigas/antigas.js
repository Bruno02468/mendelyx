var ano = getCookie("ano");
var sala = getCookie("sala");
var aulas = {};

if (ano && sala) {
    $("#ano").val(ano);
    anoUpdate();
    $("#sala").val(sala);
}

var data = new Date();
var hojeiso = data.toISOString().slice(0, 10);
data.setDate(1);
data.setMonth(0);
var anoiso = data.toISOString().slice(0, 10);

$("#envio_start").val(anoiso);
$("#entrega_start").val(anoiso);
$("#envio_end").val(hojeiso);
$("#entrega_end").val(hojeiso);

function anoUpdate() {
    ano = $("#ano").val();
    $("#sala").html("");
    for (var index in validsalas) {
        var salaid = validsalas[index];
        if (salaid[0] != ano) continue;
        var nomesala = nomeSala(salaid);
        $("#sala").append("<option value=\"" + salaid[1] + "\">" + nomesala
            + "</option>");
    }
    servGet("aulas", [$("#ano").val()], tenhoAulas);
}

function tenhoAulas(json) {
    $("#aula").html("");
    for (var index in json) {
        var aula = json[index];
        var id_aula = aula["id_aula"];
        var nome_aula = aula["nome"];
        $("#aula").append("<option value=\"" + id_aula + "\">" + nome_aula
            + "</option>");
        aulas[json[index]["id_aula"]] = json[index]["nome"];
    }
}

function pesquisar() {
    var args = {
        "ano": $("#ano").val(),
    };
    var outros = ["sala", "aula", "envio_start", "envio_end", "entrega_start",
        "entrega_end"];
    for (var index in outros) {
        var key = outros[index];
        var checkbox = $("#chk_" + key.replace(/_.+/, ""));
        if (checkbox.is(":checked")) {
            args[key] = $("#" + key).val();
        } else {
            args[key] = null;
        }
    }
    servPost("pesquisar_licoes", args, tenhoResultados);
}

var hoje = new Date();
var next = new Date();
do {
    next.setDate(next.getDate() + 1);
} while (next.getDay() == 0 || next.getDay() == 6);
var hojeIso = toIso(hoje);
var nextIso = toIso(next);

function tenhoResultados(licoes) {
    $("#resultados").hide();
    if (licoes.length == 0) {
        $("#resultados").html("Nenhum resultado encontrado!")    
    } else {
        var html = "";
        var licoes_html = "";
        for (var index = 0; index < licoes.length; index++) {
            var licao = licoes[index];
            var panelClass = "licao panel panel-";
            var panelTitle = "<b>" + aulas[licao["id_aula"]]
                + "</b><br><span class=\"paradia\">(passada em "
                + dateToBarras(isoToDate(licao["passada"])) + ")<br>";

            if (licao["para"] == hojeIso) {
                panelClass += "success";
                panelTitle += " (para hoje)";
            } else if (licao["para"] == nextIso) {
                panelClass += "danger";
                if (next.getDay() == 1 && hoje.getDay != 0) {
                    panelTitle += " (para segunda)"
                } else {
                    panelTitle += " (para amanhã)";
                }
            } else {
                panelClass += "default";
                var para = isoToDate(licao["para"]);
                var dia = para.getDate();
                var mes = para.getMonth();
                panelTitle += " (para dia " + dia;
                if (para.getMonth() != hoje.getMonth()) {
                    panelTitle += " de " + meses[para.getMonth()];
                }
                panelTitle += ")";
            }
            panelTitle += "</span>"
            
            var panelBody = "<span class=\"licao-normal-text\">" 
                + brunoml_to_html(parse_links(licao["info"]))
                + "</span><br>";
            var autor = licao["id_aluno"].toString();
            var id_licao = licao["id_licao"].toString();

            var component = "<div id=\"licao-" + id_licao + "\" class=\""
                + panelClass + "\"><div class=\"panel-heading\">"
                + "<h3 class=\"panel-title licao-normal-text\">" + panelTitle
                + "</h3></div>" + "<div class=\"panel-body\">" + panelBody
                + "</div></div><br>";
            licoes_html += component;
        }
        $("#resultados").html(licoes_html);
    }
    $("#resultados").fadeIn();
}
