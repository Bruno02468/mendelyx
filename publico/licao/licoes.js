var salas;

if (getCookie("sala")) {
    var urlsala = "sala/#" + getCookie("ano") + getCookie("sala");
    location.assign(urlsala);
    $("#toptitle").html("Redirecionando...");
    $("#salalink").attr("href", urlsala);
    $("#comsala").fadeIn();
} else {
    servGet("salas", [], function(json) {
        salas = json;
        $("#semsala").fadeIn();
    });
}

function ano(i, elem) {
    elem.style.color = nicegreen;
    setCookie("ano", i);
    var links = [];
    for (var index in salas[i]) {
        var sala = salas[i][index];
        links.push("<a href=\"javascript:void(0)\"" +
        " onclick=\"sala('" + sala + "', this)\">" + sala + "</a>");
    }
    $("#salasel").html("E qual seria sua sala?<br><br><span id=\"salasels\">"
        + links.join(" | ") + "</span>");
    $("#salasel").fadeIn();
}

function sala(c, elem) {
    elem.style.color = nicegreen;
    setCookie("sala", c);
    $("#thx").fadeIn();
    slowRedir("sala/#" + getCookie("ano") + getCookie("sala"));
}