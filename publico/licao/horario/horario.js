var ano;
var sala;

checkLoggedIn(gotLoginStatus);

function gotLoginStatus(json) {
    if (json["status"] == "FAILED") {
        $("#login").fadeIn();
    } else {
        ano = json["seu_ano"];
        sala = json["sua_sala"];
        servGet("aulas", [ano], tenhoAulas);
        $("#toptitle").append(" (" + ano + "º " + sala + ")");
    }
}

var aulas;
var selects = [];
var dias = 5;
var aulas_dia = 8;
function tenhoAulas(json) {
    aulas = json;
    var selectInner = "<option value=\"0\">- - - - - - - - - - -</option>";
    for (var index in aulas) {
        var aula = aulas[index];
        var idaula = aula["id_aula"];
        var nomeaula = aula["nome"];
        selectInner += "<option value=\"" + idaula + "\">" + nomeaula
            + "</option>";
    }
    var tbody = "";
    for (var aula = 1; aula <= aulas_dia; aula++) {
        tbody += "<tr>";
        for (var dia = 1; dia <= dias; dia++) {
            var selid = "aula_" + aula + "_dia_" + dia;
            tbody += "<td><select id=\"" + selid + "\" class=\"form-control\">"
                + selectInner + "</select></td>";
            selects.push(selid);
        }
        tbody += "</tr>";
    }
    servGet("get_horario", [ano, sala], tenhoHorario);
    $("#horario_form").html(tbody);
    $("#logado").fadeIn();
}

function tenhoHorario(json) {
    if (json["has"]) {
        var horario = JSON.parse(json["horario"]);
        for (var index in selects) {
            var selid = selects[index];
            var idmat = horario[selid];
            $("#" + selid).val(idmat);
        }
    }
}

function clearAll() {
    for (var index in selects) {
        $("#" + selects[index]).val("0");
    }
}

function goSala() {
    if (ano && sala) {
        location.assign("../sala/#" + ano + sala);
    } else {
        window.history.back();
    }
}

function enviar() {
    var arr = {};
    for (var index in selects) {
        var id = selects[index];
        arr[id] = $("#" + id).val();
    }
    var arrstr = JSON.stringify(arr);
    var args = {
        "arr": arrstr,
        "token": getToken()
    }
    servPost("set_horario", args, enviei);
}

function enviei(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        goSala();
    }
}
