var cursos = {};
var ano = getCookie("ano");
var sala = getCookie("sala");

servGet("get_cursos", [], fillDropdown);

$("#nao_logado").hide();

function fillDropdown(json) {
    json.sort(function(a, b){
        return a["nome"].localeCompare(b["nome"]);
    });
    for (var index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var nome = json[index]["nome"];
        var id = json[index]["id_curso"];
        cursos[id] = nome;
        var option = "<option value=\"" + id + "\">" + nome + "</option>";
        $("#curso_select").append(option);
    }
    if (getCookie("last_curso")) {
        $("#curso_select").val(getCookie("last_curso"));
    }
}

function syncPara() {
    $("#para").val($("#para").attr("value"));
}

function setDate(date) {
    var isos = toIso(date);
    $("#para").attr("value", isos);
    syncPara();
}

var now = new Date();
setDate(now);
$("#para").attr("min", toIso(now));

function datadd(n) {
    var added = new Date();
    added.setDate(added.getDate() + n);
    setDate(added);
}

function getNext(n) {
    var date = new Date();
    do {
        date.setDate(date.getDate() + 1);
    } while (date.getDay() != n);
    return date;
}

function prox(n) {
    var date = getNext(n);
    setDate(date);
}

function enviar() {
    var post = {
        "id_curso": $("#curso_select").val(),
        "para": $("#para").val(),
        "info": $("#info").val(),
        "token": getToken()
    }
    if (!post["info"]) {
        err("Digite alguma coisa!");
        return;
    }
    servPost("enviar_licao_curso", post, enviei);
}

function enviei(json) {
    if (json["status"] == "OK") {
        $("#info").val("");
        success("Lição adicionada com sucesso!<br>Adicione mais, ou <a"
            + " href=\"..\">volte para a lista de lições!</a>");
    } else {
        err(json["message"]);
    }
}

function loginVerified(val) {
    if (val === false) {
        $("#logado").hide();
        $("#nao_logado").fadeIn();
    } else {
        if (val["status"] === "OK") {
            $("#nominho").html(val["seu_nome"].split(" ")[0]);
            if (json["seu_ano"] != ano || json["sua_sala"] != sala) {
                setCookie("ano", json["seu_ano"]);
                setCookie("sala", json["sua_sala"]);
                window.location.reload();
            }
            $("#nao_logado").hide();
            $("#logado").fadeIn();
        } else {
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        }
    }
}

checkLoggedIn(loginVerified);
window.addEventListener("focus", function() {
    checkLoggedIn(loginVerified);
});