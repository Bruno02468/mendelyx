var cursos;
var nomescursos = {};
var meu_id = "";


servGet("get_cursos", [], tenhoCursos);

function tenhoCursos(json) {
    cursos = json;
    for (var index in cursos) {
        var curso = cursos[index];
        nomescursos[curso["id_curso"]] = curso["nome"];
        $("#curso_sel").append("<option value=\"" + curso["id_curso"] + "\">"
            + curso["nome"] + "</option>");
    }
    if (getCookie("last_curso")) {
        $("#curso_sel").val(getCookie("last_curso"));
    }
    checkLoggedIn(gotAlunoData);
}

function curset() {
    setCookie("last_curso", $("#curso_sel").val());
    appendLicoes();
}

function gotAlunoData(json_aluno) {
    if (json_aluno !== false) {
        if (json_aluno["status"] === "OK") {
            meu_id = json_aluno["seu_id"];
        }
    }
    servGet("licoes_cursos", [], appendLicoes);
}

var hoje = new Date();
var next = new Date();
do {
    next.setDate(next.getDate() + 1);
} while (next.getDay() == 0 || next.getDay() == 6);
var hojeIso = toIso(hoje);
var nextIso = toIso(next);

function comparadorEntrega(a, b) {
    return Date.parse(a["para"]) - Date.parse(b["para"]);
}

var licoes;
function appendLicoes(json) {
    if (json) {
        licoes = json;
    }
    licoes.sort(comparadorEntrega);
    $("#licoes").hide();
    var licoes_html = "";
    for (var index = 0; index < licoes.length; index++) {
        var licao = licoes[index];
        if (licao["id_curso"] != $("#curso_sel").val()) continue;
        var panelClass = "licao panel panel-";
        var panelTitle = "<b>" + nomescursos[licao["id_curso"]]
            + "</b><br><span class=\"paradia\">";

        if (licao["para"] == hojeIso) {
            panelClass += "success";
            panelTitle += " (para hoje)";
        } else if (licao["para"] == nextIso) {
            panelClass += "danger";
            if (next.getDay() == 1 && hoje.getDay != 0) {
                panelTitle += " (para segunda)"
            } else {
                panelTitle += " (para amanhã)";
            }
        } else {
            panelClass += "default";
            var para = isoToDate(licao["para"]);
            var dia = para.getDate();
            var mes = para.getMonth();
            panelTitle += " (para " + dias_semana[para.getDay()] + ", dia "
                + dia;
            if (para.getMonth() != hoje.getMonth()) {
                panelTitle += " de " + meses[para.getMonth()];
            }
            panelTitle += ")";
        }
        panelTitle += "</span>"
        
        var panelBody = "<span class=\"licao-normal-text\">" 
            + brunoml_to_html(parse_links(licao["info"]))
            + "</span><br><br>"
            + "<div class=\"autorlicao\">Adicionada por ";
        var autor = licao["id_aluno"].toString();
        var id_licao = licao["id_licao"].toString();

        if (autor == meu_id) {
            panelBody += "você.<br><button class=\"btn btn-xs btn-primary\" "
                + "onclick=\"editar('" + id_licao + "')\"><span class=\"glyphi"
                + "con glyphicon-edit iconbut\"></span> Editar</button> "
                + "<button class=\"btn btn-xs btn-danger\" onclick=\"apagar('"
                + id_licao + "', this)\"><span class=\"glyphicon "
                + "glyphicon-remove iconbut\"></span> Apagar</button></div>";

        } else {
            var den = "";
            panelBody += licao["nome_autor"].split(" ")[0] + " (nº "
                + licao["chamada_autor"] + " do " + licao["sala_autor"]
                + ").<br></div>";
        }

        var component = "<div id=\"licao-" + id_licao + "\" class=\""
            + panelClass + "\"><div class=\"panel-heading\">"
            + "<h3 class=\"panel-title licao-normal-text\">" + panelTitle
            + "</h3></div>" + "<div class=\"panel-body\">" + panelBody
            + "</div></div><br>";
        licoes_html += component;
    }
    if (licoes_html == "") {
        licoes_html = "<big>Parece que nenhuma lição foi adicionada, "
            + "<i>ainda</i>.</big>";
    }
    $("#licoes").html(licoes_html);
    $("#licoes").fadeIn();
}

function adicionar() {
    location.assign("adicionar/");
}

function editar(id) {
    location.assign("editar/#" + id);
}

function apagar(id, button) {
    button.innerHTML = "Certeza?"
    button.setAttribute("onclick", "apagarConfirma('" + id + "')");
}

function apagarConfirma(id) {
    servGet("remover_licao_curso", [getToken(), id], apagarConfirmed);
}

function apagarConfirmed() {
    location.reload();
}