var id_licao = location.hash.slice(1);
if (!id_licao) location.assign("..");
servGet("licoes_cursos", [], gotLicoes);

function gotLicoes(json) {
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        var licao = json[key];
        if (licao["id_licao"] == id_licao) {
            $("#curso_select").val(licao["id_curso"]);
            $("#para").val(licao["para"]);
            $("#info").val(licao["info"]);
            return true;
        }
    }
    location.assign("..");
}

function enviar() {
    var post = {
        "id_licao": id_licao,
        "id_curso": $("#curso_select").val(),
        "para": $("#para").val(),
        "info": $("#info").val(),
        "token": getToken()
    }
    if (!post["info"]) {
        err("Digite alguma coisa!");
        return;
    }
    servPost("editar_licao_curso", post, enviei);
}

function enviei(json) {
    if (json["status"] == "OK") {
        success("Mudanças salvas! Voltando para a sala...");
        slowRedir("..");
    } else {
        if (json["message"] == "TOKEN") {
            err("Faça login acima!");
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        } else {
            err(json["message"]);
            slowRedir("..");
        }
    }
}