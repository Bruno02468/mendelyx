var materias;
var ano = $("#ano").val();
var gotid = location.hash.slice(1);
var materias;
var populated = false;

servGet("get_all_materias", [], tenhoMaterias);

function tenhoMaterias(json) {
    materias = json;
    var oldval = $("#mat_select").val();
    $("#mat_select").html("");
    for (var index in materias) {
        var id = materias[index]["id_materia"];
        var nome = materias[index]["nome"];
        $("#mat_select").append("<option value=\"" + id + "\">" + nome
            + "</option>");
    }
    $("#mat_select").val(oldval);
    if (!materias_prova) {
        servGet("get_materias_prova", [], tenhoMateriasProva);
    }
}

function populate(mat) {
    populated = true;
    $("#ano_select").val(mat["ano"]);
    $("#mat_select").val(mat["id_materia"]);
    $("#etapa_select").val(mat["etapa"]);
    $("#periodo_select").val(mat["periodo"]);
    $("#info").val(mat["info"]);
}

function cleanup() {
    $("#info").val("");
}

var materias_prova;
function tenhoMateriasProva(json) {
    materias_prova = json;
    for (var index in json) {
        var mat = json[index];
        if (mat["id_materia_prova"] == gotid) {
            ano = mat["ano"];
            populate(mat);
            break;
        }
    }    
    if (getCookie("ano") && !gotid) {
        ano = getCookie("ano");
        $("#ano_select").val(ano);
    }
    servGet("materias", [ano], tenhoMaterias);
}

function att() {
    var p = false;
    for (var index in materias_prova) {
        var mat = materias_prova[index];
        if (mat["ano"] == ano && mat["id_materia"] == $("#mat_select").val()
            && mat["etapa"] == $("#etapa_select").val()
            && mat["periodo"] == $("#periodo_select").val()) {
            populate(mat);
            p = true;
            break;
        }
    }
    if (!p && populated) {
        populated = false;
        cleanup();
    }
}

function anoatt() {
    ano = $("#ano_select").val();
    servGet("materias", [ano], tenhoMateriasAtt);
}

function tenhoMateriasAtt(json) {
    materias = json;
    att();
}

function enviar() {
    var args = {
        "token": getToken(),
        "ano": ano,
        "etapa": $("#etapa_select").val(),
        "periodo": $("#periodo_select").val(),
        "id_materia": $("#mat_select").val(),
        "info": $("#info").val()
    };
    servPost("set_materia_prova", args, enviei);
}

function enviei() {
    if (json["status"] == "OK") {
        if (gotid) {
            success("Mudanças salvas -- voltando!");
            slowRedir("..");
        } else {
            success("Pronto! Adicione mais, ou <a href=\"..\">volte</a>!");
            servGet("get_materias_prova", [], refreshMateriasProva);
        }
    } else {
        err(json["message"]);
    }
}

function refreshMateriasProva(json) {
    materias_prova = json;
}

function loginVerified(val) {
    if (val === false) {
        $("#logado").hide();
        $("#nao_logado").fadeIn();
    } else {
        if (val["status"] === "OK") {
            $("#nominho").html(val["seu_nome"].split(" ")[0]);
            $("#nao_logado").hide();
            $("#logado").fadeIn();
        } else {
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        }
    }
}

checkLoggedIn(loginVerified);
window.addEventListener("focus", function() {
    checkLoggedIn(loginVerified);
});
