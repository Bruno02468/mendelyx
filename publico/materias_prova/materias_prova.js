var meu_id = "";

checkLoggedIn(gotAlunoData);
function gotAlunoData(json_aluno) {
    if (json_aluno !== false) {
        if (json_aluno["status"] === "OK") {
            meu_id = json_aluno["seu_id"];
        }
    }
    servGet("get_all_materias", [], tenhoMaterias);
}

var nomematerias = {};
function tenhoMaterias(json) {
    for (var index in json) {
        nomematerias[json[index]["id_materia"]] = json[index]["nome"];
    }
    servGet("get_materias_prova", [], tenhoMateriasProva);
}

var materias_prova;
function tenhoMateriasProva(json) {
    materias_prova = json;
    if (getCookie("last_periodo")) {
        $("#periodosel").val(getCookie("last_periodo"));
    }
    if (getCookie("last_etapa")) {
        $("#etapasel").val(getCookie("last_etapa"));
    }
    if (getCookie("last_ano")) {
        $("#anosel").val(getCookie("ano"));
    }
    att();
}

function att() {
    var ano = $("#anosel").val();
    var etapa = $("#etapasel").val();
    var periodo = $("#periodosel").val();
    setCookie("last_ano", ano);
    setCookie("last_etapa", etapa);
    setCookie("last_periodo", periodo);
    var nhtml = "";
    $("#resultados").hide();
    for (var index in materias_prova) {
        var mat = materias_prova[index];
        if (mat["ano"] != ano || mat["etapa"] != etapa || mat["periodo"] != periodo) {
            continue; 
        }
        var panelClass = "licao panel panel-warning";
        var panelTitle = "<b>Prova de " + nomematerias[mat["id_materia"]]
            + "</b><br>";

        var panelBody = "<span class=\"licao-normal-text aleft\">" 
            + brunoml_to_html(parse_links(mat["info"]))
            + "</span><br><br>"
            + "<div class=\"autorlicao\">Alterada por último por ";
        var autor = mat["ultimo_editor"].toString();
        var id_mat = mat["id_materia_prova"].toString();

        if (autor == meu_id) {
            panelBody += "você."
        } else {
            var den = "";
            panelBody += mat["nome_ultimo_editor"].split(" ")[0] + " (nº "
                + mat["chamada_ultimo_editor"] + " do "
                + mat["sala_ultimo_editor"] + ").";
        }
        if (getToken())
            panelBody += "<br><button class=\"btn btn-xs btn-primary\" "
                + "onclick=\"editar('" + id_mat + "')\"><span class=\"glyphi"
                + "con glyphicon-edit iconbut\"></span> Editar</button> ";

        var component = "<div id=\"licao-" + id_mat + "\" class=\""
            + panelClass + "\"><div class=\"panel-heading\">"
            + "<h3 class=\"panel-title licao-normal-text\">" + panelTitle
            + "</h3></div>" + "<div class=\"panel-body\">" + panelBody
            + "</div></div></div><br>";
        nhtml += component;
    }
    if (nhtml == "") {
        nhtml = "Parece que nada foi adicionado aqui por enquanto...";
    }
    $("#resultados").html(nhtml);
    $("#resultados").fadeIn();
}

function setar() {
    location.assign("setar/");
}

function editar(id) {
    location.assign("setar/#" + id);
}
