location.hash = location.hash.toLowerCase();
var miniurl = location.hash.slice(1).toLowerCase();

var materias;
servGet("get_all_materias", [], tenhoMaterias);
function tenhoMaterias(json) {
    materias = json;
    servGet("resumo_full", [miniurl], tenhoResumo);
}

function getMateriaById(id) {
    for (var index in materias) {
        if (materias[index]["id_materia"] == id) {
            return materias[index];
        }
    }
}

function tenhoResumo(json) {
    if (json["status"] == "FAILED") {
        location.assign(json["code"] + ".html");
    } else {
        $("#imprimir").attr("href", "imprimir/" + location.hash);
        $("#imprimir_noimg").attr("href", "imprimir/" + location.hash + "%");
        var resumo = json["resumo"];
        var assunto = resumo["assunto"];
        var ano = resumo["ano"];
        var id_materia = resumo["id_materia"];
        var nome_autor = resumo["nome_autor"];
        var etapa = resumo["etapa"];
        var periodo = resumo["periodo"];
        
        if (etapa == "0") etapa = "todas";
        else etapa += "ª";
        if (periodo == "0") periodo = "todos";
        else periodo += "º";

        $("#autor").text(nome_autor);
        $("#etapa").text(etapa);
        $("#periodo").text(periodo);
        $("#header").html(assunto)
        $(document).prop("title", assunto);
        var materia = getMateriaById(id_materia);
        $("#matlink").append(materia["nome"]);
        $("#matlink").attr("href", "../resumos/#" + ano + "_" + id_materia);
        $("#anolink").attr("href", "../resumos/#" + ano)
        $("#matlink, #anolink").append(" do " + ano + "º");
        $("#resumo").html(brunoml_to_html(resumo["texto"]));

        if (getCookie("semcor")) {
            toggleCor();
        }
    }
}

var dark = false;
var semcor = false;

if (getCookie("dark")) {
    toggleDark();
}

function toggleDark() {
    $("body").toggleClass("darkmode");
    dark = !dark;
    if (dark) {
        setCookie("dark", "on");
    } else {
        deleteCookie("dark");
    }
}

function toggleCor() {
    $(".colored, #resumo *").toggleClass("nocolor");
    semcor = !semcor;
    if (semcor) {
        setCookie("semcor", "on");
    } else {
        deleteCookie("semcor");
    }
}

function editar() {
    if (!getToken()) {
        $("#logar").fadeIn();
    } else {
        servGet("posso_editar", [miniurl, getToken()], tenhoEdit)
    }
}

function tenhoEdit(json) {
    if (json["status"] == "OK") {
        location.assign("../resumos/editar/" + location.hash);
    } else {
        if (json["proibido"]) {
            $("#logar").hide();
            $("#notseu").fadeIn();
        } else {
            $("#logar").fadeIn();
        }
    }
}
