var arr = location.hash.split("%");
if (arr.length < 1) {
    location.assign("..");
}
var miniurl = arr[0].slice(1);
var noimg = arr.length > 1;

var materias;
servGet("get_all_materias", [], tenhoMaterias);
function tenhoMaterias(json) {
    materias = json;
    servGet("resumo_full", [miniurl], tenhoResumo);
}

function getMateriaById(id) {
    for (var index in materias) {
        if (materias[index]["id_materia"] == id) {
            return materias[index];
        }
    }
}

function tenhoResumo(json) {
    if (json["status"] == "FAILED") {
        location.assign("../404.html");
    } else {
        var resumo = json["resumo"];
        var assunto = resumo["assunto"];
        var ano = resumo["ano"];
        var id_materia = resumo["id_materia"];
        var nome_autor = resumo["nome_autor"];
        var etapa = resumo["etapa"];
        var periodo = resumo["periodo"];
        
        if (etapa == "0") etapa = "todas";
        else etapa += "ª";
        if (periodo == "0") periodo = "todos";
        else periodo += "º";

        $("#autor").text(nome_autor);
        $("#etapa").text(etapa);
        $("#periodo").text(periodo);
        $("#header").html(assunto)
        $(document).prop("title", assunto);
        var materia = getMateriaById(id_materia);
        $("#resumo").html(brunoml_to_html(resumo["texto"]));
        $(".colored, #resumo *").toggleClass("nocolor");
        $("a").css("color", "black", true);
        if (noimg) {
            $("img").each(function(index, element) {
                $(element).parent().css("color", "black");
                $(element).replaceWith("<b>[Imagem removida. URL: "
                    + $(element).attr("src") + "]</b>");
            });
        }
    }
}
