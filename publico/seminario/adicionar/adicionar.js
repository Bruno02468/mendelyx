function generic_do() {
    $(".wait_do").fadeIn();
    $("#buttons").hide();
}

function do_link() {
    generic_do();
    $("#is_link").fadeIn();
}


function do_file() {
    generic_do();
    $("#is_file").fadeIn();
}

function loginVerified(val) {
    if (val === false) {
        $("#logado").hide();
        $("#nao_logado").fadeIn();
    } else {
        if (val["status"] === "OK") {
            $("#nominho").html(val["seu_nome"].split(" ")[0]);
            $("#nao_logado").hide();
            $("#logado").fadeIn();
        } else {
            $("#logado").hide();
            $("#nao_logado").fadeIn();
        }
    }
}

var fname;
var ftype;

function enviar() {
    var args = {
        "token": getToken(),
        "tema": $("#tema").val(),
        "link": $("#url").val(),
        "tem_arquivo": false,
        "arquivo": null
    };
    if ($("#file")[0].files[0]) {
        var file = $("#file")[0].files[0];
        fname = file["name"];
        ftype = file["type"];
        var reader = new FileReader();
        reader.onload = function(e) {
            var data = e.target.result;
            $("#send").val("Fazendo upload (isso pode demorar!)");
            enviarComArquivo(args, data);
        };
        reader.onprogress = function(e) {
            var percentage = 100*Math.ceil(e.loaded/e.total);
            $("#send").attr("onclick", "");
            $("#send").val("Carregando arquivo (" + percentage + "%)");
        };
        reader.readAsBinaryString(file);
    } else {
        servPost("criar_seminario", args, enviei);
    }
    return false;
}

function enviarComArquivo(args, dados) {
    if (dados.length > 20000000) {
        err("Arquivo acima do limite de tamanho (20 MB)!");
        return;
    }
    args["tem_arquivo"] = true;
    args["arquivo"] = JSON.stringify({
        "name": fname,
        "type": ftype,
        "contents": makeBase64URLSafe(btoa(dados))
    });
    servPost("criar_seminario", args, enviei);
}

function enviei(json) {
    $("#send").attr("onclick", "enviar()");
    $("#send").text("Enviar!");
    if (json["status"] == "OK") {
        success("Enviado com sucesso! Voltando...");
        slowRedir("..");
    } else {
        err(json["message"]);
    }
}

checkLoggedIn(loginVerified);
window.addEventListener("focus", function() {
    checkLoggedIn(loginVerified);
});
