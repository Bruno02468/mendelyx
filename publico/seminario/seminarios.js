var meu_id = "";

for (var index in validsalas) {
    var sala = validsalas[index];
    if (sala[0] != "2") continue;
    $("#salasel").append("<option value=\"" + sala[1] + "\">2º " + sala[1]
        + "</option>");
}

if (getCookie("sala") && getCookie("ano") == "2") {
    $("#salasel").val(getCookie("sala"));
}

if (!getToken()) {
    $("#send_btn").hide();
    servGet("get_seminarios", [], tenhoSeminarios);
} else {
    checkLoggedIn(got_sala);
}

function adicionar() {
    location.assign("adicionar/")
}

function got_sala(json) {
    if (json["status"] == "OK") {
        meu_id = "" + json["seu_id"];
        if (json["seu_ano"] == "2") {
            $("#salasel").val(json["sua_sala"]);
        } else {
            $("#send_btn").hide();
        }
    }
    servGet("get_seminarios", [], tenhoSeminarios);
}

var seminarios = null;
function tenhoSeminarios(json) {
    seminarios = json;
    sala_att();
}

function sala_att() {
    if (!seminarios) return;
    $("#resultados").hide();
    $("#resultados").html("");
    var nhtml = "";
    var wanted = $("#salasel").val();
    for (var index in seminarios) {
        var seminario = seminarios[index];
        if (wanted != "todas" && wanted != seminario["sala"]) continue;
        var panelClass = "licao panel panel-info";
        var panelTitle = "<b>Tema: \"<i>" + seminario["tema"]
            + "</i>\"</b><br>";
        
        var actual = "";
        if (seminario["id_arquivo"]) {
            var filename = seminario["arquivo"]["filename"];
            var hash = seminario["arquivo"]["hash"];
            var url = "../uploads/" + hash + "/" + encodeURIComponent(filename);
            actual = "Contém arquivo: <b><a target=\"_blank\" href=\"" + url
                + "\">" + escapeHTML(filename) + "</a></b>";
        } else {
            actual = "Contém link: <b>" + parse_links(seminario["link"])
                + "</b>";
        }

        var panelBody = "<span class=\"licao-normal-text aleft\">" 
            + actual + "</span><br>"
            + "<div class=\"autorlicao\">Enviada por ";

        var autor = seminario["id_aluno"].toString();
        var id_seminario = seminario["id_seminario"].toString();
        
        if (autor == meu_id) {
            panelBody += "você."
                + "<br><button class=\"btn btn-xs btn-primary\" "
                + "onclick=\"remover('" + id_seminario + "')\"><span class=\"glyphi"
                + "con glyphicon-remove iconbut\"></span> Remover</button> ";
        } else {
            var den = "";
            panelBody += seminario["nome_autor"] + ".";
        }

        var component = "<div id=\"seminario-" + id_seminario + "\" class=\""
            + panelClass + "\"><div class=\"panel-heading\">"
            + "<h3 class=\"panel-title licao-normal-text\">" + panelTitle
            + "</h3></div>" + "<div class=\"panel-body\">" + panelBody
            + "</div></div></div><br>";
        nhtml += component;
    }
    $("#resultados").html(nhtml);
    $("#resultados").fadeIn();
}

function remover(id) {
    if (confirm("Deseja mesmo remover seu seminário?"))
        servGet("remover_seminario", [getToken(), id], removi);
}

function removi(json) {
    if (json["status"] == "OK") {
        location.reload();
    } else {
        alert(json["message"]);
    }
}
