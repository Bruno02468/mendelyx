servGet("get_reforcos", [], dataReady);

var reforcos = [];
var mats = [];
function dataReady(json) {
    reforcos = json;
    for (var index in json) {
        var mat = json[index]["nomemateria"];
        if (mats.indexOf(mat) > -1) continue;
        $("#matsel").append("<option value=\"" + mat + "\">" + mat
            + "</option>");
        mats.push(mat);
    }
    if (getCookie("ano")) {
        updateLista();
    }
    $("#carregando").hide();
    $("#carregado").fadeIn();
}

function updateLista() {
    var choice = $("#diasel").val();
    var mat = $("#matsel").val();
    var ano = $("#ref_ano").val();
    setCookie("ano", ano);
    var iso = $("#esp").val();
    if (choice == "esp") {
        $("#espData").fadeIn();
        if (!iso) {
            return false;
        }
    } else {
        $("#espData").fadeOut();
        var data = new Date();
        switch (choice) {
            case "amanha": data.setDate(data.getDate() + 1); break;
            case "semana": data.setDate(data.getDate() + 7); break;
        }
        iso = toIso(data);
    }
    var lista = "";
    for (var index in reforcos) {
        var reforco = reforcos[index];
        if (mat != "any") {
            if (reforco["nomemateria"] != mat) continue;
        }
        var dias = JSON.parse(reforco["dias_array"]);
        if (choice != "any") {
            if (dias.indexOf(iso) < 0) continue;
        }
        lista += "<tr><td>" + reforco["nomemateria"] + "</td><td>"
            + reforco["professor"] + "</td><td>" + reforco["hora_inicio"]
            + " - " + reforco["hora_fim"] + "</td><td>" + reforco["sala"]
            + "</td></tr>";
    }
    $("#listar").html(lista);
    if (lista != "") {
        $("#semnada").hide();
        $("#resultados").fadeIn();
    } else {
        $("#resultados").hide();
        var novodia = "esse dia!";
        switch ($("#diasel").val()) {
            case "hoje": novodia = "hoje"; break;
            case "amanha": novodia = "amanhã"; break;
            case "any": novodia = "dia algum"; break;
            case "esp": novodia = dateToBarras(isoToDate($("#esp").val()));
                break; 
        }
        if ($("#matsel").val() != "any") {
            var nomemat = $("#matsel option:selected").text();
            $("#nomemat").text(" de " + nomemat);
        } else {
            $("#nomemat").html("");
        }
        $("#nomedia").html(novodia);
        $("#semnada").fadeIn();
    }
}
