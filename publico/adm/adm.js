var user;
var senha;

function admlogin() {
    var args = {
        "user": $("#admuser").val(),
        "senha": $("#admpass").val()
    }
    servPost("login_adm", args, gotLogin);
    user = args["user"];
    senha = args["senha"];
    return false;
}

function gotLogin(json) {
    if (json["status"] == "OK") {
        $("#admlogin").hide();
        $("#loggedIn").fadeIn();
        err("");
    } else {
        err(json["message"]);
    }
}

var botoes = {
    "startVotacao": "Iniciar a votação dos calendários",
    "banAluno": "Banir/desbanir um aluno",
    //"gprofs": Gerenciar Professores,
    //"reforcos": Gerenciar Reforços,
    "motd": "Mensagem no topo do site",
    "iniciais": "Links da página inicial",
    "aprovacoes": "Aprovações",
    "pesquisar": "Pesquisar alunos",
    "resetar": "Resetar um aluno",
    //"regexes": "Gerenciar regexes",
    "extrair": "Extrair nomes de listas de aprovação",
    "mapas": "Gerenciar mapas de sala de prova"
};

for (var key in botoes) {
    if (!botoes.hasOwnProperty(key)) continue;
    $("#botoesholder").append("<button class=\"btn btn-primary\" onclick=\""
        + "showAcao('" + key + "')\">" + botoes[key] + "</button><br>");
}

var shownAcao;
var shownNome;
function showAcao(nome) {
    if (shownAcao)
        shownAcao.hide();
    if (shownNome == nome) {
        shownAcao = null;
        shownNome = null;
        return false;
    }
    shownAcao = $("#" + nome);
    shownNome = nome;
    $("#botoesholder").hide();
    $("#voltar").fadeIn();
    shownAcao.fadeIn();
}

function volta() {
    shownAcao.hide();
    shownAcao = null;
    shownNome = null;
    $("#voltar").hide();
    $("#botoesholder").fadeIn();
}

var votDias = 4;

function addDiaProva() {
    votDias++;
    $("#dias_prova").append("<input class=\"form-control\" type=\"date\" "
        + "id=\"prova" + votDias + "\"><br>");
}

function removeDiaProva() {
    if (votDias == 1) return false;
    $("#prova" + votDias).remove();
    $("#dias_prova br:last-child").remove();
    votDias--;
}

function iniciarVotacao() {
    var args = {
        "etapa": $("#etapa").val(),
        "periodo": $("#periodo").val(),
        "primeiro_dia": $("#primeiroDia").val(),
        "ultimo_dia": $("#ultimoDia").val(),
        "dias": [],
        "neo": $("#usar_neo").is(":checked"),
        "responsavel_neo_matricula": $("#repneo_matricula").val(),
        "user": user,
        "senha": senha
    };
    for (key in args) {
        if (!args.hasOwnProperty(key)) continue;
        if (key == "responsavel_neo_matricula") continue;
        if (!args[key]) {
            err("Preencha todos os campos!");
            return false;
        }
    }
    for (var i = 1; i <= votDias; i++) {
        args["dias"].push($("#prova" + i).val());
    }
    args["dias"] = JSON.stringify(args["dias"]);

    err("");
    servPost("iniciar_votacao", args, inicieiVotacao);
    return false;
}

function inicieiVotacao(json) {
    if (json["status"] == "OK") {
        volta();
        success(json["message"]);
    } else {
        err(json["message"]);
    }
}

function banirAluno() {
    var args = {
        "matricula": $("#banmat").val(),
        "user": user,
        "senha": senha
    }
    servPost("toggle_banido", args, baniAluno);
    return false;
}

function baniAluno(json) {
    if (json["status"] == "OK") {
        success(json["message"]);
    } else {
        err(json["message"]);
    }
}

var materias;
servGet("get_all_materias", [], tenhoMaterias);

function tenhoMaterias(json) {
    materias = json;
    for (var key in materias) {
        if (!materias.hasOwnProperty(key)) continue;
        var mat = materias[key];
        var component = "<option value=\"" + mat["id_materia"] + "\">"
            + mat["nome"] + "</option>";
        $("#prof_materia").append(component);
    }
    servGet("get_professores", [], popularProfessores);
}

function makeProfessorLine(professor) {
    var id_professor = professor["id_professor"];
    var nome = escapeHTML(professor["nome"]);
    var id_materia = professor["id_materia"];
    var nome_materia;
    for (var j in materias) {
        if (!materias.hasOwnProperty(j)) continue;
        var mat = materias[j];
        if (mat["id_materia"] == id_materia) {
            nome_materia = mat["nome"];
            break;
        }
    }
    var edit_btn = "<span class=\"glyphicon glyphicon-pencil imgbtn\" "
        + "onclick=\"showEditProfessor('" + id_professor + "')\"></span>";
    var del_btn = "<span class=\"glyphicon glyphicon-trash imgbtn\" "
        + "onclick=\"removeProfessor('" + id_professor + "')\"></span>";
    var component = "<td> " + nome + "</td><td>" + nome_materia
        + "</td><td>?????</td><td>" + edit_btn + "</td><td>" + del_btn
        + "</td></tr>";
    return component;
}

var professores;
function popularProfessores(json) {
    professores = json;
    $("#professores").html("");
    for (var key in json) {
        if (!json.hasOwnProperty(key)) continue;
        var professor = json[key];
        var id_professor = professor["id_professor"];
        var component = "<tr class=\"professor\" id=\"professor_"
        + id_professor + "\">" + makeProfessorLine(professor) + "</tr>";
        $("#professores").append(component);
    }
}

function addProfessor() {
    var nome = $("#prof_nome").val();
    var id_mat = $("#prof_materia").val();
    var prof_senha = $("#prof_senha").val();
    var args = {
        "user": user,
        "senha": senha,
        "nome": nome,
        "senha_prof": prof_senha,
        "id_materia": id_mat
    }
    servPost("add_professor", args, adicioneiProfessor);
    return false;
}

function adicioneiProfessor(json) {
    var nome = $("#prof_nome").val("");
    var prof_senha = $("#prof_senha").val("");
    professorUpdate(json);
}

function showEditProfessor(id_professor) {
    var professor;
    for (var key in professores) {
        if (!professores.hasOwnProperty(key)) continue;
        var prof = professores[key];
        if (prof["id_professor"] == id_professor) {
            professor = prof;
            break;
        }
    }
    var nome_field = "<td><input type=\"text\" id=\"editing_" + id_professor
        + "_nome\" value=\"" + prof["nome"] + "\" class=\"form-control\"></td>";
    var materia_field = "<td><select id=\"editing_" + id_professor + "_materia"
        + "\" value=\"" + prof["id_materia"] + "\" class=\"form-control\">"
        + $("#prof_materia").html() + "</select></td>";
    var senha_field = "<td><input type=\"password\" id=\"editing_"
        + id_professor + "_senha\" class=\"form-control\"></td>";
    var save_btn = "<td><span class=\"glyphicon glyphicon-ok imgbtn\""
        + "onclick=\"sendEditProfessor('" + id_professor + "')\"></span></td>";
    var cancel_btn = "<td><span class=\"glyphicon glyphicon-remove imgbtn\""
        + "onclick=\"resetProfessor('" + id_professor + "')\"></span></td>";
    $("#professor_" + id_professor).html(nome_field + materia_field
        + senha_field + save_btn + cancel_btn);
    $("#editing_" + id_professor + "_materia").val(professor["id_materia"]);
}

function sendEditProfessor(id_professor) {
    var nome = $("#editing_"+ id_professor + "_nome").val();
    var id_materia = $("#editing_" + id_professor + "_materia").val();
    var senha_prof = $("#editing_" + id_professor + "_senha").val();
    var args = {
        "user": user,
        "senha": senha,
        "id_professor": id_professor,
        "nome": nome,
        "senha_prof": senha_prof,
        "id_materia": id_materia
    }
    servPost("edit_professor", args, editeiProfessor);
}

function editeiProfessor(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        professores = json["professores"];
        id_professor = json["id_professor"];
        resetProfessor(id_professor);
        err("");
    }
}

function resetProfessor(id_professor) {
    for (var key in professores) {
        if (!professores.hasOwnProperty(key)) continue;
        var prof = professores[key];
        if (prof["id_professor"] == id_professor) {
            $("#professor_" + id_professor).html(makeProfessorLine(prof));
            break;
        }
    }
}

function removeProfessor(id_professor) {
    var args = {
        "user": user,
        "senha": senha,
        "id_professor": id_professor
    }
    servPost("remove_professor", args, removiProfessor);
}

function removiProfessor(json) {
    if (json["status"] == "OK") {
        err("");
        $("#professor_" + json["id_removido"]).remove();
    } else {
        err(json["message"]);
    }
}

function professorUpdate(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        popularProfessores(json["professores"]);
        err("");
    }
}

function addDia(container_id) {
    var vals = "";
    var dias = getDias(container_id);
    if (dias.length > 0) {
        vals = " value=\"" + dias[dias.length-1] + "\"";
    }
    $("#" + container_id).append("<input type=\"date\" class=\"form-control\""
        + vals + "><br>");
}

function popDia(container_id) {
    $("#" + container_id + " br:last-child").remove();
    $("#" + container_id + " input:last-child").remove();
}

var reforcos = {};

function makeReforcoLine(reforco) {
    return "<td>" + reforco["ano"] + "º</td><td>" + reforco["nomemateria"]
        + "</td><td>" + reforco["professor"] + "</td><td>" + reforco["sala"]
        + "</td><td>" + reforco["hora_inicio"] + " - " + reforco["hora_fim"]
        + "</td><td>" + JSON.parse(reforco["dias_array"]).length + " dia(s)"
        + "</td><td><span class=\"glyphicon glyphicon-pencil imgbtn\" onclick="
        + "\"editReforco('" + reforco["id_reforco"] + "')\"></span></td><td>"
        + "<span class=\"glyphicon glyphicon-trash imgbtn\" onclick=\"remove"
        + "Reforco('" + reforco["id_reforco"] + "')\"></td>";
}


function updateReforcos(json) {
    reforcos = {};
    for (var index in json) {
        var reforco = json[index];
        reforco["dias"] = JSON.parse(reforco["dias_array"]);
        reforcos[reforco["id_reforco"]] = reforco;
    }
}

function populateReforcos(json) {
    $("#listarefs").html("");
    reforcos = {};
    for (var index in json) {
        var reforco = json[index];
        reforco["dias"] = JSON.parse(reforco["dias_array"]);
        $("#listarefs").append("<tr id=\"reforco_" + reforco["id_reforco"]
            + "\">" + makeReforcoLine(reforco) + "</tr>");
        reforcos[reforco["id_reforco"]] = reforco;
    }
}

function resetReforcos() {
    servGet("get_reforcos", [], populateReforcos);
}

resetReforcos();

function getDias(container_id) {
    var inputs = $("#" + container_id + " input");
    var vals = [];
    for (var index = 0; index < inputs.length; index++) {
        var input = $(inputs[index]);
        vals.push(input.val());
    }
    return vals;
}

function addReforco() {
    var args = {
        "user": user,
        "senha": senha,
        "hora_inicio": $("#ref_inicio").val(),
        "hora_fim": $("#ref_fim").val(),
        "sala": $("#ref_sala").val(),
        "ano": $("#ref_ano").val(),
        "nomemateria": $("#ref_mat").val(),
        "professor": $("#ref_prof").val(),
        "dias_array": JSON.stringify(getDias("ref_dias"))
    };
    servPost("add_reforco", args, adicioneiReforco);
    return false;
}

function adicioneiReforco() {
    if (json["status"] == "OK") {
        resetReforcos();
        err("");
    } else {
        err(json["message"]);
    }
}

function editReforco(id_reforco) {
    var reforco = reforcos[id_reforco];
    if (!reforco) return false;
    var ano_edit = "<td><select id=\"ref_" + id_reforco + "_ano\" class=\""
        + "form-control\" value=\"" + reforco["ano"]
        + "\"><option value=\"1\">1º</option>"
        + "<option value=\"2\">2º</option><option value=\"3\">3º</option>"
        + "</select></td>";
    var mat_edit = "<td><input type=\"text\" class=\"form-control\" value=\""
        + reforco["nomemateria"] + "\" id=\"ref_" + id_reforco + "_mat\">"
        + "</td>";
    var prof_edit = "<td><input type=\"text\" class=\"form-control\" value=\""
        + reforco["professor"] + "\" id=\"ref_" + id_reforco + "_prof\">"
        + "</td>";
    var sala_edit = "<td><input type=\"text\" class=\"form-control\" value=\""
        + reforco["sala"] + "\" id=\"ref_" + id_reforco + "_sala\">"
        + "</td>";
    var horario_edit = "<td><input type=\"text\" id=\"ref_" + id_reforco
        + "_inicio\" class=\"form-control hor_in\" value=\""
        + reforco["hora_inicio"] + "\"> - <input type=\"text\" id=\"ref_"
        + id_reforco + "_fim\" " + "value=\"" + reforco["hora_fim"]
        + "\" class=\"form-control hor_in\"></td>";
    var did = "ref_" + id_reforco + "_dias";
    var dias = "";
    for (var index in reforco["dias"]) {
        dias += "<input type=\"date\" class=\"form-control\" value=\""
            + reforco["dias"][index] + "\">";
    }
    var dias_edit = "<td>"
        + "<button onclick=\"addDia('" + did + "'); return false\" "
        + "class=\"btn btn-success\">"
        + "<span class=\"glyphicon glyphicon-plus\"></span>"
        + "</button>"
        + "<button onclick=\"popDia('" + did + "'); return false\" "
        + "class=\"btn btn-danger\">"
        + "<span class=\"glyphicon glyphicon-minus\"></span>"
        + "</button><div id=\"" + did + "\">" + dias + "</div></td>";
    var salvar = "<td><span class=\"glyphicon glyphicon-floppy-disk imgbtn\" "
        + "onclick=\"sendEditReforco('" + id_reforco + "')\"></span></td>";
    var cancelar = "<td><span class=\"glyphicon glyphicon-remove imgbtn\" "
        + "onclick=\"restoreReforco('" + id_reforco + "')\"></span></td>";
    $("#reforco_" + id_reforco).html(ano_edit + mat_edit + prof_edit
        + sala_edit + horario_edit + dias_edit + salvar + cancelar);
}

function restoreReforco(id) {
    var reforco = reforcos[id];
    if (!reforco) return false;
    $("#reforco_" + id).html(makeReforcoLine(reforco));
}

function sendEditReforco(id) {
    var args = {
        "user": user,
        "senha": senha,
        "hora_inicio": $("#ref_" + id + "_inicio").val(),
        "hora_fim": $("#ref_" + id + "_fim").val(),
        "sala": $("#ref_" + id + "_sala").val(),
        "ano": $("#ref_" + id + "_ano").val(),
        "nomemateria": $("#ref_" + id + "_mat").val(),
        "professor": $("#ref_" + id + "_prof").val(),
        "dias_array": JSON.stringify(getDias("ref_" + id + "_dias")),
        "id_reforco": id
    };
    servPost("edit_reforco", args, envieiEditReforco);
}

function envieiEditReforco(json) {
    if (json["status"] == "OK") {
        err("");
        updateReforcos(JSON.parse(json["reforcos"]));
        restoreReforco(json["id_editado"]);
    } else {
        err(json["message"]);
    }
}

function removeReforco(id) {
    var args = {
        "user": user,
        "senha": senha,
        "id_reforco": id
    }
    servGet("remove_reforco", args, removiReforco);
}

function removiReforco(json) {
    if (json["status"] == "OK") {
        $("#reforco_" + json["id_removido"]).remove();
        err("");
    } else {
        err(json["message"]);
    }
}

servGet("motd", [], function(s){$("#motd_in").val("" + s);});

function enviarMOTD() {
    var args = {
        "user": user,
        "senha": senha,
        "motd": $("#motd_in").val()
    };
    servPost("send_motd", args, envieiMOTD);
    return false;
}

function envieiMOTD(json) {
    if (json["status"] == "OK") {
        success("Mensagem alterada com sucesso!");
        $(".niver").remove();
        servGet("motd", [], motd);
    } else {
        err(json["message"]);
    }
}

function preencher_iniciais(json) {
    for (index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var inicial = json[index];
        var id_link = inicial["id_link"];
        var link = inicial["link"];
        var visivel = !!inicial["visivel"];
        var nome = inicial["nome"];
        var checked = "";
        if (visivel) checked = " checked";
        var elem = "<label><input type=\"checkbox\" onclick=\"updateVisivel("
            + id_link + ", this)\"" + checked + "> " + nome + "</label><br>";
        $("#iniciais_lista").append(elem);
    }
}

function updateVisivel(id_link, elem) {
    var checked = $(elem).is(":checked");
    var args = {
        "user": user,
        "senha": senha,
        "id_link": id_link,
        "visivel": +checked
    };
    servPost("set_inicial", args, noop);
}

servGet("get_iniciais", [], preencher_iniciais);


function aprov_relatorio() {
    var uargs = {
        "user": user,
        "senha": senha
    };
    servPost("relatorio_aprovacoes", uargs, tenhoRelatorio);
}

var nodl = false;
function aprov_planilha(ndl) {
    nodl = !!ndl;
    servGet("ano_diretor", [], tenhoAnoDiretor);
}

var ano_diretor = null;
function tenhoAnoDiretor(json) {
    ano_diretor = json["ano_diretor"];
    var uargs = {
        "user": user,
        "senha": senha
    };
    servPost("todas_aprovacoes_aluno", uargs, tenhoParaPlanilha);
}

function reLine(n, t) {
    return n + ": " + t + "<br>";
}

function tenhoRelatorio(json) {
    var style = "font-family: monospace; white-text: pre;";
    var text = reLine("<br>Aprovações em públicas", json["aprovacoes_publicas"])
        + reLine("Aprovações em particulares", json["aprovacoes_privadas"])
        + reLine("Aprovações \"meio de ano\"", json["aprovacoes_mda"])
        + reLine("Aprovações via Sisu", json["aprovacoes_via_sisu"])
        + reLine("Total de aprovações", json["total_aprovacoes"]) + "<br>"
        + reLine("Aprovados \"pra valer\"", json["aprovados_pra_valer"])
//        + reLine("Aprovados como treineiros", json["aprovados_treineiros"])
        + "Aprovados por ano:<br>"
        + reLine("1º ano", json["aprovados_por_ano"][1])
        + reLine("2º ano", json["aprovados_por_ano"][2])
        + reLine("3º ano", json["aprovados_por_ano"][3])
        + reLine("Total de aprovados", json["total_aprovados"]);
    $("#relatorio_aprovs").html(text);
    $("#relatorio_aprovs").fadeIn();
}

function aprovacao_linha(aprovacao) {
    var base = aprovacao["nome_uni"] + " - " + aprovacao["nome_curso"];
    var paren = "";
    var coloc = "";
    if (aprovacao["publica"] && aprovacao["treineiro"]) {
        paren = " (pública, treineiro)";
    } else if (aprovacao["publica"]) {
        paren = " (pública)";
    } else if (aprovacao["treineiro"]) {
        paren = " (treineiro)";
    }
    if (parseInt(aprovacao["rank"]) != 0) {
        coloc = " (" + aprovacao["rank"] + "º colocado)";
    }
    return base + paren + coloc + "<br>";
}

function tenhoParaPlanilha(json) {
    var thead = "<thead>"
        + "<th>Nome (no sistema)</th>"
        + "<th>Nome (corrigido)</th>"
        + "<th>Sala</th>"
        + "<th>Ano escolar</th>"
        + "<th>Instituição</th>"
        + "<th>Curso</th>"
        + "<th>Carreira</th>"
        + "<th>Colocação</th>"
        + "<th>Pública</th>"
        + "<th>Data da prova</th>"
        + "<th>Via Enem</th>"
//      + "<th>Treineiro</th>"
        + "</thead>";
    var tbody = "<tbody>";
    for (var index in json) {
        if (!json.hasOwnProperty(index)) continue;
        var aluno = json[index];
        var aprovacoes = aluno["aprovacoes"];
        for (var i in aprovacoes) {
            var aprovacao = aprovacoes[i];
            var nome_td = "<td>" + aluno["nome"] + "</td>";
            var corr_td = "<td>" + aluno["correcao_nome"] + "</td>";
            var sala_td = "<td>" + aluno["ano"] + "º " + aluno["sala"] + "</td>";
            var ano_td = "<td>" + aluno["em"] + "</td>"
            var inst_td = "<td>" + aprovacao["nome_uni"] + "</td>";
            var carreira_td = "<td>" + aprovacao["nome_curso"] + "</td>";
            var categoria_td = "<td>" + aprovacao["categoria"] + "</td>";
            var col = aprovacao["rank"] + "º";
            if (aprovacao["rank"] == 0) {
                col = "[Sem colocação]";
            }
            var colocacao_td = "<td>" + col + "</td>";
            var publica_td = "<td>" + sn(aprovacao["publica"]) + "</td>";
            mda = "Vestibular " + ano_diretor;
            if (aprovacao["meio_de_ano"])
                mda = "Meio de ano " + (ano_diretor-1);
            var mda_td = "<td>" + mda + "</td>";
            var sisu_td = "<td>" + sn(aprovacao["sisu"]) + "</td>";
            //var treineiro_td = "<td>" + sn(aprovacao["treineiro"]) + "</td>";
            var treineiro_td = "";
            tbody += "<tr>" + nome_td + corr_td + sala_td + ano_td + inst_td
                + carreira_td + categoria_td + colocacao_td + publica_td
                + mda_td + sisu_td + treineiro_td +  "</tr>";
        }
    }
    tbody += "</tbody>";
    var table = "<table>" + thead + tbody + "</table>";
    if (nodl) {
        document.write("<html><head><link rel=\"stylesheet\""
            + "href=\"../css/planilha.css\"></head><body><br>" + table
            + "</body></html>");
        return;
    }
    var planilha = btoa(table);
    var lel = document.createElement("a");
    var data_type = "data:application/vnd.ms-excel";
    lel.href = data_type + ";base64," + planilha;
    lel.download = "planilha_aprovacoes_" + ano_diretor + ".xls";
    lel.click();
}

function pesquisarAlunos() {
    var args = {
        "user": user,
        "senha": senha,
        "search_by": $("#query_param").val(),
        "query": $("#alunos_query").val()
    };
    servPost("query_alunos", args, tenhoAlunosPesquisados);
    return false;
}

$("#alunos_table").hide();
function tenhoAlunosPesquisados(json) {
    $("#alunos_encontrados").html("");
    $("#ninguem_encontrado, #alunos_table").hide();
    if (json.length == 0) {
        $("#ninguem_encontrado").fadeIn();
        return;
    }
    var trs = "";
    for (var index in json) {
        var aluno = json[index];
        var nome_td = "<td>" + aluno["nome"] + "</td>";
        var sala_td = "<td>" + aluno["ano"] + "º " + aluno["sala"] + "</td>";
        var num_td = "<td>" + aluno["chamada"] + "</td>";
        var reg_td = "<td>" + sn(aluno["registrado"]) + "</td>";
        var ban_td = "<td>" + sn(aluno["banido"]) + "</td>";
        var email_td = "<td>" + (aluno["email"] || "N/A") + "</td>";
        var lic_td = "<td>" + aluno["licoes"] + "</td>";
        var res_td = "<td>" + aluno["resumos"] + "</td>";
        trs += "<tr>" + nome_td + sala_td + num_td + reg_td + ban_td
            + email_td + lic_td + res_td;
    }
    $("#alunos_encontrados").html(trs);
    $("#alunos_table").fadeIn();
}

function resetarAluno() {
    var args = {
        "user": user,
        "senha": senha,
        "matricula": $("#reset_login").val()
    };
    servPost("resetar_aluno", args, reseteiAluno);
    return false;
}

function reseteiAluno(json) {
    if (json["status"] == "OK") {
        success(json["message"]);
        volta();
    } else {
        err(json["message"]);
    }
}

var regexes = null;
var regex_vars = ["nome", "detalhes", "pattern"];
servGet("regexes", [], updateRegexes);

function getRegexById(id_regex) {
    for (var index in regexes) {
        var regex = regexes[index];
        if (regex["id_regex"] == id_regex) {
            return regex;
        }
    }
}

function makeLinhaRegex(regex) {
    return "<td>" + regex["nome"] + "</td><td>" + regex["detalhes"]
        + "</td><td>" + regex["pattern"] + "</td><td><a href=\"javascript:void"
        + "(0)\" onclick=\"editarRegex('" + regex["id_regex"] + "')\">Editar"
        + "</a></td><td><a href=\"javascript:void(0)\" onclick=\"removerRegex('"
        + regex["id_regex"] + "')\">Remover</a></td>";
}

function makeRegexEdit(regex) {
    var i = regex["id_regex"];
    var n = regex["nome"];
    var d = regex["detalhes"];
    var p = regex["pattern"];
    var begin = " class=\"form-control\" type=\"text\" id=\"redit_"
    var i_nome = "<input" + begin + i + "_nome\">";
    var i_det = "<textarea" + begin + i + "_detalhes\"></textarea>";
    var i_pat = "<input" + begin + i + "_pattern\">";

    return "<td>" + i_nome + "</td><td>" + i_det + "</td><td>" + i_pat
        + "</td><td><a href=\"javascript:void"
        + "(0)\" onclick=\"salvarRegex('" + regex["id_regex"] + "')\">Salvar"
        + "</a></td><td><a href=\"javascript:void(0)\" onclick=\"resetarRegex('"
        + regex["id_regex"] + "')\">Cancelar</a></td>";
}

function updateRegexes(json) {
    regexes = json;
    $("#all_regexes").html("");
    for (var index in regexes) {
        var regex = regexes[index];
        $("#all_regexes").append("<tr id=\"regex_" + regex["id_regex"] + "\">"
            + makeLinhaRegex(regex) + "</tr>");
    }
    updateRegexSelect();
}

function editarRegex(id_regex) {
    var regex = getRegexById(id_regex);
    $("#regex_" + id_regex).html(makeRegexEdit(regex));
    regex_vars.map(function(varname) {
        $("#redit_" + id_regex + "_" + varname).val(regex[varname]);
    });
}

function removerRegex(id_regex) {
    var dargs = {
        "user": user,
        "senha": senha,
        "id_regex": id_regex
    };
    servPost("remove_regex", dargs, removiRegex);
}

function removiRegex(json) {
    if (json["status"] == "OK") {
        $("#regex_" + json["id_removida"]).remove();
        err("");
        regexes = json["regexes"];
        updateRegexSelect();
    } else {
        err(json["message"]);
    }
}

function salvarRegex(id_regex) {
    var eargs = {
        "user": user,
        "senha": senha,
        "id_regex": id_regex
    };
    regex_vars.map(function(varname) {
        eargs[varname] = $("#redit_" + id_regex + "_" + varname).val();
    });
    servPost("edit_regex", eargs, editeiRegex);
}

function editeiRegex(json) {
    if (json["status"] == "OK") {
        err("");
        regexes = json["regexes"];
        updateRegexSelect();
        var id_editada = json["id_editada"];
        var novaLinha = makeLinhaRegex(getRegexById(id_editada));
        $("#regex_" + id_editada).html(novaLinha);
    } else {
        err(json["message"]);
    }
}

function resetarRegex(id_regex) {
    var regex = getRegexById(id_regex);
    $("#regex_" + id_regex).html(makeLinhaRegex(regex));
}

function enviarRegex() {
    var args = {
        "user": user,
        "senha": senha
    };
    regex_vars.map(function(varname) {
        args[varname] = $("#radd_" + varname).val();
    });
    servPost("add_regex", args, envieiRegex);
    return false;
}

function envieiRegex(json) {
    if (json["status"] == "OK") {
        err("");
        regexes = json["regexes"];
        updateRegexSelect();
        updateRegexes(regexes);
        regex_vars.map(function(varname) {
            $("#radd_" + varname).val("");
        });
    } else {
        err(json["message"]);
    }
}

function updateRegexSelect() {
    $("#select_regex").html("");
    for (var index in regexes) {
        var regex = regexes[index];
        $("#select_regex").append(mkoption(regex["id_regex"], regex["nome"]));
    }
    updateDetalhes();
}

function updateDetalhes() {
    var regex = getRegexById($("#select_regex").val());
    if (!regex) return;
    $("#regex_detalhes").text(regex["detalhes"]);
}

function extrair() {
    var args = {
        "user": user,
        "senha": senha,
        //"id_regex": $("#select_regex").val(),
        "alvo": $("#lista_in").val()
    };
    servPost("extrair_nomes", args, tenhoExtraidos);
}

function tenhoExtraidos(json) {
    if (json["status"] == "OK") {
        err("");
        $("#lista_in").val("");
        $("#extraidos").html("");
        var extraidos = json["extraidos"];
        for (var index in extraidos) {
            var aluno = extraidos[index];
            $("#extraidos").append("<tr><td>" + aluno["nome"] + "</td><td>"
                + aluno["ano"] + "º " + aluno["sala"] + " de " + aluno["em"]
                + "</td><td>" + aluno["matricula"] + "</td></tr>");
        }
        $("#totalext").text(" (" + extraidos.length + ")");
    } else {
        err(json["message"]);
    }
}

$("#editbuttons").hide();
var mapa_editado = null;
var mapas = [];

function updateListaMapas(json) {
    if (json) {
        if (json["status"] == "OK") {
            success("Mudança feita com sucesso!");
        } else {
            err("Algo de errado não está certo!");
        }
    }
    servGet("todas_salas_prova", [], function(json) {
        mapas = json;
        mapas = mapas.map(function(mapa) {
            mapa["fileiras"] = JSON.parse(mapa["fileiras"]);
            return mapa;
        });
        updateMapas();
    });
}

updateListaMapas();

function updateMapas() {
    var periodo = $("#periodo_mapas").val();
    $("#current_mapas").html("");
    var added = 0;
    var lastbr = false;
    mapas.map(function(mapa, index) {
        if (mapa["periodo"] != periodo) return;
        $("#current_mapas").append("<a href=\"javascript:void(0)\" onclick=\""
            + "editarMapa(" + mapa["id_sala_prova"] + ")\">" + mapa["numsala"]
            + "</a> ");
        added++;
        if (added % 5 == 0) {
            $("#current_mapas").append("<br>");
            lastbr = true;
        } else {
            lastbr = false;
        }
    });
    if (added == 0) {
        $("#current_mapas").append("<small><i><br>[Nenhuma sala para esse período"
            + " ainda...]</i></small>");
    }
    if (!lastbr) $("#current_mapas").append("<br>");
    cancelarMapa();
}

function updateMatriz() {
    var linhas = $("#mapa_lenfileira").val();
    var colunas = $("#mapa_fileiras").val();
    $("#header_matriz").html("");
    $("#lugares_matriz").html("");
    for (var i = 1; i <= colunas; i++) {
        var seletor = "<select id=\"ano_fileira_" + i + "\" "
            + "oninput=\"updateCoresMatriz()\">";
        for (var j = 1; j <= 3; j++)
            seletor += "<option value=\"" + j + "\">" + j + "º ano</option>";
        $("#header_matriz").append("<th>" + seletor + "</th>");
    }
    for (var linha = 1; linha <= linhas; linha++) {
        var tr = "<tr>";
        for (var coluna = 1; coluna <= colunas; coluna++) {
            tr += "<td><input type=\"text\" id=\"lugar_" + linha + "_" + coluna
                + "\"></input></td>";
        }
        tr += "<tr>";
        $("#lugares_matriz").append(tr);
    }
    for (var i = 1; i <= colunas; i++) {
        $("#ano_fileira_" + i).val(((i-1) % 3) + 1);
    }
    updateCoresMatriz();
}

function updateCoresMatriz() {
    var fileiras = $("#mapa_fileiras").val();
    var lenf = $("#mapa_lenfileira").val();
    for (var fileira = 1; fileira <= fileiras; fileira++) {
        for (var linha = 1; linha <= lenf; linha++) {
            var a = $("#ano_fileira_" + fileira).val();
            $("#lugar_" + linha + "_" + fileira).attr("class", "ano" + a);
        }
    }
}

updateMatriz();

function getMapaById(i) {
    for (var index in mapas) {
        var mapa = mapas[index];
        if (mapa["id_sala_prova"] == i) {
            return mapa;
        }
    }
    return null;
}

var clonevars = ["numsala", "lousa_acima", "posicao_porta"];

function editarMapa(id_mapa) {
    var mapa = getMapaById(id_mapa);
    if (mapa == null) return;
    mapa_editado = id_mapa;
    $("#periodo_mapas").val(mapa["periodo"]);
    clonevars.map(function(varname) {
        $("#mapa_" + varname).val(mapa[varname]);
    });
    $("#mapa_fileiras").val(mapa["fileiras"].length);
    var max_fileira = 1;
    mapa["fileiras"].map(function(fileira, f) {
        max_fileira = Math.max(max_fileira, fileira["lugares"].length);
        $("#ano_fileira_" + (f+1)).val(fileira["ano"]);
    });
    $("#mapa_lenfileira").val(max_fileira);
    updateMatriz();
    for (var fileira = 1; fileira <= mapa["fileiras"].length; fileira++) {
        for (var linha = 1; linha <= max_fileira; linha++) {
            var aluno = mapa["fileiras"][fileira-1]["lugares"][linha-1];
            var nominho = "";
            if (aluno) nominho = aluno["chamada"] + aluno["sala"];
            $("#lugar_" + linha + "_" + fileira).val(nominho);
        }
    }
    $("#current_action").text("Editando sala " + mapa["numsala"] + ":");
    $("#addbuttons").hide();
    $("#editbuttons").show();
}

function cancelarMapa() {
    mapa_editado = null;
    $("#mapa_numsala").val("");
    $("#mapa_posicao_porta").val("sd");
    $("#mapa_lousa_acima").val("1");
    $("#mapa_fileiras").val("6");
    $("#mapa_lenfileira").val("7");
    $("#editbuttons").hide();
    $("#addbuttons").show();
    $("#current_action").text("Adicionar uma sala:");
    updateMatriz();
}

function geraEnviavel() {
    var obj = {
        "periodo": $("#periodo_mapas").val(),
        "fileiras": []
    };
    clonevars.map(function(varname) {
        obj[varname] = $("#mapa_" + varname).val();
    });
    for (var fileira = 1; fileira <= $("#mapa_fileiras").val(); fileira++) {
        var fobj = {
            ano: $("#ano_fileira_" + fileira).val(),
            lugares: []
        };
        for (var linha = 1; linha <= $("#mapa_lenfileira").val(); linha++) {
            var gotcha = $("#lugar_" + linha + "_" + fileira).val();
            if (!gotcha) fobj["lugares"].push(null);
            else
                fobj["lugares"].push({
                    "chamada": parseInt(gotcha.slice(0, -1)),
                    "sala": gotcha.slice(-1)
                });
        }
        obj["fileiras"].push(fobj);
    }
    obj["fileiras"] = JSON.stringify(obj["fileiras"]);
    return JSON.stringify(obj);
}

function adicionarMapa() {
    if (mapa_editado) return;
    var args = {
        "user": user,
        "senha": senha,
        "mapa": geraEnviavel()
    };
    servPost("add_mapa", args, updateListaMapas);
}

function salvarMapa() {
    if (!mapa_editado) return;
    var args = {
        "user": user,
        "senha": senha,
        "mapa": geraEnviavel(),
        "id_mapa": mapa_editado
    };
    servPost("edit_mapa", args, updateListaMapas);
}

function removerMapa(id_mapa) {
    if (!mapa_editado) return;
    var args = {
        "user": user,
        "senha": senha,
        "id_mapa": mapa_editado
    };
    servPost("remove_mapa", args, updateListaMapas);
}
