var hashes = location.hash.slice(1).split(";");
var cred = hashes[1].split(",");
var matricula = cred[0];
var oldpass = cred[1];
var action = hashes[0];

function create() {
    var pass = $("#pass").val();
    var confirm = $("#pass2").val();
    var email = $("#email").val();

    if (pass !== confirm) {
        err("As senhas estão diferentes!");
        return false;
    }
    if (pass.length < 3) {
        err("Mínimo de 4 caracteres para as senhas!");
        return false;
    }
    if (email.indexOf("@") == -1) {
        err("Isso definitivamente não é um e-mail!");
        return false;
    }

    var args = {
        "matricula": matricula,
        "oldpass": oldpass,
        "newpass": pass,
        "email": email
    }

    servPost("register", args, registered);

    return false;
}

function registered(json) {
    if (json["status"] == "OK") {
        setCookie("mendelyx_token_aluno", json["token"], 90);
        setCookie("meu_id", json["seu_id"]);
        success("Você se registrou com sucesso!");
        if (location.hash.split(";")[0] == "#quickie") {
            window.close();
            return;
        }
        location.assign("../");
    } else {
        err(json["message"]);
    }
}