if (getToken()) {
    document.title = "Fazendo login...";
    $("#toptitle").html("Verificando seu login salvo...");
    $("#comtoken").fadeIn();
    servGet("token_check", [getToken()], tokenVerified);
} else {
    document.title = "Faça login!"
    $("#toptitle").html("Faça login!");
    $("#semtoken").fadeIn();
}
function failmsg(msg) {
    $("#msg").hide();
    $("#msg").html(msg);
    $("#msg").css("color", "red");
    $("#msg").fadeIn();
}

function tokenVerified(json) {
    if (json["status"] == "OK") {
        console.log("token ok");
        window.close();
    } else {
        console.log("token failed");
        $("#comtoken").hide();
        document.title = "Faça login!"
        $("#toptitle").html("Faça login!");
        $("#semtoken").fadeIn();
        failmsg(json["message"]);
        deleteCookie("mendelyx_token_aluno");
    }
}

function login() {
    var args = {
        "user": $("#matricula").val(),
        "pass": $("#senha").val()
    }
    servPost("login_aluno", args, loginVerified);
    return false;
}

function loginVerified(json) {
    if (json["status"] == "OK") {
        if (json["offer"]) {
            location.assign("criar_senha/#quickie;"
                + $("#matricula").val() + "," + $("#senha").val());
        } else {
            setCookie("mendelyx_token_aluno", json["token"], 180);
            setCookie("ano", json["seu_ano"]);
            setCookie("sala", json["sua_sala"]);
            window.close();
        }
    } else {
        failmsg(json["message"]);
    }
}

function attnome() {
    var q = $("#nominho").val();
    if (q.length > 3)
        servGet("get_alunos_lista", [q], tenhoLista);
}

function tenhoLista(json) {
    $("#ress").hide();
    $("#ress").html("");
    if (json["message"]) {
        $("#ress").html(json["message"]);
    } else {
        for (var index in json) {
            $("#ress").append(jslink("showMoo('" + json[index]["matricula"] + "')",
                json[index]["nome"]) + "<br>");
        }
    }
    if ($("#ress").html() == "") {
        $("#ress").html("Nenhum resultado encontrado.");
    }
    $("#ress").fadeIn();
}

function showMoo(matricula) {
    $("#ress").html("Sua matrícula é " + matricula + ".");
}