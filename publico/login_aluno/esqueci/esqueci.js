function trans(percentage, text) {
    $("#workingtext").fadeOut();
    $("#workingtext").html(text);
    $("#bar").css("width", percentage + "%");
    $("#workingtext").fadeIn();
}

$("#working").hide();
var first = true;

function esqueci_form() {
    if (first) {
        $("#working").fadeIn();
        first = false;
    }
    trans(50, "Comunicando com o servidor...");

    var matricula = $("#matricula").val();

    servGet("esqueci_senha", [matricula], esqueciStatus);
    return false;
}

function esqueciStatus(json) {
    trans(100, json["message"]);
}