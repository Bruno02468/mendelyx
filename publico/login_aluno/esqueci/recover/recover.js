if (location.hash.length != 33) {
    $("#checking").fadeOut();
    $("#brokentoken").fadeIn();
}

var rec = location.hash.slice(1);

servGet("recover_info", [rec], gotInfo)

function gotInfo(json) {
    $("#checking").fadeOut();
    if (json["status"] == "FAILED") {
        $("#notoken").fadeIn();
    } else {
        $("#nome").html(json["nome"]);
        $("#withtoken").fadeIn();
    }
}

function mudarsenha() {
    var newpass = $("#newpass").val();
    var newpass_confirm = $("#confirm").val();

    if (newpass != newpass_confirm) {
        err("As senhas acima não são iguais!");
        return false;
    }
    if (newpass.length < 4) {
        err("Mínimo de 4 caracteres para as senhas!");
        return false;
    }

    var args = {
        "token": rec,
        "newpass": newpass
    }

    servPost("mudar_senha", args, mudei);

    return false;
}

function mudei(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        success(json["message"]);
    }
}