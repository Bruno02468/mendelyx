var mini = location.hash.slice(1);
initEditor($("#editor"));
servGet("get_all_materias", [], tenhoMaterias);

var materias = [];
function tenhoMaterias(json) {
    materias = json;
    var old = $("#materia").val();
    $("#materia").html("");
    for (var index in json) {
        var mat = json[index];
        if ($("#ano").val() != "1" && mat["nome"] == "Artes") continue;
        $("#materia").append("<option value=\"" + mat["id_materia"] + "\">"
            + mat["nome"] + "</option>");
        if (mat["id_materia"] == old) {
            $("#materia").val(old);
        }
    }
    servGet("resumo_full", [mini], tenhoResumo)
}

function tenhoResumo(json) {
    if (json["status"] == "FAILED") {
        location.assign("../../resumo/404.html");
    } else {
        var resumo = json["resumo"];
        var assunto = resumo["assunto"];
        var ano = resumo["ano"];
        var id_materia = resumo["id_materia"];
        var nome_autor = resumo["nome_autor"];
        var etapa = resumo["etapa"];
        var periodo = resumo["periodo"];
        var html = resumo["texto"];
        $("#ano").val(ano);
        $("#etapa").val(etapa);
        $("#periodo").val(periodo);
        $("#materia").val(id_materia);
        $("#assunto").val(assunto);
        setHTML(html);
    }
}

function updateMaterias() {
    tenhoMaterias(materias);
}

function salve() {
    var args = {
        "ano": $("#ano").val(),
        "id_materia": $("#materia").val(),
        "assunto": $("#assunto").val(),
        "etapa": $("#etapa").val(),
        "periodo": $("#periodo").val(),
        "miniurl": mini,
        "html": makeBase64URLSafe(
            btoa(unescape(encodeURIComponent(getHTML())))
        ),
        "token": getToken()
    }
    servPost("editar_resumo", args, editeiResumo);
}

function editeiResumo(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        location.assign("../../resumo/" + location.hash);
    }
}

function remover() {
    if (confirm("Certeza?\n\nSeu resumo poderá ser restaurado a qualquer"
        + " momento na página Meus Resumos!")) {
        confirmRemover();
    }
}

function confirmRemover() {
    servGet("toggle_remover_resumo", [getToken(), mini], removi);
}

function removi() {
    if (json["status"] == "OK") {
        location.assign("../meus/");
    } else {
        err(json["message"]);
    }
}
