if (!getToken()) {
    location.assign("..");
}

servGet("get_all_materias", [], tenhoMaterias);
var materias = {};
function tenhoMaterias(json) {
    for (var key in json) {
        var mat = json[key];
        materias[mat["id_materia"]] = mat;
    }
    updateResumos();
}

function updateResumos() {
    servGet("meus_resumos", [getToken()], tenhoResumos);
}

function make_td(str) {
    return "<td>" + str + "</td>";
}

function tenhoResumos(json) {
    if (json["status"] == "FAILED") {
        $("#carregando").hide();
        $("#login").fadeIn();
    } else {
        if (json.length == 0) {
            $("#lista_txt").html("Você não tem resumos no momento...<br><br>"
                + "<a href=\"../enviar/\">Que tal escrever um agora mesmo?"
                + "</a>");
            $("#carregando").hide();
            $("#resumos").fadeIn();
        } else {
            json.reverse();
            $("#lista_txt").html("Seguem seus resumos:<br><br>");
            $("#lista").html("");
            for (var index in json) {
                var resumo = json[index];
                var mini = resumo["miniurl"];
                var link_td = make_td("<a href=\"../../resumo/#" + mini + "\">"
                    + "###</a>");
                var mat_td = make_td(materias[resumo["id_materia"]]["nome"]);
                var as_td = make_td(resumo["assunto"]);
                var ano_td = make_td(resumo["ano"] + "º");
                var eta_td = resumo["etapa"] == "0"
                    ? make_td("todas")
                    : make_td(resumo["etapa"] + "ª");
                var per_td = resumo["periodo"] == "0"
                    ? make_td("todos")
                    : make_td(resumo["periodo"] + "º");
                var rem_td = resumo["removido"]
                    ? make_td("sim (<a href=\"javascript:void(0)\" onclick=\""
                        + "toggleRemovido('" + mini + "')\">"
                        + "restaurar</a>)")
                    : make_td("não (<a href=\"javascript:void(0)\" onclick=\""
                        + "toggleRemovido('" + mini + "')\">"
                            + "remover</a>)");
                var edit_td = make_td("<a href=\"../editar/#" + mini + "\" "
                    + "class=\"glyphicon glyphicon-pencil imgbtn\"></a>");
                    
                var tr = "<tr>" + link_td + mat_td + as_td + ano_td + eta_td
                    + per_td + rem_td + edit_td + "</tr>";
                $("#lista").append(tr);
            }
            $("#resumos").append("Aumente esta lista, <a href=\"../enviar/\">"
                + "envie um resumo!</a>");
            $("#carregando").hide();
            $("#resumos").fadeIn();
        }
    }
}

function toggleRemovido(mini) {
    servGet("toggle_remover_resumo", [getToken(), mini], updateResumos);
}
