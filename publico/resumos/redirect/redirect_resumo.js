var count = parseInt($("#countdown").html())-1;
var interval;
var bars = location.href.split("/");
var url = "../../resumo/#" + bars[bars.length-1];

$("#manual").attr("href", url);

function countDown() {
    $("#countdown").html(count);
    if (count == 0) {
        location.assign(url);
        clearInterval(interval);
    } else {
        count--;
    }
}

interval = setInterval(countDown, 1000);
