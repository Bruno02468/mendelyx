initEditor($("#editor"));

servGet("get_all_materias", [], tenhoMaterias);

var materias = [];
function tenhoMaterias(json) {
    materias = json;
    var old = $("#materia").val();
    $("#materia").html("");
    for (var index in json) {
        var mat = json[index];
        if ($("#ano").val() != "1" && mat["nome"] == "Artes") continue;
        $("#materia").append("<option value=\"" + mat["id_materia"] + "\">"
            + mat["nome"] + "</option>");
        if (mat["id_materia"] == old) {
            $("#materia").val(old);
        }
    }
}

function updateMaterias() {
    tenhoMaterias(materias);
}

function enviar() {
    checkLoggedIn(tenhoLoginEnviar);
}

function tenhoLoginEnviar(json) {
    if (json["status"] == "FAILED") {
        err("Você precisa <a href=\"javascript:void(0)\" onclick=\""
            + "loginQuickie(2)\">fazer login</a> antes!");
    } else {
        var args ={
            "ano": $("#ano").val(),
            "id_materia": $("#materia").val(),
            "assunto": $("#assunto").val(),
            "etapa": $("#etapa").val(),
            "periodo": $("#periodo").val(),
            "miniurl": $("#mini").text(),
            "html": makeBase64URLSafe(
                btoa(unescape(encodeURIComponent(getHTML())))
            ),
            "token": getToken()
        }
        servPost("enviar_resumo", args, envieiResumo);
    }
}

function envieiResumo(json) {
    if (json["status"] == "FAILED") {
        err(json["message"]);
    } else {
        location.assign("../../resumo/#" + $("#mini").text())
    }
}
