var shown;

function acc(nome) {
    if (shown) {
        shown.hide();
    }
    shown = $("#acc_" + nome);
    shown.fadeIn();
    setResultados("");
}
acc("nome");

var placeholders = [
    "quimica geometria anions",
    "literatura til",
    "pre-historia",
    "reino monera",
    "matematica formulas",
    "john locke",
    "fisica optica formulas",
    "gramatica oracoes",
    "ingles 1",
    "redacao narrativa",
    "geografia agropecuaria",
    "crise"
];

function change_placeholder() {
    $("#pesquisa").attr("placeholder", "\"" + randomFrom(placeholders) + "\"");    
}

change_placeholder();
setInterval(change_placeholder, 3000);

if (getToken()) {
    servGet("meus_resumos", [getToken()], testMeus);
}

function testMeus(json) {
    if (json["status"] != "FAILED") {
        $("#meus").show();
    }
}

servGet("contagem_resumos", [], tenhoContagem);

function tenhoContagem(json) {
    $("#contagem").html("Contamos com " + json["total"] + " resumos");
    var meu_ano = getCookie("meu_ano");
    if (meu_ano) {
        $("#contagem").append(", sendo " +  json["por_ano"][meu_ano]
            + " para o " + meu_ano + "º ano!");
    } else {
        $("#contagem").append("!");
    }
}

var materias;
servGet("get_all_materias", [], tenhoMaterias);
function tenhoMaterias(json) {
    materias = json;
    if (location.hash.length > 1) {
        acc("mat");
        anosel(location.hash[1]);
    }
    if (location.hash.indexOf("_") > -1) {
        var m = location.hash.split("_")[1];
        matid(m);
    }
}

function err_pesquisa(msg) {
    setResultados("<span class=\"err\">" + msg + "</span>");
}

function setResultados(str) {
    $("#resultados").hide();
    $("#resultados").html(str);
    $("#resultados").fadeIn();
}

function tenhoResultados(json) {
    if (json["status"] == "FAILED") {
        err_pesquisa(json["message"]);
        return;
    }
    var resumos = JSON.parse(json["resumos"]);
    if (resumos.length == 0) {
        err_pesquisa("Nenhum resultado encontrado... ");
        return;
    }
    setResumos(resumos);
}

var incluir_mat_e_ano = true;

function setResumos(lista) {
    var final = "";
    for (var index in lista) {
        var resumo = lista[index];
        if (materias && incluir_mat_e_ano) {
            for (var i in materias) {
                var mat = materias[i];
                if (mat["id_materia"] == resumo["id_materia"]) {
                    resumo["assunto"] = "[" + mat["nome"] + " - "+resumo["ano"]
                    +  "º Ano] " + resumo["assunto"];
                }
            }
        }
        final += "<a href=\"../resumo/#" + resumo["miniurl"] + "\" target="
            + "\"_blank\"><li class=\"list-group-item\">" + resumo["assunto"]
            + "</li></a>";
    }
    if (final == "") final = "<span class=\"err\">Nenhum resumo encontrado!</span>";
    setResultados(final);
    if (!incluir_mat_e_ano) scrollToBottom();
}

function pesquisa() {
    var query = $("#pesquisa").val().trim();
    if (query.length == 0) {
        setResultados("");
        return;
    }
    if (query.length < 5) {
        err_pesquisa("Digite pelo menos 5 caracteres!");
        return;
    }
    setResultados("Pesquisando...");
    incluir_mat_e_ano = true;
    servGet("pesquisar_resumos", [query], tenhoResultados);
}

var ano;
function anosel(a) {
    ano = a;
    $("#matlinks").html("");
    for (var index in materias) {
        var mat = materias[index];
        if (mat["artes"] && ano != 1) continue;
        $("#matlinks").append("<a href=\"javascript:void(0)\" onclick=\"matid("
            + mat["id_materia"] + ")\">" + mat["nome"] + "</a><br>");
    }
    $("#ano_sel").hide();
    $("#mat_sel").fadeIn();
    scrollToBottom();
}


function goback() {
    $("#mat_sel").hide();
    $("#ano_sel").fadeIn();
    setResultados("");
}

function matid(id_materia) {
    incluir_mat_e_ano = false;
    servGet("get_resumos_old_way", [ano, id_materia], tenhoResultados);
}


function gotAno(a) {
    ano = a;
    setCookie("ano", a);
    servGet("votacoes_basic", [], tenhoVotacoes);
    $("#acc_prova").html("");
}

function comparador_votacoes(a, b) {
    return Date.parse(a["fim"]) - Date.parse(b["fim"]);
}

var vot;
function tenhoVotacoes(json) {
    if (json.lenngth == 0) {
        $("#acc_prova").append("Parece que por enquanto não sabemos o calendário...");
        return;
    }
    json.sort(comparador_votacoes);
    vot = json[json.length-1];
    $("#acc_prova").append("Parece que as próximas provas são a da "
        + vot["etapa"] + "ª etapa do " + vot["periodo"] +"º período.<br><br>");
    if (vot["acabou"]) {
        $("#acc_prova").append("Baseado na votação que acabou e no calendário"
            + " que ganhou, organizamos os resumos recomendados:");
        servGet("votacao_full", [vot["id_votacao"], ano], tenhoVotacao);
    } else {
        $("#acc_prova").append("Parece que a votação ainda não acabou, então"
            + "não a levamos em conta, mas aí vão os resumos recomendados,"
            + " organizados por matéria:");
        servGet("get_resumos_epa", [vot["etapa"], vot["periodo"], ano],
            tenhoResumosEPA);
    }
    $("#acc_prova").append("<br><br><div id=\"pmatlinks\"></div>");
}

var votacao;
function tenhoVotacao(json) {
    votacao = json["data"];
    servGet("get_resumos_epa", [vot["etapa"], vot["periodo"], ano],
        tenhoResumosEPA);
}

function getMateriaById(id) {
    for (var index in materias) {
        if (materias[index]["id_materia"] == id) {
            return materias[index];
        }
    }
}

function appendLink(mat) {
    $("#pmatlinks").append("<a href=\"javascript:(0)\" onclick=\"pmat("
        + mat["id_materia"] + ")\">" + mat["nome"] + "</a><br>");
}

var byepa;
function tenhoResumosEPA(json) {
    byepa = json;
    if (votacao) {
        var calendarios = votacao["calendarios"];
        calendarios.sort(comparadorVotos);
        var vencedor = JSON.parse(calendarios[0]["calendario"]);
        for (var i = 1; i <= 4; i++) {
            var date = isoToDate(votacao["prova" + i]);
            var dia = date.getDate();
            var mes = date.getMonth()+1;
            var semana = dias_semana[date.getDay()];
            $("#pmatlinks").append("<br>Provas do dia " + dia + "/" + mes + " ("
                + semana + "):<br>");
            var cal = vencedor[i-1];
            console.log(cal);
            for (var index in cal) {
                var id_mat = cal[index];
                if (id_mat == "none") {
                    break;
                }
                var mat = getMateriaById(id_mat);
                appendLink(mat);
            }
        }
    } else {
        for (var index in materias) {
            var mat = materias[index];
            if (mat["artes"] && ano != 1) continue;
            if (mat["nome"] == "Redação") continue;
            appendLink(mat);
        }
    }
    scrollToBottom();
}

function pmat(id) {
    var res = [];
    for (var index in byepa) {
        var resumo = byepa[index];
        if (resumo["id_materia"] != id) continue;
        res.push(resumo);
    }
    incluir_mat_e_ano = false;
    setResumos(res);
}
