Atividades rotineiras
Votações dos calendários de prova
Oito vezes por ano já classifica como rotina.

O fato é, as votações dos calendários de prova são uma das coisas nas quais
você terá que mais prestar atenção, especialmente no que tange à interação com
os orientadores de cada ano, e entre você e os demais administradores.

Vocês deverão discutir entre si quem vai iniciar a votação, e, caso seja usado
o sistema "representativo", quem vai postar os calendários escolhidos.

Basicamente, uma coisa sempre deverá ser feita antes de mais nada: determinar
os dados básicos da votação. Alguns são óbvios, outros nem tanto.

	· Etapa e período: referem-se às provas.

	· Período de votação: o primeiro e o último dia para votar no formulário
	  são, sim, inclusos no período de votação, das 00:00:00 do primeiro dia
	  às 23:59:59 do último dia. Devem ser discutidos com a orientação, afinal,
	  eles que determinam quando o resultado deve chegar.

	· Dias de prova: esses você deverá perguntar para a orientação. Geralmente
	  são quatro, mas você pode adicionar ou remover dias caso necessário.

Agora, o sistema representativo merece um parágrafo próprio.

Geralmente, as votações seguiam uma democracia total, ou seja, qualquer um
podia enviar um calendário, e todos recebiam votos de todos ao longo do período
estipulado. Porém, a diluição dos votos devido ao grande número de calendários
trouxe um novo sistema, no qual vai um representante de cada sala e, junto com
os orientadores do seu ano, são escolhidos geralmente três calendários para
serem votados.

O fato é, quando isso acontece, isso quer dizer que ninguém mais pode postar
calendários, apenas uma pessoa.

Então, quando esse sistema "representativo" for ser usado (e parece que vai ser
assim para sempre), você deve, além de especificar isso, fornecer o login
daquele que será responsável por postar os calendários que serão votados.

Aquele que tiver o login especificado será o único para quem vai aparecer um
botão de "enviar calendário". Não importa através de que ano você acessou;
aparecerá ao final da tela de envio um seletor para você especificar de que
ano é o calendário. Preste atenção nesse seletor, ou as coisas podem dar
MUITO errado para todo mundo.

Caso você faça algo errado na hora de abrir a votação ou de enviar um
calendário, avise-me o quanto antes.

Deixe bem claro para a administração sobre os limites de votação (que incluem
apenas dias, e não horários) e que ela não pode mais ser reaberta ou desfeita.

E, frisando pela última vez, VERIFIQUE TUDO QUE VOCÊ DIGITAR NISSO. É uma das
partes mais sensíveis do site, e se algo sair errado, vai dar trabalho pra todo
mundo, inclusive pra mim. Sua função é justamente evitar isso. Verifique.
