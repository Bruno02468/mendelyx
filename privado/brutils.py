# programado por bruno borges paschoalinoto ou borginhos ou bruno02468

import re
import datetime


acentos = [
    ["á", "a"],
    ["é", "e"],
    ["í", "i"],
    ["ó", "o"],
    ["ú", "u"],

    ["â", "a"],
    ["ê", "e"],
    ["ô", "o"],

    ["ã", "a"],
    ["õ", "o"],

    ["ü", "u"],

    ["ç", "c"]
]

for index in range(0, len(acentos)):
    acentos[index][0] = re.compile(acentos[index][0])

def str_simplify(string):
    string = string.lower()
    for tup in acentos:
        string = tup[0].sub(tup[1], string)
    return string

def is_valid_mini(string):
    return bool(re.match("^[A-Za-z0-9_-]*$", string))

meses_finais = [9, 10, 11, 12]
meses_iniciais = [1, 2, 3, 4]

def get_ano_diretor():
    agora = datetime.datetime.now()
    ano_diretor = agora.year
    mes = agora.month
    if mes not in (meses_finais + meses_iniciais):
        return 0
    if mes in meses_finais:
        ano_diretor += 1
    return ano_diretor

