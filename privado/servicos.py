# programado por bruno borges paschoalinoto ou borginhos ou bruno02468

import math
import json
import banco
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
import base64
from brutils import str_simplify
import brutils
import os

db = banco.Banco()

salas = {
    "1": list("ABCDEFGHIJ"),
    "2": list("ABCDEFGH"),
    "3": list("ABCDEFG")
}

logfolder = "../logs/"

maxFileSize = 20000000

allowedExtensions = [
    "jpg", "jpeg", "png", "gif", "bmp",
    "pdf", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "odt", "txt", "rtf",
    "pps", "ppsx", "key", "csv", "tiff", "xcf",
    "mp4", "mp3", "wav", "mkv", "ogg", "avi", "m4a", "m4v", "mov", "ogv", "wmv",
    "zip", "7z", "rar", "tar",
]

def sendHTMLEmail(uname, para, assunto, html, text_version):
    msg = MIMEMultipart("alternative")
    msg["Subject"] = assunto
    eu = formataddr((str(Header(u"Licoes.com", "utf-8")),
        uname + "@licoes.com"))
    msg["From"] = eu
    msg["To"] = para
    part1 = MIMEText(text_version, "plain")
    part2 = MIMEText(html, "html")
    msg.attach(part1)
    msg.attach(part2)
    s = smtplib.SMTP("127.0.0.1", 9993)
    #s.starttls(None, None, None)
    s.ehlo()
    s.sendmail(eu, para, msg.as_string())
    s.quit()

def checkArgs(snome, received):
        for nome, reqs in getServico(snome).requires.items():
            if (nome == "url"):
                got = len(received[nome])
                reqs = len(reqs)
            else:
                got = sorted(list(received[nome].keys()))
                reqs.sort()
            if reqs != got:
                return "Falha nos argumentos: {}, precisa de {}, porém recebi {}." \
                .format(nome.upper(), reqs, got)
        return True

def opaqueEmail(email):
    parts = email.split("@")
    if len(parts) < 2:
        return ("[email inválido]", "#")
    username = parts[0]
    server = parts[1]
    if len(username) < 1 or len(server) < 1:
        return ("[email inválido]", "#")
    if len(username) == 1:
        return ("·@" + server, server)
    return (username[0] + ((len(username)-1) * "·") + "@" + server, server)


class ServicoBase:
    description = "A base para todos os serviços."
    requires = {
        "url": [],
        "post": [],
    }
    contentType = "text/json"
    def get(args):
        return "{}"

class Alive(ServicoBase):
    description = "Retorna um JSON com alive=true para propósitos de teste."
    def get(args):
        return {
            "alive": True
        }

class GetAPIDescription(ServicoBase):
    descrption = "A API se autodescreve!"
    def get(args):
        descricao = {}
        for index, servico in _servicos.items():
            descricao[index] = {
                "descricao": servico.description,
                "argumentos": servico.requires
            }
        return json.dumps(descricao, indent=4, ensure_ascii=False)

class GetLicoesSala(ServicoBase):
    description = "Retorna todas as lições de uma certa sala."
    requires = {
        "url": ["ano", "sala"],
        "post": [],
    }

    def get(args):
        return json.dumps(db.getLicoesSala(args["url"][0], args["url"][1]))

class GetSalas(ServicoBase):
    description = "Retorna todas as salas existentes."
    def get(args):
        return salas

class GetAulas(ServicoBase):
    description = "Retorna todas as aulas de um certo ano."
    requires = {
        "url": ["ano"],
        "post": [],
    }
    def get(args):
        aulas = db.getAulas(args["url"][0])
        return json.dumps(aulas)

class VerifyToken(ServicoBase):
    description = "Verifica um token de login."
    requires = {
        "url": ["token"],
        "post": [],
    }
    def get(args):
        aluno = db.getLogin(args["url"][0])
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Seu login expirou.",
                "offer": "false"
            }
        else:
            return {
                "status": "OK",
                "message": "OK",
                "matricula": aluno["matricula"],
                "offer": "false",
                "seu_id": aluno["id_aluno"],
                "seu_nome": aluno["nome"],
                "seu_numero": aluno["chamada"],
                "seu_ano": aluno["ano"],
                "sua_sala": aluno["sala"]
            }

class VerifyAluno(ServicoBase):
    description = "Verifica o login de um aluno."
    requires = {
        "url": [],
        "post": ["user", "pass"],
    }
    def post(args):
        matricula = args["post"]["user"]
        senha = args["post"]["pass"]
        if len(matricula) != 8:
            return {
                "status": "FAILED",
                "message": "Números de matrícula costumam ter 8 dígitos..."
            }
        response = db.isValidAluno(matricula, senha)
        temsenha = db.hasPassword(matricula)
        if response:
            aluno = db.getAlunoBy("matricula", matricula)
            return {
                "status": "OK",
                "token": db.makeToken(matricula),
                "offer": not temsenha,
                "seu_id": aluno["id_aluno"],
                "seu_ano": aluno["ano"],
                "sua_sala": aluno["sala"]
            }
        else:
            k = ""
            if not temsenha:
                k = "<br>Veja as instruções abaixo para logar pela primeira vez!"
            return {
                "status": "FAILED",
                "message": "Credenciais incorretas!" + k,
                "offer": "false"
            }

class RegisterAluno(ServicoBase):
    description = "Chamado quando um aluno se registra."
    requires = {
        "url": [],
        "post": ["matricula", "oldpass", "newpass", "email"],
    }
    def post(args):
        matricula = args["post"]["matricula"]
        oldpass = args["post"]["oldpass"]
        newpass = args["post"]["newpass"]
        email = args["post"]["email"]
        if len(email) < 5:
            return {
                "status": "FAILED",
                "message": "Isso definitivamente não é um e-mail válido!"
            }
        if len(newpass) < 5:
            return {
                "status": "FAILED",
                "message": "Sua senha deve ter no mínimo 5 caracteres!"
            }
        if db.hasPassword(matricula):
            return {
                "status": "FAILED",
                "message": ("Você já se registrou! "
                    "<a href=\"../login_quickie.html\">Faça login!</a>")
            }
        else:
            if db.isValidAluno(matricula, oldpass):
                aluno = db.getAlunoByMatricula(matricula)
                response = db.registerAluno(matricula, oldpass, newpass, email)
                return {
                    "status": "OK",
                    "token": db.makeToken(matricula),
                    "seu_id": aluno["id_aluno"]
                }
            else:
                return {
                    "status": "FAILED",
                    "message": ("Os dados fornecidos na URL são inválidos.<br>"
                        "<a href=\"javascript:void(0)\" onclick=\"window.close()\">"
                        "Para de trolar!</a>")
                }

class AdicionarLicao(ServicoBase):
    description = "Adiciona uma lição"
    requires = {
        "url": [],
        "post": ["token", "id_aula", "para", "info"],
    }
    def post(args):
        token = args["post"]["token"]
        id_aula = args["post"]["id_aula"]
        para = args["post"]["para"]
        info = args["post"]["info"].strip()
        matricula = db.isValidToken(token)

        if len(info) < 1:
            return {
                "status": "FAILED",
                "message": "EMPTY"
            }

        if matricula:
            aluno = db.getAlunoByMatricula(matricula)
            if aluno["banido"]:
                return {
                    "status": "FAILED",
                    "message": "BANNED"
                }
            response = db.adicionarLicao(aluno["id_aluno"], id_aula, para, info)
            return {
                "status": "OK"
            }
        else:
            return {
                "status": "FAILED",
                "message": "TOKEN"
            }

class GetAlunoBasic(ServicoBase):
    description = "Usado pelos clientes para pegar informações sobre um aluno."
    requires = {
        "url": ["id"],
        "post": [],
    }
    def get(args):
        aluno = db.getAlunoBy("id_aluno", args["url"][0])
        if aluno == None:
            return "{}"
        else:
            return {
                "nome": aluno["nome"],
                "email": aluno["email"],
                "banido": aluno["banido"],
                "autor": aluno["autor"]
            }

class EditarLicao(ServicoBase):
    description = "Usado para editar uma lição."
    requires = {
        "url": [],
        "post": ["token", "id_aula", "para", "info", "id_licao"],
    }
    def post(args):
        token = args["post"]["token"]
        id_aula = args["post"]["id_aula"]
        para = args["post"]["para"]
        info = args["post"]["info"]
        matricula = db.isValidToken(token)
        id_licao = args["post"]["id_licao"]

        if matricula:
            aluno = db.getAlunoByMatricula(matricula)
            licao = db.getLicaoById(id_licao)
            if licao == None:
                return {
                    "status": "FAILED",
                    "message": "TOKEN"
                }
            if str(licao["id_aluno"]) == str(aluno["id_aluno"]):
                db.updateLicao(id_licao, id_aula, para, info)
                return {
                    "status": "OK"
                }
            else:
                return {
                    "status": "FAILED",
                    "message": "NOTYOURS"
                }

class ApagarLicao(ServicoBase):
    description = "Usado para apagar uma lição."
    requires = {
        "url": ["token", "id_licao"],
        "post": [],
    }
    def get(args):
        token = args["url"][0]
        id_licao = args["url"][1]
        matricula = db.isValidToken(token)

        if matricula:
            aluno = db.getAlunoByMatricula(matricula)
            licao = db.getLicaoById(id_licao)
            if licao == None:
                return {
                    "status": "FAILED",
                    "message": "TOKEN"
                }
            if str(licao["id_aluno"]) == str(aluno["id_aluno"]):
                db.removeLicao(id_licao)
                return {
                    "status": "OK"
                }
            else:
                return {
                    "status": "FAILED",
                    "message": "NOTYOURS"
                }
        else:
            return {
                "status": "FAILED",
                "message": "TOKEN"
            }

class ReportarLicao(ServicoBase):
    description = "Usado quando alguém tenta reportar uma lição."
    requires = {
        "url": ["token", "id_licao"],
        "post": [],
    }
    def get(args):
        token = args["url"][0]
        id_licao = args["url"][1]
        matricula = db.isValidToken(token)

        if matricula:
            aluno = db.getAlunoByMatricula(matricula)
            return db.reportLicao(aluno["id_aluno"], id_licao)
        else:
            return {
                "status": "FAILED",
                "message": "TOKEN"
            }

class EsqueciMinhaSenha(ServicoBase):
    description = "Envia um email de mudança de senha para alguém."
    requires = {
        "url": ["matricula"],
        "post": [],
    }
    def get(args):
        matricula = args["url"][0]
        aluno = db.getAlunoByMatricula(matricula)
        if aluno == None:
            return {
                "status": "FAILED",
                "message": "Esse número de matrícula não existe!"
            }
        else:
            if not aluno["email"]:
                return {
                    "status": "FAILED",
                    "message": ("Não consta um email no sistema para você.<br>"
                        "<i>Você já se registrou?</i> <a href=\"javascript:void(0)\" "
                        "onclick=\"window.history.back()\">"
                        "Tente voltar e tentar de novo!</a>")
                }
            if db.hasActiveRecovery(aluno["id_aluno"]):
                return {
                    "status": "FAILED",
                    "message": ("Já há um link de recuperação de senha ativo"
                        " para você, verifique seu e-mail!")
                }
            recoverToken = db.criarLinkEsqueci(aluno["id_aluno"])
            url = (db.getConfig()["urlinfo"]["public_html"]
                + "login_aluno/esqueci/recover/#" + recoverToken)
            nome = aluno["nome"].split(" ")[0]
            content = db.getEmailTemplate("esqueci_minha_senha") % (nome, url)
            alunoemail = aluno["email"]
            subject = "Esqueceu sua senha? - Licoes.com"
            textvers = "Esqueceu sua senha?\n\n" + url
            sendHTMLEmail("esqueci", alunoemail, subject, content, textvers)
            emailAnalysis = opaqueEmail(alunoemail)
            return {
                "status": "OK",
                "message": ("Enviamos um email para seu endereço %s.<br>"
                    "Que tal verificá-lo em <a href=\"//%s\">%s</a>?") %
                    (emailAnalysis[0], emailAnalysis[1], emailAnalysis[1])
            }

class GetRecoverInfo(ServicoBase):
    description = "É usado para pegar informações de um token de mudança de senha."
    requires = {
        "url": ["token"],
        "post": [],
    }
    def get(args):
        token = args["url"][0]
        info = db.getValidRecoverToken(token)
        if info == None:
            return {
                "status": "FAILED",
                "message": ("Esse link já foi usado ou expirou.<br>"
                    "<a href=\"..\">Tente novamente!</a>")
            }
        else:
            return {
                "status": "OK",
                "nome": db.getAlunoBy("id_aluno", info["id_aluno"])["nome"].split(" ")[0]
            }

class MudarSenha(ServicoBase):
    description = "Muda a senha de um aluno usando o token dado..."
    requires = {
        "url": [],
        "post": ["token", "newpass"],
    }
    def post(args):
        token = args["post"]["token"]
        newpass = args["post"]["newpass"]
        token_info = db.getValidRecoverToken(token)
        if token_info == None:
            return {
                "status": "FAILED",
                "message": ("Esse link já foi usado ou expirou.<br>"
                    "<a href=\"..\">Tente novamente!</a>")
            }
        else:
            id_aluno = token_info["id_aluno"]
            id_link = token_info["id_link"]
            db.alterarSenha(id_aluno, newpass, id_link)
            return {
                "status": "OK",
                "message": ("Sua senha foi alterada com sucesso!<br>"
                    "Que tal voltar ao <a href=\"../../../\">site</a>?")
            }

class LoginAdm(ServicoBase):
    description = "Usado para verificar um login de administrador."
    requires = {
        "url": [],
        "post": ["user", "senha"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        admins = db.getConfig()["admins"]
        if user in admins:
            if admins[user] == senha:
                return {
                    "status": "OK"
                }
            else:
                return {
                    "status": "FAILED",
                    "message": "Senha incorreta!"
                }
        else:
            return {
                "status": "FAILED",
                "message": "Esse usuário não existe!"
            }

class IniciarVotacao(ServicoBase):
    description = "Usado por um administrador para iniciar uma votação."
    requires = {
        "url": [],
        "post": ["user", "senha", "etapa", "periodo", "primeiro_dia", "ultimo_dia",
            "dias", "neo", "responsavel_neo_matricula"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        etapa = args["post"]["etapa"]
        periodo = args["post"]["periodo"]
        primeiro_dia = args["post"]["primeiro_dia"]
        ultimo_dia = args["post"]["ultimo_dia"]
        dias = args["post"]["dias"]
        neo = (args["post"]["neo"] == "true")
        responsavel_neo_matricula = args["post"]["responsavel_neo_matricula"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        responsavel = db.getAlunoByMatricula(responsavel_neo_matricula)

        if not responsavel:
            return {
                "status": "FAILED",
                "message": "Esse aluno não existe!"
            }

        db.iniciarVotacao(periodo, etapa, primeiro_dia, ultimo_dia, dias, neo,
                          responsavel["id_aluno"])

        return {
            "status": "OK",
            "message": "Votação iniciada com sucesso!"
        }

class ToggleBanido(ServicoBase):
    description = "Usado por um administrador para banir ou desbanir um aluno."
    requires = {
        "url": [],
        "post": ["user", "senha", "matricula"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        matricula = args["post"]["matricula"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        banStatus = db.toggleBanido(matricula)
        if banStatus == True:
            return {
                "status": "OK",
                "message": "Aluno banido com sucesso!"
            }
        if banStatus == False:
            return {
                "status": "OK",
                "message": "Aluno desbanido com sucesso!"
            }
        else:
            return {
                "status": "FAILED",
                "message": "Esse aluno não existe!"
            }

class GetVotacoesBasic(ServicoBase):
    description = "Usado para pegar a lista simples de votações visíveis."
    def get(args):
        return json.dumps(db.getVotacoesBasic())

class GetVotacaoFull(ServicoBase):
    description = "Usado para pegar a lista de calendários e outras informações."
    requires = {
        "url": ["id_votacao", "ano"],
        "post": [],
    }
    def get(args):
        id_votacao = args["url"][0]
        ano = args["url"][1]
        votacao = db.getVotacaoFull(id_votacao, ano)
        if votacao == None:
            return {
                "status": "FAILED",
                "message": "Essa votação não existe!"
            }
        else:
            return {
                "status": "OK",
                "data": votacao
            }

class GetMaterias(ServicoBase):
    description = "Usado para pegar as matérias de um certo ano."
    requires = {
        "url": ["ano"],
        "post": [],
    }
    def get(args):
        ano = args["url"][0]
        return json.dumps(db.getMateriasByAno(ano))

class GetAllMaterias(ServicoBase):
    description = "Usado para obter todas as matérias."
    def get(args):
        return json.dumps(db.getMaterias())

class EnviarCalendario(ServicoBase):
    description = "Usado para enviar um calendário de provas."
    requires = {
        "url": [],
        "post": ["token", "id_votacao", "calendario", "voting_as"],
    }
    def post(args):
        token = args["post"]["token"]
        id_votacao = args["post"]["id_votacao"]
        calendario_str = args["post"]["calendario"]
        voting_as = args["post"]["voting_as"]

        votacao = db.getVotacaoFull(id_votacao, voting_as)
        if votacao == None:
            return {
                "status": "FATAL",
                "message": "Essa votação não existe!"
            }

        if votacao["neo"]:
            aluno = db.getLogin(token)
            if aluno["id_aluno"] != votacao["responsavel_neo"]:
                return {
                    "status": "FATAL",
                    "message": "Essa votação não aceita calendários (novo esquema)!"
                }

        matricula = db.isValidToken(token)
        if matricula == False:
            return {
                "status": "FAILED",
                "message": ("Seu login é inválido ou expirou!<br>"
                    "<a href=\"javascript:void(0)\" onclick=\"doLogin(2)\">"
                    "Faça login novamente!</a>")
            }
        aluno = db.getAlunoByMatricula(matricula)
        if not votacao["neo"]:
            voting_as = "0"
            if aluno["banido"]:
                return {
                    "status": "FAILED",
                    "message": "Você está banido!"
                }
            if str(aluno["ano"]) != str(voting_as):
                return {
                    "status": "FAILED",
                    "message": ("Você não é do " + str(voting_as)
                        + "º ano, cara! Vá na"
                        "<a href=\"..\">votação do seu ano!</a>")
                }
            if db.jaTerminou(id_votacao):
                return {
                    "status": "FAILED",
                    "message": ("Essa votação já terminou!")
                }
            if db.getSpecificCalendario(id_votacao, aluno["id_aluno"]) != None:
                return {
                    "status": "FAILED",
                    "message": "Você só pode enviar um calendário!"
                }

        calendario = []
        for i in range(0, len(votacao["dias"])):
            calendario.append([])
        for index, val in enumerate(calendario_str.split(",")):
            dia_index = math.floor(index/3)
            calendario[dia_index].append(val)
            calendario[dia_index] = sorted(calendario[dia_index])

        duplicata = db.getDuplicata(id_votacao, voting_as, calendario)
        if duplicata[0]:
            return {
                "status": "FAILED",
                "message": ("Já existe um <a href="
                    "\"../ver/#%s,%s,%s\">calendário equivalente</a>!" % (
                    id_votacao, voting_as, duplicata[1]))
            }
        calen_id = db.adicionarCalendario(id_votacao, aluno["id_aluno"],
                                          calendario, voting_as)
        return {
            "status": "OK",
            "id_calendario": calen_id["id_calendario"]
        }

class PossoEnviarCalendario(ServicoBase):
    description = "Usado para saber se você pode votar numa votação 'neo'."
    requires = {
        "url": ["token", "id_votacao"],
        "post": []
    }
    def get(args):
        id_votacao = args["url"][1]
        token = args["url"][0]
        votacao = db.getVotacaoFull(id_votacao, 3)
        aluno = db.getLogin(token)

        if not votacao or not aluno:
            return {
                "status": "FAILED",
                "message": "Votação ou token de login inválidos!"
            }

        if votacao["neo"]:
            if votacao["responsavel_neo"] == aluno["id_aluno"]:
                return {
                    "status": "OK",
                    "pode": True
                }
            else:
                return {
                    "status": "OK",
                    "pode": False
                }

        return {
            "status": "OK",
            "pode": True
        }

class GetMeuVoto(ServicoBase):
    description = ("É usado por umm usuário logado para descobrir em qual "
        "calendário ele votou.")
    requires = {
        "url": ["token", "id_votacao"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        id_votacao = args["url"][1]
        matricula = db.isValidToken(token)
        if matricula == False:
            return {
                "votou": False,
                "logado": False
            }
        aluno = db.getAlunoByMatricula(matricula)
        voto = db.getMeuVoto(id_votacao, aluno["id_aluno"])
        enviou  = db.getSpecificCalendario(id_votacao, aluno["id_aluno"]) != None
        if voto == None:
            return {
                "votou": False,
                "logado": True,
                "enviou": enviou
            }
        else:
            return {
                "votou": True,
                "logado": True,
                "id": voto,
                "enviou": enviou
            }

class VotarEm(ServicoBase):
    description = "Usado por um usuário para votar em um calendário"
    requires = {
        "url": [],
        "post": ["token", "id_calendario", "id_votacao"],
    }
    def post(args):
        token = args["post"]["token"]
        id_calendario = args["post"]["id_calendario"]
        id_votacao = args["post"]["id_votacao"]
        matricula = db.isValidToken(token)
        if matricula == False:
            return {
                "status": "FAILED",
                "message": "Faça login lá em cima!"
            }
        aluno = db.getAlunoByMatricula(matricula)
        if db.jaTerminou(id_votacao):
            return {
                "status": "FAILED",
                "message": "A votação já acabou :("
            }
        db.setVoto(id_votacao, aluno["id_aluno"], id_calendario)
        return {
            "status": "OK",
            "message": "Você votou neste calendário!"
        }

class GetProfessores(ServicoBase):
    description = "Usado para obter a lista de professores."
    def get(args):
        professores = db.getProfessores()
        return json.dumps(professores)

class AddProfessor(ServicoBase):
    description = "Usado por um administrador para adicionar um professor."
    requires = {
        "url": [],
        "post": ["user", "senha", "nome", "senha_prof", "id_materia"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        nome = args["post"]["nome"]
        senha_prof = args["post"]["senha_prof"]
        id_materia = args["post"]["id_materia"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        if not nome or not senha_prof or not id_materia:
            return {
                "status": "FAILED",
                "message": "Não deixe nenhum dos campos em branco!"
            }

        db.addProfessor(nome, senha_prof, id_materia)

        return {
            "status": "OK",
            "professores": db.getProfessores()
        }

class EditProfessor(ServicoBase):
    description = "Usado por um administrador para editar um professor."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_professor", "nome", "senha_prof",
            "id_materia"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_professor = args["post"]["id_professor"]
        nome = args["post"]["nome"]
        senha_prof = args["post"]["senha_prof"]
        id_materia = args["post"]["id_materia"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        if not id_professor:
            return {
                "status": "FAILED",
                "message": "Especifique um professor!"
            }

        db.editProfessor(id_professor, nome, senha_prof, id_materia)

        return {
            "status": "OK",
            "professores": db.getProfessores(),
            "id_professor": id_professor
        }

class RemoverProfessor(ServicoBase):
    description = "Usado por um administrador para remover um professor."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_professor"],
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_professor = args["post"]["id_professor"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        db.removerProfessor(id_professor)
        return {
            "status": "OK",
            "id_removido": id_professor
        }

class CheckProfessor(ServicoBase):
    description = "Usado para se autenticar como professor."
    requires = {
        "url": [],
        "post": ["nome", "senha"]
    }
    def post(args):
        nome = args["post"]["nome"]
        senha = args["post"]["senha"]
        id_professor = db.isProfessor(nome, senha)
        if id_professor == None:
            return {
                "status": "FAILED",
                "message": "Login inválido!"
            }
        else:
            return {
                "status": "OK",
                "id_professor": id_professor
            }

class CriarAviso(ServicoBase):
    description = "Usado por um professor criar um aviso com arquivos."
    requires = {
        "url": [],
        "post": ["nome", "senha", "titulo", "texto", "tem_arquivo", "arquivo",
            "ano"]
    }
    def post(args):
        nome = args["post"]["nome"]
        senha = args["post"]["senha"]
        titulo = args["post"]["titulo"]
        texto = args["post"]["texto"]
        tem_arquivo = args["post"]["tem_arquivo"]
        arquivo_str = args["post"]["arquivo"]
        ano = args["post"]["ano"]

        id_professor = db.isProfessor(nome, senha)

        if id_professor == None:
            return {
                "status": "FAILED",
                "message": "Login inválido!"
            }

        if not titulo or not texto:
            return {
                "status": "FAILED",
                "message": "Insira título e texto!"
            }

        if tem_arquivo == "true":
            arquivo = json.loads(arquivo_str)
            filename = arquivo["name"]
            base64_contents = arquivo["contents"]

            if len(base64_contents) % 4:
                base64_contents += "=" * (4 - len(value) % 4)

            if len(base64_contents) > (maxFileSize * 4/3):
                return {
                    "status": "FAILED",
                    "message": ("O arquivo \"" + filename + "\" excede o limite"
                        " de tamanho!")
                }

            validExtension = False
            for extension in allowedExtensions:
                if filename.endswith("." + extension):
                    validExtension = True
                    break
            if not validExtension:
                return {
                    "status": "FAILED",
                    "message": ("O arquivo é de um formato não-permitido! Só se"
                        " pode enviar arquivos terminados em: "
                        + (", ".join(allowedExtensions)) + ".")
                }

            contents = base64.urlsafe_b64decode(base64_contents.encode("ascii"))
            id_arquivo = db.addArquivo(filename, contents, id_professor, False)
        else:
            id_arquivo = None

        db.criarAviso(id_professor, titulo, texto, id_arquivo, ano)

        return {
            "status": "OK"
        }

class GetMeusAvisos(ServicoBase):
    description = "Usado por um professor para obter seus avisos."
    requires = {
        "url": [],
        "post": ["nome", "senha"]
    }
    def post(args):
        nome = args["post"]["nome"]
        senha = args["post"]["senha"]

        id_professor = db.isProfessor(nome, senha)

        if id_professor == None:
            return {
                "status": "FAILED",
                "message": "Login inválido!"
            }

        return {
            "status": "OK",
            "avisos": json.dumps(db.getMeusAvisos(id_professor))
        }

class EditarAviso(ServicoBase):
    requires = "Usado por um professor para editar um de seus avisos."
    requires = {
        "url": [],
        "post": ["nome", "senha", "id_aviso", "titulo", "texto", "ano"]
    }
    def post(args):
        nome = args["post"]["nome"]
        senha = args["post"]["senha"]
        id_aviso = args["post"]["id_aviso"]
        titulo = args["post"]["titulo"]
        texto = args["post"]["texto"]
        ano = args["post"]["ano"]

        id_professor = db.isProfessor(nome, senha)

        if id_professor == None:
            return {
                "status": "FAILED",
                "message": "Login inválido!"
            }

        if not titulo or not texto:
            return {
                "status": "FAILED",
                "message": "Não deixe nada em branco!"
            }

        aviso = db.getAvisoById(id_aviso)

        if not aviso:
            return {
                "status": "FAILED",
                "message": "Esse aviso não existe!"
            }

        if str(aviso["id_professor"]) != str(id_professor):
            return {
                "status": "FAILED",
                "message": "Esse aviso não é seu!"
            }

        db.editAviso(id_aviso, titulo, texto, ano)

        return {
            "status": "OK"
        }

class DeletarAviso(ServicoBase):
    requires = "Usado por um professor para deletar um de seus avisos."
    requires = {
        "url": [],
        "post": ["nome", "senha", "id_aviso"]
    }
    def post(args):
        nome = args["post"]["nome"]
        senha = args["post"]["senha"]
        id_aviso = args["post"]["id_aviso"]

        id_professor = db.isProfessor(nome, senha)

        if id_professor == None:
            return {
                "status": "FAILED",
                "message": "Login inválido!"
            }

        aviso = db.getAvisoById(id_aviso)

        if not aviso:
            return {
                "status": "FAILED",
                "message": "Esse aviso não existe!"
            }

        if str(aviso["id_professor"]) != str(id_professor):
            return {
                "status": "FAILED",
                "message": "Esse aviso não é seu!"
            }

        db.deleteAviso(id_aviso)

        return {
            "status": "OK"
        }

class GetAvisosByAno(ServicoBase):
    description = "Usado para obter os avisos de um dado ano, ou todos."
    requires = {
        "url": ["ano"],
        "post": []
    }
    def get(args):
        ano = args["url"][0]

        if not ano or ano == "0":
            return json.dumps(db.getAllAvisos())
        else:
            return json.dumps(db.getAvisosByAno(ano))

class GetArquivo(ServicoBase):
    description = "Usado para obter detalhes sobre um arquivo."
    requires = {
        "url": ["id_arquivo"],
        "post": []
    }
    def get(args):
        id_arquivo = args["url"][0]
        arquivo = db.getArquivoByID(id_arquivo)

        if not arquivo:
            return {
                "status": "FAILED"
            }
        else:
            return {
                "status": "OK",
                "id_arquivo": id_arquivo,
                "hash": arquivo["hash"],
                "filename": arquivo["filename"],
                "id_enviou": arquivo["id_enviou"],
                "aluno": arquivo["aluno"]
            }

class ContagemResumos(ServicoBase):
    description = "Retorna o número de resumos."
    def get(args):
        resumos = db.getResumosSemTexto()
        total = 0
        porano = {
            "1": 0,
            "2": 0,
            "3": 0
        }
        for resumo in resumos:
            total += 1
            porano[resumo["ano"]] += 1
        return {
            "total": total,
            "por_ano": porano
        }

class PesquisarResumos(ServicoBase):
    description = "Usado para pesquisar resumos usando palavras-chave."
    requires = {
        "url": ["query"],
        "post": []
    }
    def get(args):
        query = str_simplify(args["url"][0].strip())
        if (len(query) < 5):
            return {
                "status": "FAILED",
                "message": "Digite pelo menos 5 caracteres!"
            }
        aulas = db.getAllAulas()
        materias = db.getMaterias()
        fullarr = materias + aulas
        words = query.split(" ")
        id_materia = None
        ano = None
        not_limiting_words = []
        for word in words:
            imp = False

            if len(word) == 1:
                if word in ["1", "2", "3"]:
                    ano = word
                    continue

            for item in fullarr:
                if not item["id_materia"]:
                    continue
                thenome = str_simplify(item["nome"])
                theid = item["id_materia"]
                if word == thenome:
                    imp = True
                    if id_materia == None:
                        id_materia = theid

            if not imp and len(word) >= 4:
                not_limiting_words.append(word)

        if id_materia == None:
            resumos = db.getResumosSemTexto()
        else:
            resumos = db.getResumosSemTextoByMateria(id_materia)

        resultados = []
        if not_limiting_words == []:
            for resumo in resumos:
                if ano != None:
                    if resumo["ano"] != ano:
                        continue
                resultados.append(resumo)
        else:
            for resumo in resumos:
                if ano != None:
                    if resumo["ano"] != ano:
                        continue
                for word in not_limiting_words:
                    if (word in str_simplify(resumo["assunto"])
                        and resumo not in resultados):
                        resultados.append(resumo)

        return {
            "status": "OK",
            "resumos": json.dumps(resultados)
        }

class GetResumosOldWay(ServicoBase):
    description = "Usado para achar resumos do jeito antigo"
    requires = {
        "url": ["ano", "id_materia"],
        "post": []
    }
    def get(args):
        ano = args["url"][0]
        id_materia = args["url"][1]
        resultados = db.getResumosSemTextoOld(id_materia, ano)

        return {
            "status": "OK",
            "resumos": json.dumps(resultados)
        }

class GetResumosByEPA(ServicoBase):
    description = "Usado para obter resumos de acordo com Etapa, Período e Ano."
    requires = {
        "url": ["etapa", "periodo", "ano"],
        "post": []
    }
    def get(args):
        etapa = int(args["url"][0])
        periodo = int(args["url"][1])
        ano = args["url"][2]
        resumos = db.getResumosSemTexto()
        resultados = []
        for resumo in resumos:
            if resumo["ano"] != ano:
                continue
            per = int(resumo["periodo"])
            eta = int(resumo["etapa"])
            if periodo > per or (per == periodo and etapa >= eta):
                resultados.append(resumo)
        return json.dumps(resultados)

class GetResumoFull(ServicoBase):
    description = "Usado para obter tudo sobre um resumo."
    requires = {
        "url": ["miniurl"],
        "post": []
    }
    def get(args):
        mini = args["url"][0].lower()
        resumo = db.getResumoByMini(mini)

        if resumo == None:
            return {
                "status": "FAILED",
                "message": "Esse resumo não existe!",
                "code": "404"
            }

        if resumo["removido"]:
            return {
                "status": "FAILED",
                "message": "Esse resumo foi removido!",
                "code": "removido"
            }

        return {
            "status": "OK",
            "resumo": resumo
        }

class EnviarResumo(ServicoBase):
    description = "Usado para enviar um resumo."
    requires = {
        "url": [],
        "post": ["ano", "assunto", "etapa", "periodo", "id_materia", "miniurl",
            "html", "token"]
    }
    def post(args):
        ano = args["post"]["ano"]
        assunto = args["post"]["assunto"].strip()
        etapa = args["post"]["etapa"]
        periodo = args["post"]["periodo"]
        id_materia = args["post"]["id_materia"]
        miniurl = args["post"]["miniurl"].strip().lower()
        html = base64.urlsafe_b64decode(args["post"]["html"].strip())
        token = args["post"]["token"]

        if (len(miniurl) < 1):
            return {
                "status": "FAILED",
                "message": "Seu resumo precisa de uma URL!"
            }

        if not brutils.is_valid_mini(miniurl):
            return {
                "status": "FAILED",
                "message": "Só são permitidas letras, números, - e _ na URL!"
            }

        if (db.getResumoByMini(miniurl) != None):
            return {
                "status": "FAILED",
                "message": "Já existe um resumo com essa URL!"
            }

        if (len(assunto) < 1):
            return {
                "status": "FAILED",
                "message": "Especifique o assunto do seu resumo!"
            }

        if (len(html) < 1):
            return {
                "status": "FAILED",
                "message": "Seu resumo não tem nada nele?"
            }

        matricula = db.isValidToken(token)

        if matricula == None:
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        autor = db.getAlunoByMatricula(matricula)

        if autor["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }

        db.addResumo(autor, id_materia, miniurl, html, ano, etapa, periodo,
            assunto)

        return  {
            "status": "OK"
        }

class PossoEditar(ServicoBase):
    description = "Usado para saber se você pode editar um certo resumo."
    requires = {
        "url": ["miniurl", "token"],
        "post": []
    }
    def get(args):
        miniurl = args["url"][0]
        token = args["url"][1]

        matricula = db.isValidToken(token)

        if not matricula:
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        resumo = db.getResumoByMini(miniurl)

        if not resumo:
            return {
                "status": "FAILED",
                "message": "Esse resumo não existe!"
            }

        aluno = db.getAlunoByMatricula(matricula)

        if resumo["nome_autor"] == aluno["nome"]:
            return {
                "status": "OK"
            }
        else:
            return {
                "status": "FAILED",
                "proibido": True
            }

class EditarResumo(ServicoBase):
    description = "Usado por um aluno para editar um resumo"
    requires = {
        "url": [],
        "post": ["token", "id_materia", "ano", "html", "assunto", "etapa",
            "periodo", "miniurl"]
    }
    def post(args):
        ano = args["post"]["ano"]
        assunto = args["post"]["assunto"].strip()
        etapa = args["post"]["etapa"]
        periodo = args["post"]["periodo"]
        id_materia = args["post"]["id_materia"]
        miniurl = args["post"]["miniurl"].strip()
        html = base64.urlsafe_b64decode(args["post"]["html"].strip())
        token = args["post"]["token"]

        if (len(miniurl) < 1):
            return {
                "status": "FAILED",
                "message": "Seu resumo precisa de uma URL!"
            }

        if (len(assunto) < 1):
            return {
                "status": "FAILED",
                "message": "Especifique o assunto do seu resumo!"
            }

        if (len(html) < 1):
            return {
                "status": "FAILED",
                "message": "Seu resumo não tem nada nele?"
            }

        resumo = db.getResumoByMini(miniurl)

        if not resumo:
            return {
                "status": "FAILED",
                "message": "Esse resumo não existe!"
            }

        matricula = db.isValidToken(token)

        if matricula == None:
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        autor = db.getAlunoByMatricula(matricula)

        if resumo["nome_autor"] != autor["nome"]:
            return {
                "status": "FAILED",
                "message": "Esse resumo não é seu!"
            }

        db.updateResumo(id_materia, miniurl, html, ano, etapa, periodo,
         assunto)

        return {
            "status": "OK"
        }

class PossoEditar(ServicoBase):
    description = "Usado para sabeir se você pode editar um certo resumo."
    requires = {
        "url": ["mini", "token"],
        "post": []
    }
    def get(args):
        mini = args["url"][0]
        token = args["url"][1]

        matricula = db.isValidToken(token)

        if not matricula:
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        aluno = db.getAlunoByMatricula(matricula)
        resumo = db.getResumoByMini(mini)

        if not resumo:
            return {
                "status": "FAILED",
                "message": "Esse resumo não existe!"
            }

        if resumo["nome_autor"] == aluno["nome"]:
            return {
                "status": "OK"
            }
        else:
            return {
                "status": "FAILED",
                "proibido": True,
                "message": "Esse resumo não é seu!"
            }

class ToggleRemoverResumo(ServicoBase):
    description = "Usado pelo autor para remover um resumo seu."
    requires = {
        "url": ["token", "mini"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        mini = args["url"][1]
        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Seu login não é válido!"
            }

        resumo = db.getResumoByMini(mini)

        if not resumo:
            return {
                "status": "FAILED",
                "message": "Esse resumo não existe!",
            }

        if resumo["nome_autor"] != aluno["nome"]:
            return {
                "status": "FAILED",
                "message": "Esse resumo não é seu!"
            }

        removido = db.toggleRemoverResumo(resumo)

        ret = {
            "status": "OK",
            "removido": removido,
        }

        if removido:
            ret["message"] = "Resumo removido!"
        else:
            ret["message"] = "Resumo restaurado!"

        return ret

class MeusResumos(ServicoBase):
    description = "Usado para obter a lista de todos seus resumos."
    requires = {
        "url": ["token"],
        "post": []
    }
    def get(args):
        token = args["url"][0]

        aluno = db.getLogin(token)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você precisa estar logado!"
            }

        resumos = db.listResumosByAutor(aluno["nome"])
        return json.dumps(resumos)

class GetReforcos(ServicoBase):
    description = "Usado para obter todos os reforços no sistema."
    def get(args):
        reforcos = db.getReforcos()
        return json.dumps(reforcos)

class AddReforco(ServicoBase):
    description = "Usado por um admin para adicionar um reforço."
    requires = {
        "url": [],
        "post": ["user", "senha", "hora_inicio", "hora_fim", "sala", "ano",
            "nomemateria", "professor", "dias_array"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        hora_inicio = args["post"]["hora_inicio"]
        hora_fim = args["post"]["hora_fim"]
        sala = args["post"]["sala"]
        ano = args["post"]["ano"]
        nomemateria = args["post"]["nomemateria"]
        professor = args["post"]["professor"]
        dias_array = args["post"]["dias_array"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        db.addReforco(hora_inicio, hora_fim, sala, ano, nomemateria, professor,
            dias_array)

        return {
            "status": "OK"
        }

class EditReforco(ServicoBase):
    description = "Usado por um admin para editar um reforço."
    requires = {
        "url": [],
        "post": ["user", "senha", "hora_inicio", "hora_fim", "sala", "ano",
            "nomemateria", "professor", "dias_array", "id_reforco"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        hora_inicio = args["post"]["hora_inicio"]
        hora_fim = args["post"]["hora_fim"]
        sala = args["post"]["sala"]
        ano = args["post"]["ano"]
        nomemateria = args["post"]["nomemateria"]
        professor = args["post"]["professor"]
        dias_array = args["post"]["dias_array"]
        id_reforco = args["post"]["id_reforco"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        db.editReforco(id_reforco, hora_inicio, hora_fim, sala, ano,
            nomemateria, professor, dias_array)

        return {
            "status": "OK",
            "reforcos": json.dumps(db.getReforcos()),
            "id_editado": id_reforco
        }

class RemoveReforco(ServicoBase):
    description = "Usado por um admin para remover um reforço."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_reforco"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_reforco = args["post"]["id_reforco"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Seu login é inválido!"
            }

        db.removeReforco(id_reforco)

        return {
            "status": "OK",
            "id_removido": id_reforco
        }

class GetHorario(ServicoBase):
    description = "Usado para obter o horário de aulas de uma sala."
    requires = {
        "url": ["ano", "sala"],
        "post": []
    }
    def get(args):
        ano = args["url"][0]
        sala = args["url"][1]

        horario = db.getHorarioBySala(ano, sala)

        if horario:
            lasteditor = db.getAlunoById(horario["ultimo_editor"])
            return {
                "has": True,
                "horario": horario["horario"],
                "last_editor_nome": lasteditor["nome"],
                "last_editor_chamada": lasteditor["chamada"],
                "last_edit_time": horario["lastedit"]
            }
        else:
            return {
                "has": False
            }

class SetHorario(ServicoBase):
    description = "Usado para atualizar o horário de aulas de uma sala."
    requires = {
        "url": [],
        "post": ["token", "arr"]
    }
    def post(args):
        token = args["post"]["token"]
        arr = args["post"]["arr"]

        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Faça login antes!"
            }
        if aluno["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }

        db.setHorario(aluno, arr)

        return {
            "status": "OK"
        }

class GetFeitas(ServicoBase):
    description = "Usado por um aluno para obter suas lições feitas."
    requires = {
        "url": ["token"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        aluno = db.getLogin(token)

        if aluno:
            feitas = db.getFeitas(aluno["id_aluno"])
            if feitas:
                return {
                    "has": True,
                    "feitas": feitas["feitas"]
                }

        return {
            "has": False
        }

class SetFeitas(ServicoBase):
    description = "Usado por um aluno para salvar suas lições feitas."
    requires = {
        "url": [],
        "post": ["token", "feitas"]
    }
    def post(args):
        token = args["post"]["token"]
        feitas = args["post"]["feitas"]
        aluno = db.getLogin(token)

        if aluno:
            db.setFeitas(aluno["id_aluno"], feitas)
            return {
                "status": "OK"
            }

        return {
            "status": "FAILED"
        }

class ToggleFeita(ServicoBase):
    description = "Usada por um aluno para mudar se uma lição está feita."
    requires = {
        "url": ["token", "id_licao"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        id_licao = int(args["url"][1])
        aluno = db.getLogin(token)

        if aluno:
            feitas_str = db.getFeitas(aluno["id_aluno"])
            if feitas_str:
                feitas = json.loads(feitas_str["feitas"])
            else:
                feitas = []
            if id_licao in feitas:
                feitas.remove(id_licao)
            else:
                feitas.append(id_licao)
            db.setFeitas(aluno["id_aluno"], json.dumps(feitas))
            return {
                "status": "OK"
            }

        return {
            "status": "FAILED"
        }

class GetMsgSala(ServicoBase):
    description = "Usado para obter o a mensagem do dia de uma sala."
    requires = {
        "url": ["ano", "sala"],
        "post": []
    }
    def get(args):
        ano = args["url"][0]
        sala = args["url"][1]

        msg = db.getMsgSala(ano, sala)

        if msg:
            lasteditor = db.getAlunoById(msg["ultimo_editor"])
            return {
                "has": True,
                "texto": msg["texto"],
                "last_editor_nome": lasteditor["nome"],
                "last_editor_chamada": lasteditor["chamada"],
                "last_edit_time": msg["lastedit"]
            }
        else:
            return {
                "has": False
            }

class SetMsgSala(ServicoBase):
    description = "Usado para atualizar a mensagem do dia de uma sala."
    requires = {
        "url": [],
        "post": ["token", "texto"]
    }
    def post(args):
        token = args["post"]["token"]
        texto = base64.urlsafe_b64decode(args["post"]["texto"])

        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Faça login antes!"
            }
        if aluno["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }

        db.setMsgSala(aluno, texto)

        return {
            "status": "OK"
        }

class PesquisarLicoes(ServicoBase):
    description = "Usado para pesquisar lições antigas."
    requires = {
        "url": [],
        "post": ["ano", "sala", "aula", "envio_start", "envio_end",
            "entrega_start", "entrega_end"]
    }
    def post(args):
        licoes = db.pesquisarLicoes(**args["post"])
        return json.dumps(licoes)

class GetCursos(ServicoBase):
    description = "Usado para obter a lista de cursos."
    def get(args):
        cursos = db.getCursos()
        return json.dumps(cursos)

class GetLicoesCursos(ServicoBase):
    description = "Usado para obter as lições ativas dos cursos."
    def get(args):
        licoes = db.getLicoesAtivasCursos()
        return json.dumps(licoes)

class EnviarLicaoCurso(ServicoBase):
    description = "Usado por um aluno para enviar uma lição de curso."
    requires = {
        "url": [],
        "post": ["token", "id_curso", "para", "info"]
    }
    def post(args):
        token = args["post"]["token"]
        id_curso = args["post"]["id_curso"]
        para = args["post"]["para"]
        info = args["post"]["info"]
        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você precisa fazer login!"
            }
        if str(aluno["ano"]) != "3":
            return {
                "status": "FAILED",
                "message": "Você nem é do terceiro ano!"
            }
        if aluno["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }

        db.adicionarLicaoCurso(id_curso, aluno["id_aluno"], para, info)

        return {
            "status": "OK",
            "message": "Lição adicionada!"
        }

class EditarLicaoCurso(ServicoBase):
    description = "Usado por um aluno para editar uma lição de curso."
    requires = {
        "url": [],
        "post": ["token", "id_licao", "id_curso", "para", "info"]
    }
    def post(args):
        token = args["post"]["token"]
        id_licao = args["post"]["id_licao"]
        id_curso = args["post"]["id_curso"]
        para = args["post"]["para"]
        info = args["post"]["info"]
        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você precisa fazer login!"
            }
        if aluno["ano"] != "3":
            return {
                "status": "FAILED",
                "message": "Você nem é do terceiro ano!"
            }
        if aluno["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }

        db.editarLicaoCurso(id_licao, id_curso, aluno["id_aluno"], para, info)

        return {
            "status": "OK",
            "message": "Lição editada com sucesso! Voltando..."
        }

class RemoverLicaoCurso(ServicoBase):
    description = "Usado por um aluno para remover uma lição de curso."
    requires = {
        "url": ["token", "id_licao"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        id_licao = args["url"][1]
        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você precisa fazer login!"
            }
        if aluno["ano"] != "3":
            return {
                "status": "FAILED",
                "message": "Você nem é do terceiro ano!"
            }

        db.removerLicaoCurso(id_licao, aluno["id_aluno"])

        return {
            "status": "OK",
            "message": "Lição removida com sucesso!"
        }

class GetMateriasProva(ServicoBase):
    description = "Usado para obter as matérias de prova."
    def get(args):
        materias_prova = db.getMateriasProva()
        return json.dumps(materias_prova)

class SetMateriaProva(ServicoBase):
    description = "Usado para setar uma matéria de prova."
    requires = {
        "url": [],
        "post": ["token", "id_materia", "etapa", "periodo", "info", "ano"]
    }
    def post(args):
        token = args["post"]["token"]
        id_materia = args["post"]["id_materia"]
        etapa = args["post"]["etapa"]
        periodo = args["post"]["periodo"]
        info = args["post"]["info"]
        supostoAno = args["post"]["ano"]
        aluno = db.getLogin(token)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você precisa fazer login!"
            }
        if aluno["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido!"
            }
        if str(aluno["ano"]) != str(supostoAno):
            return {
                "status": "FAILED",
                "message": "Você não é do " + str(supostoAno) + "º ano!"
            }

        db.setMateriaProva(id_materia, etapa, periodo, info, aluno["id_aluno"],
            aluno["ano"])

        return {
            "status": "OK"
        }

class GetAlunosLista(ServicoBase):
    description = "Usado para obter os nomes baseando-se em pesquisa."
    requires = {
        "url": ["query"],
        "post": []
    }
    def get(args):
        query = args["url"][0]
        if len(query) < 4:
            return {
                "status": "FAILED",
                "message": "Digite mais!"
            }
        alunos = db.queryAlunos(str_simplify(query))
        return json.dumps(alunos)

class GetSalasProvaEP(ServicoBase):
    description = "Usado para obter os períodos onde se tem as salas de prova."
    def get(args):
        return json.dumps(db.getSalasProvaPeriodo())

class GetTodasSalasProva(ServicoBase):
    description = "Usado para puxar todas as salas de prova, todas."
    def get(args):
        todas = []
        for periodo in range(1, 5):
            todas += db.getAllSalasProva(periodo)
        return json.dumps(todas)

class GetMinhaSalaProva(ServicoBase):
    description = "Usado para obter a sua sala de prova."
    requires = {
        "url": ["periodo", "ano", "sala", "chamada"],
        "post": []
    }
    def get(args):
        periodo = args["url"][0]
        ano = args["url"][1]
        sala = args["url"][2]
        chamada = args["url"][3]
        if not chamada:
            return {
                "status": "FAILED",
                "message": "Entre com seu número de chamada!"
            }
        salaprova = db.getMinhaSalaProva(periodo, ano, sala, chamada)
        if salaprova:
            return {
                "status": "OK",
                "id_sala_prova": salaprova
            }
        else:
            return {
                "status": "FAILED",
                "message": "Não conseguimos te encontrar!"
            }

class GetFullSalaProva(ServicoBase):
    description = "Usado para obter uma sala de prova específica."
    requires = {
        "url": ["id_sala_prova"],
        "post": []
    }
    def get(args):
        id_sala_prova = args["url"][0]
        return db.getSalaProva(id_sala_prova)

class GetCurrentMOTD(ServicoBase):
    description = "Usado para obter o MOTD atual."
    def get(args):
        return "\"" + db.getCurrentMOTD() + "\""

class EnviarMOTD(ServicoBase):
    description = "Usado por um admin para setar o MOTD."
    requires = {
        "url": [],
        "post": ["user", "senha", "motd"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        motd = args["post"]["motd"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.setMOTD(motd, user)

        return {
            "status": "OK",
        }

class CriarSeminario(ServicoBase):
    description = "Usado por um aluno para colocar seu seminário no site."
    requires = {
        "url": [],
        "post": ["token", "tema", "link", "arquivo", "tem_arquivo"]
    }
    def post(args):
        token = args["post"]["token"]
        tema = args["post"]["tema"]
        link = args["post"]["link"]
        arquivo_str = args["post"]["arquivo"]
        tem_arquivo = args["post"]["tem_arquivo"]

        aluno = db.getLogin(token)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Parece que você não está logado!"
            }

        if not str(aluno["ano"]) == "2":
            return {
                "status": "FAILED",
                "message": "Você não é do segundo ano!"
            }

        if not tema:
            return {
                "status": "FAILED",
                "message": "Tema em branco? Sério?"
            }

        seminarios = db.getSeminarios()
        for seminario in seminarios:
            if seminario["id_aluno"] == aluno["id_aluno"]:
                return {
                    "status": "FAILED",
                    "message": "Você já enviou um seminário!"
                }

        if tem_arquivo == "true":
            if not arquivo_str:
                return {
                    "status": "FAILED",
                    "message": "Erro no envio do arquivo (sem conteúdo)!"
                }
            arquivo = json.loads(arquivo_str)
            filename = arquivo["name"]
            base64_contents = arquivo["contents"]

            if len(base64_contents) % 4:
                base64_contents += "=" * (4 - len(value) % 4)

            if len(base64_contents) > (maxFileSize * 4/3):
                return {
                    "status": "FAILED",
                    "message": ("O arquivo \"" + filename + "\" excede o limite"
                        " de tamanho!")
                }

            validExtension = False
            extseminarios = ["ppt", "pptx", "pptm", "ppsx", "ppsm", "pps",
                             "odp", "pdf", "tiff", "doc", "docx"]
            for extension in extseminarios:
                if filename.endswith("." + extension):
                    validExtension = True
                    break
            if not validExtension:
                return {
                    "status": "FAILED",
                    "message": ("O arquivo é de um formato não-permitido! Só se"
                        " pode enviar arquivos terminados em: "
                        + (", ".join(extseminarios)) + ".")
                }

            contents = base64.urlsafe_b64decode(base64_contents.encode("ascii"))
            id_arquivo = db.addArquivo(filename, contents, aluno["id_aluno"], True)
        else:
            id_arquivo = None
            if not link:
                return {
                    "status": "FAILED",
                    "message": "Link vazio? Sério?"
                }
            if "javascript" in link:
                return {
                    "status": "FAILED",
                    "message": "Boa tentativa."
                }

        db.criarSeminario(aluno["id_aluno"], tema, link, id_arquivo)

        return {
            "status": "OK"
        }

class RemoverSeminario(ServicoBase):
    description = "Usado por um aluno para remover um seminário do site."
    requires = {
        "url": ["token", "id_seminario"],
        "post": []
    }
    def get(args):
        token = args["url"][0]
        id_seminario = args["url"][1]

        aluno = db.getLogin(token)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Você não está logado!"
            }

        seminarios = db.getSeminarios()
        for seminario in seminarios:
            if (str(seminario["id_seminario"]) == str(id_seminario)
                and str(seminario["id_aluno"]) == str(aluno["id_aluno"])):
                db.removerSeminario(id_seminario)
                if seminario["id_arquivo"]:
                    arquivo = db.getArquivoByID(seminario["id_arquivo"])
                    db.deleteArquivo(arquivo["hash"])
                return {
                    "status": "OK"
                }

        return {
            "status": "FAILED",
            "message": "Nenhum seminário foi encontrado para remover..."
        }

class GetSeminarios(ServicoBase):
    description = "Usado para obter os seminários, com todas as informações."
    def get(args):
        return json.dumps(db.getSeminarios())

class GetIniciais(ServicoBase):
    description = "Usado para obter a visibilidade de cada link da homepage."
    def get(args):
        return json.dumps(db.getIniciais())

class SetarInicial(ServicoBase):
    description = "Usado para mudar a visibilidade de um link da homepage."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_link", "visivel"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_link = args["post"]["id_link"]
        visivel = bool(int(args["post"]["visivel"]))

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.setInicial(id_link, visivel)

        return {
            "status": "OK",
        }

class VerificarAlunoAprov(ServicoBase):
    description = "Usado por (ex-)alunos para entrar no sistema de aprovações."
    requires = {
        "url": [],
        "post": ["credenciais_json"]
    }
    def post(args):
        credenciais_json = args["post"]["credenciais_json"]
        aluno = db.verificarCredenciaisJSON(credenciais_json)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Seus dados de aluno são inválidos!"
            }
        else:
            return {
                "status": "OK",
                "seu_nome": aluno["nome"],
                "seu_id": aluno["id_aluno"],
                "seu_ano": aluno["ano"],
                "sua_sala": aluno["sala"],
                "seu_numero": aluno["chamada"],
                "seu_em": aluno["em"],
                "seu_nome_corrigido": aluno["correcao_nome"] or "",
                "aprovacoes": db.getAprovacoesAluno(aluno["id_aluno"])
            }

class EnviarCorrecaoNome(ServicoBase):
    description = "Usado para enviar a correção de nome por um aluno."
    requires = {
        "url": [],
        "post": ["credenciais_json", "correcao"]
    }
    def post(args):
        credenciais_json = args["post"]["credenciais_json"]
        correcao = args["post"]["correcao"]
        aluno = db.verificarCredenciaisJSON(credenciais_json)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Seus dados de aluno são inválidos!"
            }
        else:
            db.setCorrecaoNome(aluno["id_aluno"], correcao)
            return {
                "status": "OK"
            }

class GetAnoDiretor(ServicoBase):
    description = "Retorna o ano usado para determinar quem pode postar aprovações."
    def get(args):
        return {
            "ano_diretor": brutils.get_ano_diretor()
        }

class AdicionarAprovacao(ServicoBase):
    description = "Usado por um aluno para adicionar uma aprovação."
    requires = {
        "url": [],
        "post": ["credenciais_json", "nome_uni", "nome_curso", "publica", "rank",
                 "treineiro", "meio_de_ano", "sisu", "categoria"]
    }
    def post(args):
        credenciais_json = args["post"]["credenciais_json"]
        nome_uni = args["post"]["nome_uni"]
        nome_curso = args["post"]["nome_curso"]
        publica = bool(int(args["post"]["publica"]))
        if args["post"]["rank"] == "":
            args["post"]["rank"] = "0"
        rank = int(args["post"]["rank"])
        treineiro = bool(int(args["post"]["treineiro"]))
        meio_de_ano = bool(int(args["post"]["meio_de_ano"]))
        sisu = bool(int(args["post"]["sisu"]))
        categoria = args["post"]["categoria"]
        aluno = db.verificarCredenciaisJSON(credenciais_json)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Faça login!"
            }
        ano_diretor = brutils.get_ano_diretor()
        if ano_diretor == 0:
            return {
                "status": "FAILED",
                "message": "O sistema não está aberto!"
            }
        em = aluno["em"]
        ano = aluno["ano"]
        if not ((em == ano_diretor-1) or
            ((em == ano_diretor-2 or em == ano_diretor-3) and ano == 3)):
            return {
                "status": "FAILED",
                "message": ("Apenas alunos de " + str(ano_diretor-1) + ", ou "
                            "terceiranistas de " + str(ano_diretor-2) + " e "
                            + str(ano_diretor-3) + " podem postar!")
            }

        db.addAprovacao(aluno["id_aluno"], nome_uni, nome_curso, publica,
                        rank, treineiro, meio_de_ano, sisu, categoria)
        return {
            "status": "OK",
            "nova_lista": db.getAprovacoesAluno(aluno["id_aluno"])
        }

class EditarAprovacao(ServicoBase):
    description = "Usado por um aluno para editar uma aprovação."
    requires = {
        "url": [],
        "post": ["credenciais_json", "id_aprovacao", "nome_uni", "nome_curso",
                 "publica", "rank", "treineiro", "meio_de_ano", "sisu",
                 "categoria"]
    }
    def post(args):
        credenciais_json = args["post"]["credenciais_json"]
        id_aprovacao = args["post"]["id_aprovacao"]
        nome_uni = args["post"]["nome_uni"]
        nome_curso = args["post"]["nome_curso"]
        publica = bool(int(args["post"]["publica"]))
        if args["post"]["rank"]:
            rank = int(args["post"]["rank"])
        else:
            rank = 0
        treineiro = bool(int(args["post"]["treineiro"]))
        meio_de_ano = bool(int(args["post"]["meio_de_ano"]))
        sisu = bool(int(args["post"]["sisu"]))
        categoria = args["post"]["categoria"]
        aluno = db.verificarCredenciaisJSON(credenciais_json)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Faça login!"
            }
        if not db.aprovacaoPertence(id_aprovacao, aluno["id_aluno"]):
            return {
                "status": "FAILED",
                "message": "Essa aprovação não lhe pertence!"
            }

        db.editAprovacao(id_aprovacao, nome_uni, nome_curso, publica,
                        rank, treineiro, meio_de_ano, sisu, categoria)
        return {
            "status": "OK",
            "voce_editou": id_aprovacao,
            "nova_lista": db.getAprovacoesAluno(aluno["id_aluno"])
        }

class RemoverAprovacao(ServicoBase):
    description = "Usado por um aluno para remover uma aprovação."
    requires = {
        "url": [],
        "post": ["credenciais_json", "id_aprovacao"]
    }
    def post(args):
        credenciais_json = args["post"]["credenciais_json"]
        id_aprovacao = args["post"]["id_aprovacao"]
        aluno = db.verificarCredenciaisJSON(credenciais_json)
        if not aluno:
            return {
                "status": "FAILED",
                "message": "Faça login!"
            }
        if not db.aprovacaoPertence(id_aprovacao, aluno["id_aluno"]):
            return {
                "status": "FAILED",
                "message": "Essa aprovação não lhe pertence!"
            }
        db.removerAprovacao(id_aprovacao)
        return {
            "status": "OK",
            "nova_lista": db.getAprovacoesAluno(aluno["id_aluno"])
        }

class GetTodasAprovacoesPorAluno(ServicoBase):
    description = "Usado por um administrador para obter todas as aprovações."
    requires = {
        "url": [],
        "post": ["user", "senha"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }
        return json.dumps(db.getAprovacoesPorAluno())

class GetRelatorioAprovacoes(ServicoBase):
    description = ("Usado por um administrador para obter o relatório de"
        + "aprovações.")
    requires = {
        "url": [],
        "post": ["user", "senha"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }
        return db.getRelatorioAprovacoes()

class GetErrorLogs(ServicoBase):
    description = "Usado por um admin para obter logs de erro."
    requires = {
        "url": [],
        "post": ["user", "senha", "include_contents"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        include_contents = int(args["post"]["include_contents"])

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        logfiles = sorted(os.listdir(logfolder))
        results = []

        for logfile in logfiles:
            path = logfolder + logfile
            mtime = os.path.getmtime(path)
            fsize = os.path.getsize(path)
            obj = {
                "filename": logfile,
                "filesize": fsize,
                "last_write": mtime
            }
            if include_contents:
                with open(path, "r") as handler:
                    contents = handler.read()
                    obj["contents"] = contents
            results.append(obj)

        return json.dumps(results)

class ResetarAluno(ServicoBase):
    description = "Permite a um admin resetar a senha de um aluno."
    requires = {
        "url": [],
        "post": ["user", "senha", "matricula"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        matricula = args["post"]["matricula"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        if len(matricula) != 8:
            return {
                "status": "FAILED",
                "message": "Números de matrícula costumam ter 8 dígitos..."
            }

        aluno = db.getAlunoByMatricula(matricula)

        if not aluno:
            return {
                "status": "FAILED",
                "message": "Aluno não encontrado!"
            }

        db.resetarAluno(aluno["id_aluno"])

        return {
            "status": "OK",
            "message": "Aluno resetado com sucesso!"
        }

class QueryAlunos(ServicoBase):
    description = "Permite a um admin obter alunos da lista."
    requires = {
        "url": [],
        "post": ["user", "senha", "search_by", "query"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        search_by = args["post"]["search_by"]
        query = args["post"]["query"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        alunos = []

        if search_by == "matricula":
            if len(query) == 9:
                alunos.append(db.getAlunoBy("matricula", query))
        elif search_by == "ano_e_sala":
            if len(query) == 2:
                by_ano = db.getAlunosBy("ano", query[0])
                for a in by_ano:
                    if a["sala"] == query[1]:
                        alunos.append(a)

        for index, aluno in enumerate(alunos):
            if not aluno:
                del alunos[index]
                continue
            alunos[index]["registrado"] = bool(aluno["opaque"])
            del alunos[index]["salt"]
            del alunos[index]["opaque"]
            alunos[index]["licoes"] = db.contarLicoesAluno(aluno["id_aluno"])
            alunos[index]["resumos"] = db.contarResumosAluno(aluno["id_aluno"])

        return json.dumps(alunos)

class GetManuais(ServicoBase):
    description = "Retorna os manuais de administração do site."
    def get(args):
        arquivos = sorted(os.listdir("manuel/"))
        arr = []
        for fname in arquivos:
            path = "manuel/" + fname
            with open(path, "r") as handler:
                contents = handler.read()
                lines = contents.split("\n");
                arr.append({
                    "fname": fname[:-7],
                    "categoria": lines[0],
                    "titulo": lines[1],
                    "texto": "\n".join(lines[2:]).replace("\n\n", "<br><br>")
                    .replace("\n", " ")
                })
        return json.dumps(arr)

class GetAjudas(ServicoBase):
    description = "Retorna os tópicos de ajuda."
    def get(args):
        arquivos = sorted(os.listdir("ajuda/"))
        arr = []
        for fname in arquivos:
            path = "ajuda/" + fname
            with open(path, "r") as handler:
                contents = handler.read()
                lines = contents.split("\n");
                arr.append({
                    "fname": fname[:-6],
                    "categoria": lines[0],
                    "titulo": lines[1],
                    "texto": "\n".join(lines[2:]).replace("\n\n", "<br><br>")
                    .replace("\n", " ")
                })
        return json.dumps(arr)

class HandleCrash(ServicoBase):
    description = "Usado quando um erro é encontrado."
    requires = {
        "url": [],
        "post": ["crash_json"]
    }
    def post(args):
        crash_json = args["post"]["crash_json"]
        crash = json.loads(crash_json)
        if not db.existemCrashesSimilares(crash["Classe do erro"]):
            content = db.getEmailTemplate("crash") % (crash_json)
            adm_email = db.getConfig()["admin_email"]
            subject = "Relatório de erro automático do Site"
            textvers = "Relatório de erro automático do Site\n\n" + crash_json
            sendHTMLEmail("erros_automaticos", adm_email, subject, content, textvers)
            emailed = True
        else:
            emailed = False
        db.pushCrashToDatabase(crash_json, emailed)
        with open("templates/crash.html") as content:
            return {
                "status": "OK",
                "pretty_error": crash_json,
                "emailed": emailed,
                "crash_template": content.read()
            }

class GetRegexes(ServicoBase):
    description = "Retorna todas as regexes para pesquisa de alunos."
    def get(args):
        return json.dumps(db.getRegexes())

class AddRegex(ServicoBase):
    description = "Cria uma regex para pesquisa de alunos."
    requires = {
        "url": [],
        "post": ["user", "senha", "nome", "detalhes", "pattern"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        nome = args["post"]["nome"]
        detalhes = args["post"]["detalhes"]
        pattern = args["post"]["pattern"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.addRegex(nome, detalhes, pattern)

        return {
            "status": "OK",
            "regexes": db.getRegexes()
        }

class EditRegex(ServicoBase):
    description = "Edita uma regex de pesquisa de alunos."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_regex", "nome", "detalhes", "pattern"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_regex = args["post"]["id_regex"]
        nome = args["post"]["nome"]
        detalhes = args["post"]["detalhes"]
        pattern = args["post"]["pattern"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.editRegex(id_regex, nome, detalhes, pattern)

        return {
            "status": "OK",
            "regexes": db.getRegexes(),
            "id_editada": id_regex
        }

class RemoveRegex(ServicoBase):
    description = "Edita uma regex de pesquisa de alunos."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_regex"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_regex = args["post"]["id_regex"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.removeRegex(id_regex)

        return {
            "status": "OK",
            "regexes": db.getRegexes(),
            "id_removida": id_regex
        }

class ExtrairNomes(ServicoBase):
    description = "Usado para extrair nomes de listas de aprovação."
    requires = {
        "url": [],
        "post": ["user", "senha", "alvo"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        #id_regex = args["post"]["id_regex"]
        alvo = args["post"]["alvo"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        alvo = str_simplify(alvo)
        extraidos = []
        alunos = db.alunosParaExtrair()
        for aluno in alunos:
            nome = str_simplify(aluno["nome"])
            temcorr = False
            if aluno["correcao_nome"]:
                corrnome = str_simplify(aluno["correcao_nome"])
                temcorr = corrnome in alvo
            if nome in alvo or temcorr:
                extraidos.append(aluno)
        porem = {}
        for tar in extraidos:
            ns = str_simplify(tar["nome"])
            if ns not in porem:
                porem[ns] = int(tar["em"])
            else:
                if porem[ns] < int(tar["em"]):
                    porem[ns] = int(tar["em"])
        semdupes = []
        for k in extraidos:
            ns = str_simplify(k["nome"])
            if int(k["em"]) == int(porem[ns]):
                semdupes.append(k)

        return {
            "status": "OK",
            "extraidos": semdupes
        }

class AdicionarSalaProva(ServicoBase):
    description = "Usado por um administrador para adicionar uma sala de prova."
    requires = {
        "url": [],
        "post": ["user", "senha", "mapa"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        mapa = json.loads(args["post"]["mapa"])

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.addSalaProva(mapa);

        return {
            "status": "OK"
        }

class EditarSalaProva(ServicoBase):
    description = "Usado por um administrador para editar uma sala de prova."
    requires = {
        "url": [],
        "post": ["user", "senha", "mapa", "id_mapa"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        mapa = json.loads(args["post"]["mapa"])
        id_mapa = args["post"]["id_mapa"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.editSalaProva(id_mapa, mapa);

        return {
            "status": "OK"
        }

class RemoverSalaProva(ServicoBase):
    description = "Usado por um administrador para editar uma sala de prova."
    requires = {
        "url": [],
        "post": ["user", "senha", "id_mapa"]
    }
    def post(args):
        user = args["post"]["user"]
        senha = args["post"]["senha"]
        id_mapa = args["post"]["id_mapa"]

        if not db.isAdmin(user, senha):
            return {
                "status": "FAILED",
                "message": "Faça login como admin!"
            }

        db.removeSalaProva(id_mapa);

        return {
            "status": "OK"
        }

_servicos = {
    "alive": Alive,
    "help": GetAPIDescription,
    "licoes_ativas_sala": GetLicoesSala,
    "salas": GetSalas,
    "token_check": VerifyToken,
    "login_aluno": VerifyAluno,
    "register": RegisterAluno,
    "aulas": GetAulas,
    "adicionar_licao": AdicionarLicao,
    "get_aluno_basic": GetAlunoBasic,
    "editar_licao": EditarLicao,
    "apagar_licao": ApagarLicao,
    "reportar_licao": ReportarLicao,
    "esqueci_senha": EsqueciMinhaSenha,
    "recover_info": GetRecoverInfo,
    "mudar_senha": MudarSenha,
    "login_adm": LoginAdm,
    "iniciar_votacao": IniciarVotacao,
    "posso_enviar_calendario": PossoEnviarCalendario,
    "toggle_banido": ToggleBanido,
    "votacoes_basic": GetVotacoesBasic,
    "votacao_full": GetVotacaoFull,
    "materias": GetMaterias,
    "enviar_calendario": EnviarCalendario,
    "meu_voto": GetMeuVoto,
    "votar_em": VotarEm,
    "get_all_materias": GetAllMaterias,
    "add_professor": AddProfessor,
    "edit_professor": EditProfessor,
    "remove_professor": RemoverProfessor,
    "get_professores": GetProfessores,
    "check_professor": CheckProfessor,
    "criar_aviso": CriarAviso,
    "contagem_resumos": ContagemResumos,
    "pesquisar_resumos": PesquisarResumos,
    "get_resumos_old_way": GetResumosOldWay,
    "get_resumos_epa": GetResumosByEPA,
    "resumo_full": GetResumoFull,
    "enviar_resumo": EnviarResumo,
    "posso_editar": PossoEditar,
    "editar_resumo": EditarResumo,
    "toggle_remover_resumo": ToggleRemoverResumo,
    "meus_resumos": MeusResumos,
    "get_reforcos": GetReforcos,
    "add_reforco": AddReforco,
    "edit_reforco": EditReforco,
    "remove_reforco": RemoveReforco,
    "get_horario": GetHorario,
    "set_horario": SetHorario,
    "get_feitas": GetFeitas,
    "set_feitas": SetFeitas,
    "toggle_feita": ToggleFeita,
    "get_meus_avisos": GetMeusAvisos,
    "editar_aviso": EditarAviso,
    "deletar_aviso": DeletarAviso,
    "get_avisos_ano": GetAvisosByAno,
    "get_arquivo": GetArquivo,
    "get_msg_sala": GetMsgSala,
    "set_msg_sala": SetMsgSala,
    "pesquisar_licoes": PesquisarLicoes,
    "get_cursos": GetCursos,
    "licoes_cursos": GetLicoesCursos,
    "enviar_licao_curso": EnviarLicaoCurso,
    "editar_licao_curso": EditarLicaoCurso,
    "remover_licao_curso": RemoverLicaoCurso,
    "get_materias_prova": GetMateriasProva,
    "set_materia_prova": SetMateriaProva,
    "get_alunos_lista": GetAlunosLista,
    "get_salas_prova_ep": GetSalasProvaEP,
    "get_minha_sala_prova": GetMinhaSalaProva,
    "get_full_sala_prova": GetFullSalaProva,
    "todas_salas_prova": GetTodasSalasProva,
    "motd": GetCurrentMOTD,
    "send_motd": EnviarMOTD,
    "get_seminarios": GetSeminarios,
    "criar_seminario": CriarSeminario,
    "remover_seminario": RemoverSeminario,
    "get_iniciais": GetIniciais,
    "set_inicial": SetarInicial,
    "verificar_aluno_aprov": VerificarAlunoAprov,
    "enviar_correcao_nome": EnviarCorrecaoNome,
    "ano_diretor": GetAnoDiretor,
    "add_aprovacao": AdicionarAprovacao,
    "edit_aprovacao": EditarAprovacao,
    "remove_aprovacao": RemoverAprovacao,
    "todas_aprovacoes_aluno": GetTodasAprovacoesPorAluno,
    "relatorio_aprovacoes": GetRelatorioAprovacoes,
    "get_error_logs": GetErrorLogs,
    "resetar_aluno": ResetarAluno,
    "query_alunos": QueryAlunos,
    "manuais": GetManuais,
    "ajudas": GetAjudas,
    "crash": HandleCrash,
    "regexes": GetRegexes,
    "add_regex": AddRegex,
    "edit_regex": EditRegex,
    "remove_regex": RemoveRegex,
    "extrair_nomes": ExtrairNomes,
    "add_mapa": AdicionarSalaProva,
    "edit_mapa": EditarSalaProva,
    "remove_mapa": RemoverSalaProva
}

def todos():
    return _servicos

def existe(nome):
    return nome in _servicos

def getServico(nome):
    return _servicos[nome]
