DELETE FROM mendelyx.materias;
ALTER TABLE mendelyx.materias AUTO_INCREMENT=1;

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (1, "Matemática", "EXATAS", 0, "1,2,3", 0);           /* 01 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (2, "Química", "EXATAS", 0, "1,2,3", 0);              /* 02 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (3, "Física", "EXATAS", 0, "1,2,3", 0);               /* 03 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (4, "História", "HUMANAS", 0, "1,2,3", 0);            /* 04 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (5, "Geografia", "HUMANAS", 0, "1,2,3", 0);           /* 05 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (6, "Língua Portuguesa", "HUMANAS", 0, "1,2,3", 0);   /* 06 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (7, "Inglês", "INGLES", 0, "1,2,3", 0);              /* 07 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (8, "Filosofia", "HUMANAS", 1, "1,2,3", 0);           /* 08 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (9, "Sociologia", "HUMANAS", 1, "1,2,3", 0);          /* 09 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (10, "Artes", "HUMANAS", 1, "1", 1);                   /* 10 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (11, "Biologia", "BIOLOGICAS", 0, "1,2,3", 0);         /* 11 */

INSERT INTO mendelyx.materias (id_materia, nome, tipo, curta, anos, artes)
    VALUES (12, "Redação", "HUMANAS", 0, "", 0);                  /* 12 */