

CREATE TABLE `admins` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `email` mediumtext,
  `username` mediumtext,
  `opaque` varchar(128) DEFAULT NULL,
  `salt` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `aluno_tokens` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(8) DEFAULT NULL,
  `token` varchar(128) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  PRIMARY KEY (`id_token`)
) AUTO_INCREMENT=1027 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `alunos` (
  `id_aluno` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(9) DEFAULT NULL,
  `nome` mediumtext,
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  `banido` tinyint(4) DEFAULT '0',
  `autor` tinyint(4) DEFAULT '0',
  `opaque` char(128) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `email` mediumtext,
  `chamada` int(11) DEFAULT NULL,
  `em` int(11) DEFAULT NULL,
  `correcao_nome` mediumtext,
  PRIMARY KEY (`id_aluno`)
) AUTO_INCREMENT=1745 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `aprovacoes` (
  `id_aprovacao` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `nome_uni` mediumtext,
  `nome_curso` mediumtext,
  `publica` tinyint(4) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `treineiro` tinyint(4) DEFAULT NULL,
  `meio_de_ano` tinyint(4) DEFAULT NULL,
  `sisu` tinyint(4) DEFAULT NULL,
  `categoria` mediumtext,
  `ano_diretor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_aprovacao`)
) AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `aulas` (
  `id_aula` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `nome` mediumtext,
  `anos` set('1','2','3') DEFAULT NULL,
  PRIMARY KEY (`id_aula`)
) AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `avisos` (
  `id_aviso` int(11) NOT NULL AUTO_INCREMENT,
  `id_professor` int(11) DEFAULT NULL,
  `titulo` mediumtext,
  `texto` mediumtext,
  `id_arquivo` int(11) DEFAULT NULL,
  `ano` enum('0','1','2','3') DEFAULT NULL,
  PRIMARY KEY (`id_aviso`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `calendarios` (
  `id_calendario` int(11) NOT NULL AUTO_INCREMENT,
  `id_autor` int(11) DEFAULT NULL,
  `calendario` mediumtext,
  `id_votacao` int(11) DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  `neo_ano` enum('0','1','2','3') DEFAULT '0',
  PRIMARY KEY (`id_calendario`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `crash_reports` (
  `crash_id` int(11) NOT NULL AUTO_INCREMENT,
  `crash_report` mediumtext,
  `received` datetime DEFAULT NULL,
  `emailed` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`crash_id`)
) AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `cursos` (
  `id_curso` int(11) NOT NULL AUTO_INCREMENT,
  `nome` mediumtext,
  PRIMARY KEY (`id_curso`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `esqueci_links` (
  `id_link` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `url_token` varchar(128) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `used` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_link`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `feitas` (
  `id_feita` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `feitas` mediumtext,
  PRIMARY KEY (`id_feita`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `horarios` (
  `id_horario` int(11) NOT NULL AUTO_INCREMENT,
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  `horario` mediumtext,
  `ultimo_editor` int(11) DEFAULT NULL,
  `lastedit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_horario`)
) AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `iniciais` (
  `id_link` int(11) NOT NULL AUTO_INCREMENT,
  `link` mediumtext,
  `visivel` tinyint(4) DEFAULT '0',
  `nome` mediumtext,
  PRIMARY KEY (`id_link`)
) AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `licoes` (
  `id_licao` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `passada` date DEFAULT NULL,
  `para` date DEFAULT NULL,
  `id_aula` int(11) DEFAULT NULL,
  `info` longtext,
  `removida` tinyint(4) DEFAULT '0',
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_licao`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `licoes_cursos` (
  `id_licao` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_curso` int(11) DEFAULT NULL,
  `passada` date DEFAULT NULL,
  `para` date DEFAULT NULL,
  `info` mediumtext,
  `removida` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_licao`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `materias` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `nome` mediumtext,
  `tipo` enum('EXATAS','HUMANAS','BIOLOGICAS','INGLES') DEFAULT NULL,
  `curta` tinyint(4) DEFAULT NULL,
  `anos` set('1','2','3') DEFAULT NULL,
  `artes` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_materia`)
) AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `materias_prova` (
  `id_materia_prova` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `etapa` enum('1','2') DEFAULT NULL,
  `periodo` enum('1','2','3','4') DEFAULT NULL,
  `info` mediumtext,
  `ultimo_editor` int(11) DEFAULT NULL,
  `ano` enum('1','2','3') DEFAULT NULL,
  PRIMARY KEY (`id_materia_prova`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `motd` (
  `id_motd` int(11) NOT NULL AUTO_INCREMENT,
  `set_by` mediumtext,
  `texto` mediumtext,
  `enviado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id_motd`)
) AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `professores` (
  `id_professor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` mediumtext,
  `salt` char(16) DEFAULT NULL,
  `opaque` char(128) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_professor`)
) AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `provas` (
  `id_matprova` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `ano` enum('1','2','3') DEFAULT NULL,
  `info` longtext,
  PRIMARY KEY (`id_matprova`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `reforcos` (
  `id_reforco` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` mediumtext,
  `hora_fim` mediumtext,
  `sala` mediumtext,
  `ano` enum('1','2','3') DEFAULT NULL,
  `nomemateria` mediumtext,
  `professor` mediumtext,
  `dias_array` mediumtext,
  PRIMARY KEY (`id_reforco`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `regexes` (
  `id_regex` int(11) NOT NULL AUTO_INCREMENT,
  `nome` mediumtext,
  `detalhes` mediumtext,
  `pattern` mediumtext,
  PRIMARY KEY (`id_regex`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `reports_licoes` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_licao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_report`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `resumos` (
  `id_resumo` int(11) NOT NULL AUTO_INCREMENT,
  `nome_autor` mediumtext,
  `id_materia` int(11) DEFAULT NULL,
  `texto` longtext,
  `miniurl` mediumtext,
  `ano` enum('1','2','3') DEFAULT NULL,
  `removido` tinyint(4) DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  `periodo` enum('0','1','2','3','4') DEFAULT NULL,
  `etapa` enum('0','1','2') DEFAULT NULL,
  `assunto` mediumtext,
  `id_autor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_resumo`)
) AUTO_INCREMENT=109 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `sala_msgs` (
  `id_msg` int(11) NOT NULL AUTO_INCREMENT,
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` char(1) DEFAULT NULL,
  `texto` mediumtext,
  `ultimo_editor` int(11) DEFAULT NULL,
  `lastedit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_msg`)
) AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `salas_prova` (
  `id_sala_prova` int(11) NOT NULL AUTO_INCREMENT,
  `periodo` enum('1','2','3','4') DEFAULT NULL,
  `numsala` int(11) DEFAULT NULL,
  `fileiras` mediumtext,
  `posicao_porta` enum('se','sd','ie','id') DEFAULT NULL,
  `lousa_acima` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_sala_prova`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `seminarios` (
  `id_seminario` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) NOT NULL,
  `tema` mediumtext,
  `id_arquivo` int(11) DEFAULT NULL,
  `link` mediumtext,
  `removido` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_seminario`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `uploads` (
  `id_arquivo` int(11) NOT NULL AUTO_INCREMENT,
  `filename` mediumtext,
  `hash` varchar(128) DEFAULT NULL,
  `id_enviou` int(11) DEFAULT NULL,
  `aluno` tinyint(4) DEFAULT '0',
  `removido` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_arquivo`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `votacoes` (
  `id_votacao` int(11) NOT NULL AUTO_INCREMENT,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `periodo` enum('1','2','3','4') DEFAULT NULL,
  `etapa` enum('1','2') DEFAULT NULL,
  `dias` mediumtext,
  `neo` tinyint(4) DEFAULT '0',
  `responsavel_neo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_votacao`)
) DEFAULT CHARSET=utf8mb4;



CREATE TABLE `votos` (
  `id_voto` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_calendario` int(11) DEFAULT NULL,
  `id_votacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_voto`)
) AUTO_INCREMENT=1004 DEFAULT CHARSET=utf8mb4;



CREATE TABLE `votos_resumos` (
  `id_voto_resumo` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_resumo` int(11) DEFAULT NULL,
  `is_downvote` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_voto_resumo`)
) DEFAULT CHARSET=utf8mb4;

