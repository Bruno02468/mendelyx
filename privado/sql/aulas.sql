DELETE FROM mendelyx.aulas;
ALTER TABLE mendelyx.aulas AUTO_INCREMENT=1;

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (1, "Matemática 1", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (1, "Matemática 2", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (1, "Matemática 3", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (1, "Matemática 4", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Química 1", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Química 2", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Química 3", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Físico-Química", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Química Geral", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (2, "Química Orgânica", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (3, "Mecânica", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (3, "Óptica", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (3, "Termologia", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (3, "Elétrica", "2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (3, "Ondulatória", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (4, "História Geral", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (4, "História do Brasil", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (5, "Geografia Geral", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (5, "Geografia Física", "1,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (5, "Geografia Humana", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (5, "Geografia do Brasil", "2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (5, "Atualidades", "2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (6, "Literatura", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (6, "Gramática", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (12, "Redação", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (7, "Inglês Gramática", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (7, "Inglês Oral", "1");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (7, "Inglês Texto", "2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (8, "Filosofia", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (9, "Sociologia", "1,2,3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (10, "Artes", "1");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Citologia", "1");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Genética", "2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Zoologia", "1");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Botânica", "2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Ecologia", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Citogenética", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (11, "Fisiologia", "3");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (NULL, "P.O.P.", "2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (NULL, "E.F.", "1,2");

INSERT INTO mendelyx.aulas (id_materia, nome, anos)
    VALUES (NULL, "Religião", "1,2");