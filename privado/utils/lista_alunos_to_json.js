var db = {};
var salaRegex = /(\d)ª Série (.)/;
var alunoRegex = /(\d+)\n(\d{7}-\d)\n(.+)/;
for (var ano = 1; ano <= 3; ano++) {
    var rows = document.getElementsByClassName("ta" + ano)[0].children[1].children;
    var sala = null;
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var salaMatch = row.innerText.match(salaRegex);
        if (salaMatch) {
            ano = salaMatch[1];
            sala = salaMatch[2];
            //console.log("ano " + ano + " e sala " + sala);
            continue;
        }
        var alunoMatch = row.innerText.match(alunoRegex);
        if (alunoMatch) {
            var aluno = {
                nome: alunoMatch[3],
                ano: ano,
                sala: sala,
                chamada: alunoMatch[1],
            };
            var matricula = alunoMatch[2].replace(/-/g, "").replace(/^00/, "");
            //console.log("pushing aluno", aluno);
            db[matricula] = aluno;
        }
    }
}
document.write(JSON.stringify(db));