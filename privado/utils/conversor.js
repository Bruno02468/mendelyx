var banco;
var in_json = document.getElementById("in_json");

function make_td(inner) {
    return "<td>" + inner + "</td>";
}

var select_etapa = "<td><select><option value=\"0\">Todas etapas</option>"
    + "<option value=\"1\">1ª Etapa</option>"
    + "<option value=\"2\">2ª Etapa</option>"
    + "</select></td>";

var select_periodo = "<td><select><option value=\"0\">Todos períodos</option>"
    + "<option value=\"1\">1º Período</option>"
    + "<option value=\"2\">2º Período</option>"
    + "<option value=\"3\">3º Período</option>"
    + "<option value=\"4\">4º Período</option>"
    + "</select></td>";

var materias = [
    "Matemática",
    "Química",
    "Física",
    "História",
    "Geografia",
    "Língua Portuguesa",
    "Inglês",
    "Filosofia",
    "Sociologia",
    "Artes",
    "Biologia",
    "Redação"
];
var select_materia = "<td><select>";
for (var index in materias) {
    select_materia += "<option value=\"" + (index+1) + "\">" + materias[index]
        + "</option>";
}
select_materia += "</select></td>";

var lista = document.getElementById("lista");
function mklista() {
    banco = JSON.parse(in_json.value);
    in_json.value = "";
    var porautor = {};
    var autores = [];
    var novalista = "";
    for (var key in banco) {
        if (!banco.hasOwnProperty(key)) continue;
        var autor = banco[key]["autor"].split(" ").slice(0, 2).join(" ");
        if (autores.indexOf(autor) < 0) {
            autores.push(autor);
            autores.sort();
            porautor[autor] = [];
        }
        porautor[autor].push(banco[key]);
    }
    for (var key in autores) {
        var autor = autores[key];
        var td_nome = make_td(autor);
        for (var index in porautor[autor]) {
            var resumo = porautor[autor][index];
            var td_ano = make_td(resumo["ano"] + "º ano");
            var td_info = make_td(resumo["materia"].trim() + ": "
                + resumo["assunto"].trim());
            
            novalista += "<tr>" + td_nome + td_ano + td_info + select_periodo
                + select_etapa + select_materia + "</tr>";
        }
    }
    lista.innerHTML = novalista;
}


var out_sql = document.getElementById("out_sql");
var gotem = {};
function mksql() {
    gotem = {};
    var sql = "";
    var linhas = lista.children[0].children;
    for (var index = 0; index < linhas.length; index++) {
        var linha = linhas[index];
        var ano = linha.children[1].innerText[0];
        var info = linha.children[2].innerText.trim();
        var periodo = linha.children[3].children[0].value;
        var etapa = linha.children[4].children[0].value;
        var id_materia = linha.children[5].children[0].value;
        var key = ano + "§" + info;
        var val = {
            "periodo": periodo,
            "etapa": etapa,
            "id_materia": id_materia
        }
        gotem[key] = val;
    }
    for (var key in banco) {
        if (!banco.hasOwnProperty(key)) continue;
        var resumo = banco[key];
        var ano = resumo["ano"].trim();
        var materia = resumo["materia"].trim();
        var assunto = resumo["assunto"].trim();
        var nome_autor = resumo["autor"].trim();
        var texto = JSON.stringify(resumo["conteudo"]);
        var miniurl = resumo["mini"].trim();
        var ikey = ano + "§" + materia + ": " + assunto;
        var periodo = gotem[ikey]["periodo"];
        var etapa = gotem[ikey]["etapa"];
        var id_materia = gotem[ikey]["id_materia"];

        sql += "INSERT INTO resumos (nome_autor, id_materia, miniurl, ano,"
        + " removido, enviado_em, periodo, etapa, id_autor, texto) VALUES"
        + "(\"" + nome_autor + "\", \"" + id_materia + "\", "
        + "\"" + miniurl + "\", \"" + ano + "\", FALSE, NOW(),"
        + " \"" + periodo + "\", \"" + etapa + "\", NULL, " + texto + ");\n"
    }
    out_sql.value = sql;
    console.log(sql);
}
