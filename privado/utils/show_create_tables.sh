#!/bin/sh

mysqldump -h localhost -u mendelyx -p --no-data --compact mendelyx \
	| sed "s/\/\*.\+\*\/;//g" \
	| sed "s/AUTO_INCREMENT=./AUTO_INCREMENT=1/g" \
	| sed "s/ENGINE=InnoDB //g";
