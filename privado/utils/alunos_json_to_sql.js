function to_query(alunos) {
    var query = "";
    for (key in alunos) {
        if (!alunos.hasOwnProperty(key)) continue;
        var aluno = alunos[key];
        var matricula = "00" + key.slice(0, -1) + "-" + key[key.length - 1];
        query += "INSERT INTO alunos (matricula, nome, ano, sala, chamada) VALUES ("
            + "\"" + matricula + "\","
            + "\"" + aluno["nome"] + "\","
            + "\"" + aluno["ano"] + "\","
            + "\"" + aluno["sala"] + "\","
            + "\"" + aluno["chamada"] + "\""
            + ");\n<br>";
    }
    return query;
}

