function nomeconv(str) {
    var pieces = str.split(" ");
    for (var i = 0; i < pieces.length; i++) {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}
var hj = new Date().getFullYear();
var sql = "";
var salaRegex = /(\d)ª série (.)/g;
var alunoRegex = /(\d{8})\n\n(\d+)\n\n(.+)/;
var rows = document.getElementsByClassName("ta1")[0].children[1].children;
var sala = null;
for (var i = 0; i < rows.length; i++) {
    var row = rows[i];
    var salaMatch = row.innerText.match(salaRegex);
    if (salaMatch) {
        var salastr = salaMatch[0];
        ano = salastr[0];
        sala = salastr[salastr.length-1];
        console.log("achei sala:", ano + sala)
        continue;
    }
    console.log(row.innerText);
    var alunoMatch = alunoRegex.exec(row.innerText);
    if (alunoMatch) {
        var nome = nomeconv(alunoMatch[3]);
        var chamada = alunoMatch[2];
        var matricula = alunoMatch[1];
        console.log("pushing aluno", nome);
        sql += "INSERT INTO alunos (nome, ano, sala, chamada, matricula, em)"
            + " VALUES ('" + nome + "', " + ano + ", '" + sala + "', "
            + chamada + ", '" + matricula + "', " + hj + ");\n<br>";
    }
}
document.write(sql);