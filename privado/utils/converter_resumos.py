#!/bin/python

import sys
import json
from random import randint

if (len(sys.argv) < 3):
    print("Especifique o JSON e o arquivo de saída.")
    sys.exit(22)

fname = sys.argv[1]
outname = sys.argv[2]

try:
    arquivo = open(fname, "r")
    outfile = open(outname, "w")
except FileNotFoundError as fnferr:
    print("Erro: esse arquivo não existe.")
    sys.exit(fnferr.errno)
except IsADirectoryError as direrr:
    print("Erro: isso é uma pasta.")
    sys.exit(direrr.errno)
except PermissionError as permerr:
    print("Erro: permissões insuficientes para ler o arquivo.")
    sys.exit(permerr.errno)
except IOError as ioerr:
    print("Erro: falha de I/O ao ler o arquivo.")
    sys.exit(5)
except OSError as oserr:
    print("Erro:", oserr.strerror)
    sys.exit(oserr.errno)
except BaseException as bex:
    print("Um erro não-especificado foi lançado.")
    sys.exit(1)

banco = json.load(arquivo)
arquivo.close()

materias = [
    "Matemática",
    "Química",
    "Física",
    "História",
    "Geografia",
    "Língua Portuguesa",
    "Inglês",
    "Filosofia",
    "Sociologia",
    "Artes",
    "Biologia",
    "Redação"
]

def makeInsert(json_entry):
    ano = json_entry["ano"]
    nome_materia = json_entry["materia"].strip()
    assunto = json_entry["assunto"].strip()
    nome_autor = json_entry["autor"].strip()
    texto = json.dumps(json_entry["conteudo"])
    miniurl = json_entry["mini"].strip()

    id_materia = 1
    for materia in materias:
        if materia in nome_materia:
            break
        id_materia += 1

    periodo = input("    => Período do resumo \"" + assunto + "\": ")
    etapa = input("    => Etapa do resumo \"" + assunto + "\": ")

    if len(periodo) == 0:
        periodo = ""
    if len(etapa) == 0:
        etapa = ""
    
    return ("INSERT INTO resumos (nome_autor, id_materia, texto, miniurl, ano,"
        " removido, enviado_em, periodo, etapa, id_autor) VALUES"
        "(\"" + nome_autor + "\", \"" + str(id_materia) + "\", " + texto + ", "
        "\"" + miniurl + "\", \"" + str(ano) + "\", 0, NOW(),"
        " \"" + str(periodo) + "\", \"" + str(etapa) + "\", NULL);\n")


porautor = {}
for index, entry in banco.items():
    autor = entry["autor"].strip()
    if autor not in porautor:
        porautor[autor] = []
    porautor[autor].append(entry)


sql = ""
for autor, lista in porautor.items():
    print("\n==> Agora os resumos de", autor + ":")
    for resumo in lista:
        sql += makeInsert(resumo)
        print("")

outfile.write("/* ===== BEGIN SQL SCRIPT ===== */\n")
outfile.write(sql)
outfile.write("/* =====  END SQL SCRIPT  ===== */\n")

print("O SQL foi salvo em" , outname + "!")