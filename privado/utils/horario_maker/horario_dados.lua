-- este arquivo contém todos os dados necessários à geração do horário
-- usado por horario_maker.lua

-- AULAS
-- semana: lista de quantas aulas tem por semana em cada ano
-- dobradinha: "nem" proíbe, "meh" dá baixa prioridade, "requer" exige,
-- "strict" exige dobradinhas que não sejam cortadas por intervalos
aulas = {}

-- matemática
aulas.mat1 = {
	["semana"] = {2, 2, 2},
	["dobradinha"] = "requer"
}
aulas.mat2 = aulas.mat1
aulas.mat3 = {
	["semana"] = {1, 1, 2},
	["dobradinha"] = "requer"
}

-- física
aulas.fis1 = {
	["semana"] = {3, 3, 3},
	["dobradinha"] = "pode"
}
aulas.fis2 = {
	["semana"] = {2, 2, 2},
	["dobradinha"] = "requer"
}
aulas.fis3 = {
	["semana"] = {nil, nil, 2},
	["dobradinha"] = "meh"
}

-- química
aulas.quim1 = {
	["semana"] = {2, 2, 2},
	["dobradinha"] = "pode"
}
aulas.quim2 = aulas.quim1
aulas.quim3 = {
	["semana"] = {1, 1, 2},
	["dobradinha"] = "requer"
}

-- biologia
aulas.cito = {
	["semana"] = {2, nil, nil},
	["dobradinha"] = "meh"
}
aulas.zoo = aulas.cito
aulas.gen = {
	["semana"] = {nil, 2, nil},
	["dobradinha"] = "meh"
}
aulas.bot = aulas.gen
aulas.citogen = {
	["semana"] = {nil, nil, 2},
	["dobradinha"] = "meh"
}
aulas.fisio = aulas.citogen
aulas.eco = aulas.citogen

-- geografia
aulas.geofis = {
	["semana"] = {1, nil, 1},
	["dobradinha"] = "nem"
}
aulas.geogeral = {
	["semana"] = {2, 1, nil},
	["dobradinha"] = "nem"
}
aulas.geobr = {
	["semana"] = {nil, 2, nil},
	["dobradinha"] = "nem"
}
aulas.geohum = {
	["semana"] = {nil, nil, 2},
	["dobradinha"] = "nem"
}

-- história
aulas.hisgeral = {
	["semana"] = {2, 2, 2},
	["dobradinha"] = "nem"
}
aulas.hisbr = {
	["semana"] = {1, 1, 1},
	["dobradinha"] = "nem"
}

-- português
aulas.gram = {
	["semana"] = {1, 2, 1},
	["dobradinha"] = "nem"
}
aulas.lit = {
	["semana"] = {2, 2, 3},
	["dobradinha"] = "pode"
}

-- redação
aulas.red = {
	["semana"] = {2, 2, 2},
	["dobradinha"] = "strict"
}

-- inglês
aulas.ing = {
	["semana"] = {2, nil, nil},
	["dobradinha"] = "requer"
}
aulas.inggram = {
	["semana"] = {nil, 1, 1},
	["dobradinha"] = "nem"
}
aulas.ingtexto = aulas.inggram

-- filosofia
aulas.filo = {
	["semana"] = {1, 1, 1},
	["dobradinha"] = "nem"
}

-- sociologia
aulas.socio = aulas.filo

-- educação física
aulas.ef = {
	["semana"] = {1, 1, nil},
	["dobradinha"] = "nem"
}
