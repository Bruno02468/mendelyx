

CREATE TABLE `admins` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `email` text,
  `username` text,
  `opaque` varchar(128) DEFAULT NULL,
  `salt` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `aluno_tokens` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `moodle` varchar(6) DEFAULT NULL,
  `token` varchar(128) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;



CREATE TABLE `alunos` (
  `id_aluno` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(9) DEFAULT NULL,
  `nome` text,
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  `banido` tinyint(4) DEFAULT '0',
  `autor` tinyint(4) DEFAULT '0',
  `opaque` char(128) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `email` text,
  `chamada` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_aluno`)
) ENGINE=InnoDB AUTO_INCREMENT=886 DEFAULT CHARSET=utf8;



CREATE TABLE `aulas` (
  `id_aula` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `nome` text,
  `anos` set('1','2','3') DEFAULT NULL,
  PRIMARY KEY (`id_aula`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;



CREATE TABLE `avisos` (
  `id_aviso` int(11) NOT NULL AUTO_INCREMENT,
  `id_professor` int(11) DEFAULT NULL,
  `title` text,
  `contents` text,
  `files` text,
  PRIMARY KEY (`id_aviso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `calendarios` (
  `id_calendario` int(11) NOT NULL AUTO_INCREMENT,
  `id_autor` int(11) DEFAULT NULL,
  `calendario` text,
  `id_votacao` int(11) DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  PRIMARY KEY (`id_calendario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;



CREATE TABLE `esqueci_links` (
  `id_link` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `url_token` varchar(128) DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `used` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id_link`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;



CREATE TABLE `horarios` (
  `id_horario` int(11) NOT NULL AUTO_INCREMENT,
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  `horario` text,
  `ultimo_editor` int(11) DEFAULT NULL,
  `lastedit` datetime DEFAULT NULL,
  PRIMARY KEY (`id_horario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `licoes` (
  `id_licao` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `passada` date DEFAULT NULL,
  `para` date DEFAULT NULL,
  `id_aula` int(11) DEFAULT NULL,
  `info` longtext,
  `removida` tinyint(4) DEFAULT '0',
  `ano` enum('1','2','3') DEFAULT NULL,
  `sala` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_licao`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;



CREATE TABLE `materias` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `tipo` enum('EXATAS','HUMANAS','BIOLOGICAS','INGLES') DEFAULT NULL,
  `curta` tinyint(4) DEFAULT NULL,
  `anos` set('1','2','3') DEFAULT NULL,
  `artes` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;



CREATE TABLE `professores` (
  `id_professor` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text,
  `salt` char(16) DEFAULT NULL,
  `opaque` char(128) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_professor`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;



CREATE TABLE `provas` (
  `id_matprova` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `ano` enum('1','2','3') DEFAULT NULL,
  `info` longtext,
  PRIMARY KEY (`id_matprova`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `reforcos` (
  `id_reforco` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` text,
  `hora_fim` text,
  `sala` text,
  `ano` enum('1','2','3') DEFAULT NULL,
  `nomemateria` text,
  `professor` text,
  `dias_array` text,
  PRIMARY KEY (`id_reforco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `reports_licoes` (
  `id_report` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_licao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_report`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



CREATE TABLE `resumos` (
  `id_resumo` int(11) NOT NULL AUTO_INCREMENT,
  `nome_autor` text,
  `id_materia` int(11) DEFAULT NULL,
  `texto` longtext,
  `miniurl` text,
  `ano` enum('1','2','3') DEFAULT NULL,
  `removido` tinyint(4) DEFAULT NULL,
  `enviado_em` datetime DEFAULT NULL,
  `periodo` enum('0','1','2','3','4') DEFAULT NULL,
  `etapa` enum('0','1','2') DEFAULT NULL,
  `assunto` text,
  `id_autor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_resumo`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;



CREATE TABLE `uploads` (
  `id_arquivo` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text,
  `hash` varchar(128) DEFAULT NULL,
  `id_professor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_arquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `votacoes` (
  `id_votacao` int(11) NOT NULL AUTO_INCREMENT,
  `inicio` date DEFAULT NULL,
  `fim` date DEFAULT NULL,
  `periodo` enum('1','2','3','4') DEFAULT NULL,
  `etapa` enum('1','2') DEFAULT NULL,
  `prova1` date DEFAULT NULL,
  `prova2` date DEFAULT NULL,
  `prova3` date DEFAULT NULL,
  `prova4` date DEFAULT NULL,
  PRIMARY KEY (`id_votacao`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `votos` (
  `id_voto` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_calendario` int(11) DEFAULT NULL,
  `id_votacao` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_voto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



CREATE TABLE `votos_resumos` (
  `id_voto_resumo` int(11) NOT NULL AUTO_INCREMENT,
  `id_aluno` int(11) DEFAULT NULL,
  `id_resumo` int(11) DEFAULT NULL,
  `is_downvote` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_voto_resumo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

