var salareg = /Sala (\d+)/i;
var seriereg = /(\d)ª série/i;
var alunoreg = /(\d+) (.)/;
var vazio = "VAZIO";

function analisar(elem) {
    var text = elem.innerText.trim();
    return {
        "text": text,
        "sala": salareg.exec(text),
        "serie": seriereg.exec(text),
        "aluno": (text.length > 5) ? null : alunoreg.exec(text),
        "vazio": (text == vazio)
    };
}

var periodo = prompt("periodo?");
var csalas = [];
var banco = {};
var visalas = false;
var portas = {
    "301": "sd", "302": "sd", "303": "sd", "307": "ie", "308": "ie",
    "309": "ie", "310": "ie", "311": "ie", "312": "se", "201": "sd",
    "202": "sd", "203": "sd", "207": "ie", "208": "ie", "209": "ie",
    "210": "ie", "211": "ie", "212": "se", "107": "ie", "109": "ie"
};
var fileira = 0;
var elems = document.getElementsByTagName("td");
for (var index = 0; index < elems.length; index++) {
    var elem = elems[index];
    var d = analisar(elem);
    if (!d) continue;
    if (d["sala"] !== null) {
        var sala = d["sala"][1];
        var n = analisar(elems[index+1]);
        var p = analisar(elems[index-1]);
        if (p) {
            if (p["sala"] !== null) {
                csalas = [p["sala"][1], sala];
            }
        }
        if (n && p) {
            if (n["sala"] == null && p["sala"] == null) {
                csalas = [sala];
            }
        }
        //console.log(sala, csalas);
        banco[sala] = {
            "posicao_porta": portas[sala],
            "lousa_acima": (sala[0] !== "1"),
            "fileiras": []
        };
        for (var i = 0; i < 6; i++) {
            banco[sala]["fileiras"].push({
                "ano": 0,
                "lugares": []
            });
        }
    } else if (d["serie"] !== null || d["aluno"] !== null || d["vazio"]) {
        if (d["serie"] !== null) {
            if (!banco[csalas[fileira < 6 ? 0 : 1]]["fileiras"][fileira % 6]["ano"]) {
                console.log("estou na sala", csalas[fileira < 6 ? 0 : 1], "olhando",
                "o ano da fileira absoluta", fileira, "que é", d["serie"][1]);
                banco[csalas[fileira < 6 ? 0 : 1]]["fileiras"][fileira % 6]["ano"]
                    = parseInt(d["serie"][1]);
            }
        } else if (d["aluno"] !== null) {
            //console.log("estou na fileira absoluta", fileira, "adicionando",
            //    d["aluno"][0], "à sala", csalas[fileira < 6 ? 0 : 1]);
            //console.log(csalas[fileira < 6 ? 0 : 1]);
            banco[csalas[fileira < 6 ? 0 : 1]]["fileiras"][fileira % 6]["lugares"].push({
                "chamada": parseInt(d["aluno"][1]),
                "sala": d["aluno"][2]
            });
        } else if (d["vazio"]) {
            //console.log("aluno vazio na sala", csalas[fileira < 6 ? 0 : 1]
            //    + ", fileira absoluta", fileira);
            banco[csalas[fileira < 6 ? 0 : 1]]["fileiras"][fileira % 6]["lugares"].push(null);
        }
        fileira = (fileira + 1) % (csalas.length == 1 ? 6 : 12);
    }
}

var sql = "";
for (var sala in banco) {
    if (!banco.hasOwnProperty(sala)) continue;
    sql += "INSERT INTO mendelyx.salas_prova (periodo, numsala, fileiras, "
        + "posicao_porta, lousa_acima) VALUES (" + periodo + ", "
        + sala + ", '" + JSON.stringify(banco[sala]["fileiras"]) + "', '"
        + banco[sala]["posicao_porta"] + "', " + banco[sala]["lousa_acima"] + ");<br>";
}
//document.write(sql);
