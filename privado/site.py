#!/usr/bin/env python3
# programado por bruno borges paschoalinoto ou borginhos ou bruno02468

import sys
sys.dont_write_bytecode = 1

import servicos
import tornado
import tornado.web
import tornado.ioloop
import signal
from urllib.parse import unquote

def sigint_handler(signal, frame):
    print("\nSIGINT capturado, saindo com cuidado!")
    sys.exit(0)
signal.signal(signal.SIGINT, sigint_handler)

def debug_log(message):
    if "-d" in sys.argv or "--debug" in sys.argv:
        print(message)

port = 9994

class PaginaBase(tornado.web.RequestHandler):
    def get(self):
        self.write("Esta é a página inicial do serviço!")

class Status(tornado.web.RequestHandler):
    def get(self):
        self.write("Status dos serviços: tudo OK." +
            "<br>Escrito por Bruno Borges Paschoalinoto.")

class Getters(tornado.web.RequestHandler):
    def get(self, nomeservico, argstring):
        args = {
            "url": argstring.split(","),
            "post": {},
        }
        # DEBUG
        debug_log("GET: " + nomeservico + " " + str(args))
        if (len(argstring) == 0):
            args["url"] = []
        if not servicos.existe(nomeservico):
            self.write("Esse serviço não existe!")
        else:
            servico = servicos.getServico(nomeservico)
            self.set_header("Content-Type", servico.contentType)
            check = servicos.checkArgs(nomeservico, args)
            if check == True:
                self.write(servico.get(args))
            else:
                self.write(check)

class Posters(tornado.web.RequestHandler):
    def get(self, name):
        self.write("Este serviço é para requests POST!")

    def post(self, name):
        args = {
            "url": [],
            "post": {},
            "files": self.request.files
        }
        if not servicos.existe(name):
            self.write("Esse serviço não existe!")
        else:
            servico = servicos.getServico(name)
            for nome in servico.requires["post"]:
                args["post"][nome] = unquote(self.get_body_argument(nome))
            check = servicos.checkArgs(name, args)
            if check == True:
                # DEBUG
                debug_log("POST: " + name + " " + str(args["post"]))
                self.write(servico.post(args))
            else:
                self.write(check)

if __name__ == "__main__":
    print("Servidor rodando ao vivo na porta " + str(port) + "!")
    print("(" + str(len(servicos.todos())) + " serviços carregados!)")
    tornado.web.Application([
        (r"/", PaginaBase),
        (r"/status/?", Status),
        (r"/get/([^/]+)/(.*)/?", Getters),
        (r"/post/([^/]+)/?", Posters)
    ]).listen(port)
    tornado.ioloop.IOLoop.current().start()