# programado por bruno borges paschoalinoto ou borginhos ou bruno02468

import mysql.connector
import json
from hashlib import sha512
import datetime
import string
import random
import os
import shutil
from brutils import str_simplify
import brutils
import sys

uploadsFolder = "../publico/uploads/"

def make_salt(size=16, chars=string.ascii_uppercase + string.digits):
    return "".join(random.choice(chars) for _ in range(size))

def agora():
    return str(datetime.datetime.now())

def hashstr(string):
    obj = sha512(string.encode("UTF-8"))
    return obj.hexdigest()

def random_hash():
    return hashstr(make_salt())

def ano_atual():
    return datetime.datetime.now().year

class Banco():
    config = None

    def __init__(self):
        try:
            with open("config_private.json") as json_data:
                self.config = json.load(json_data)
        except FileNotFoundError:
            print("Você esqueceu de criar o config_private.py!")
            print("Leia o SETUP.txt caso tenha dúvidas!")
            sys.exit(0)

    def getConfig(self):
        return self.config

    def getEmailTemplate(self, name):
        with open("email_templates/" + name + ".html") as content:
            return content.read()
        return "ERRO AO ABRIR EMAIL TEMPLATE \"%s\"" % name

    def makeConnection(self):
        return mysql.connector.connect(**self.config["banco"], use_unicode=True,
            charset="utf8")

    def getRows(self, query, params):
        connection = self.makeConnection()
        connection.set_charset_collation("utf8mb4")
        cursor = connection.cursor()
        cursor.execute("SET NAMES utf8mb4;")
        cursor.execute("SET CHARACTER SET utf8mb4;")
        cursor.execute("SET character_set_connection=utf8mb4;")
        cursor.execute("SET character_set_client=utf8mb4;")
        cursor.execute("SET character_set_results=utf8mb4;")
        cursor.execute(query, params)
        rows = []
        for x in cursor:
            rows.append(x)
        cursor.close()
        connection.close()
        return rows

    def getRow(self, query, params):
        rows = self.getRows(query, params)
        if len(rows) > 0:
            return rows[0]
        else:
            return None

    def simpleSelect(self, table, columns, postquery="", params={}):
        if len(postquery) > 0:
            postquery = " " + postquery
        query = ("SELECT "+ ", ".join(columns) + " FROM " + table
            + postquery + ";")
        rows = self.getRows(query, params)
        arr = []
        for row in rows:
            obj = {}
            i = 0;
            for column in columns:
                val = row[i]
                t = str(type(val))
                if "datetime" in t:
                    val = val.isoformat()
                if isinstance(val, set):
                    val = list(val)
                obj[column] = val
                i += 1
            arr.append(obj)
        return arr

    def singleSelect(self, table, columns, postquery="", params={}):
        objs = self.simpleSelect(table, columns, postquery, params)
        if len(objs) > 0:
            return objs[0]
        else:
            return None

    def doChange(self, query, params):
        connection = self.makeConnection()
        connection.set_charset_collation("utf8mb4")
        cursor = connection.cursor()
        cursor.execute("SET NAMES utf8mb4;")
        cursor.execute("SET CHARACTER SET utf8mb4;")
        cursor.execute("SET character_set_connection=utf8mb4;")
        cursor.execute("SET character_set_client=utf8mb4;")
        cursor.execute("SET character_set_results=utf8mb4;")
        cursor.execute(query, params)
        connection.commit()
        cursor.close()
        connection.close()

    def simpleInsert(self, table, obj):
        columns = list(obj.keys())
        cols = ", ".join(columns)
        vals = ", ".join(["%(" + k + ")s" for k in columns])
        query = ("INSERT INTO " + table + " (" + cols + ") VALUES (" + vals
            + ");")
        self.doChange(query, obj)

    def simpleUpdate(self, table, obj, postquery="", params={}):
        columns = list(obj.keys())
        sets = ", ".join([col + "=%(" + col + ")s" for col in columns])
        newp = obj.copy()
        newp.update(params)
        if len(postquery) > 0:
            postquery = " " + postquery
        query = "UPDATE " + table + " SET " + sets + postquery + ";"
        self.doChange(query, newp)

    def simpleDelete(self, table, postquery, params):
        query = "DELETE FROM " + table + " " + postquery
        self.doChange(query, params)

    def simpleCount(self, table, columns, postquery, params):
        results = self.simpleSelect(table, columns, postquery, params)
        return len(results)

    def isValidToken(self, token):
        query = ("SELECT matricula FROM aluno_tokens WHERE "
            "token=%(token)s AND valid_until >= NOW() AND YEAR(valid_until - "
            "INTERVAL 365 DAY)=YEAR(CURDATE());")
        params = {
            "token": token
        }
        result = self.getRows(query, params)
        if len(result) > 0:
            return result[0][0]
        else:
            return False

    def isValidAluno(self, matricula, senha):
        query = ("SELECT ano, sala, chamada, opaque, salt FROM alunos WHERE "
            "banido=FALSE AND matricula=%(mat)s AND em=YEAR(CURDATE());")
        params = {
            "mat": matricula
        }
        rows = self.getRows(query, params)
        for row in rows:
            ano, sala, chamada, opaque, salt = row
            if opaque == None and salt == None:
                senhaideal = str(ano) + str(sala) + str(chamada)
                return senha.upper() == senhaideal.upper()
            else:
                hashed = hashstr(senha + salt)
                return opaque == hashed
        return False

    def makeToken(self, matricula):
        token = random_hash()
        query = ("INSERT INTO aluno_tokens (matricula, token, valid_until)"
            "VALUES (%(matricula)s, %(token)s, NOW() + INTERVAL 365 DAY);")
        params = {
            "matricula": matricula,
            "token": token
        }
        self.doChange(query, params)
        return token

    def hasPassword(self, matricula):
        query = ("SELECT opaque, salt FROM alunos WHERE matricula=%(mat)s "
                 "AND em=" + str(datetime.datetime.now().year))
        params = {
            "mat" : matricula
        }
        rows = self.getRows(query, params)
        for row in rows:
            return row[0] != None and row[1] != None
        return False

    def registerAluno(self, matricula, oldpass, password, email):
        salt = make_salt();
        opaque = hashstr(password + salt)
        ano = oldpass[0]
        sala = oldpass[1]
        chamada = oldpass[2:]
        table = "alunos"
        obj = {
            "opaque": opaque,
            "salt": salt,
            "email": email
        }
        postquery = ("WHERE matricula=%(matricula)s AND ano=%(ano)s "
            "AND sala=%(sala)s AND chamada=%(chamada)s")
        params = {
            "matricula": matricula,
            "ano": ano,
            "sala": sala,
            "chamada": chamada,
        }
        self.simpleUpdate(table, obj, postquery, params)

    def getAulas(self, ano):
        table = "aulas"
        columns = ["id_aula", "nome"]
        postquery = "WHERE FIND_IN_SET(%(ano)s, anos) > 0"
        params = {
            "ano": ano
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getAllAulas(self):
        table = "aulas"
        columns = ["id_aula", "nome", "anos", "id_materia"]
        return self.simpleSelect(table, columns)

    def getAlunosBy(self, varname, value, em=None):
        if not em:
            em = datetime.datetime.now().year
        k = " AND em=%(em)s"
        if varname == "matricula" or varname == "id_aluno":
            k = ""
        table = "alunos"
        columns = ["id_aluno", "matricula", "nome", "ano", "sala", "banido",
            "autor", "opaque", "salt", "email", "chamada", "correcao_nome"]
        postquery = "WHERE " + varname + "=%(value)s" + k
        params = {
            "value": value,
            "em": em
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getAlunoBy(self, varname, value, em=None):
        if not em:
            em = datetime.datetime.now().year
        alunos = self.getAlunosBy(varname, value, em)
        if len(alunos) > 0:
            return alunos[0]
        else:
            return None

    def getAlunoById(self, id_aluno):
        table = "alunos"
        columns = ["id_aluno", "matricula", "nome", "ano", "sala", "banido",
            "autor", "opaque", "salt", "email", "chamada", "em",
            "correcao_nome"]
        postquery = "WHERE id_aluno=%(i)s"
        params = {
            "i": id_aluno
        }
        return self.singleSelect(table, columns, postquery, params)

    def getAlunoByMatricula(self, matricula, em=None):
        if not em:
            em = datetime.datetime.now().year
        table = "alunos"
        columns = ["id_aluno", "matricula", "nome", "ano", "sala", "banido",
            "autor", "opaque", "salt", "email", "chamada", "em",
            "correcao_nome"]
        postquery = "WHERE matricula=%(i)s AND em=" + str(em)
        params = {
            "i": matricula
        }
        return self.singleSelect(table, columns, postquery, params)

    def adicionarLicao(self, id_aluno, id_aula, para, info):
        aluno = self.getAlunoById(id_aluno)
        licao = {
            "id_aluno": id_aluno,
            "passada": datetime.datetime.now(),
            "para": para,
            "id_aula": id_aula,
            "info": info,
            "ano": aluno["ano"],
            "sala": aluno["sala"]
        }
        self.simpleInsert("licoes", licao)

    def getLicoesSala(self, ano, sala):
        table = "licoes"
        columns = ["id_licao", "id_aluno", "passada", "para", "id_aula", "info",
            "removida"]
        postquery = ("WHERE ano=%(ano)s AND sala=%(sala)s AND para >= CURDATE()"
            " AND removida=FALSE")
        params = {
            "ano": ano,
            "sala": sala
        }
        licoes = self.simpleSelect(table, columns, postquery, params)
        for index in range(0, len(licoes)):
            autor = self.getAlunoById(licoes[index]["id_aluno"])
            licoes[index]["nome_autor"] = autor["nome"]
            licoes[index]["chamada_autor"] = autor["chamada"]
        return licoes

    def getLicaoById(self, id_licao):
        table = "licoes"
        columns = ["id_licao", "id_aluno", "passada", "para", "id_aula", "info",
            "removida"]
        postquery = "WHERE id_licao=%(id_licao)s"
        params = {
            "id_licao": id_licao
        }
        return self.singleSelect(table, columns, postquery, params)

    def updateLicao(self, id_licao, id_aula, para, info):
        table = "licoes"
        obj = {
            "id_aula": id_aula,
            "para": para,
            "info": info,
        }
        postquery = "WHERE id_licao=%(id_licao)s"
        params = {
            "id_licao": id_licao
        }
        self.simpleUpdate(table, obj, postquery, params)

    def removeLicao(self, id_licao):
        table = "licoes"
        obj = {
            "removida": 1
        }
        postquery = "WHERE id_licao=%(id_licao)s"
        params = {
            "id_licao": id_licao
        }
        self.simpleUpdate(table, obj, postquery, params)

    def getReportsByLicao(self, id_licao):
        table = "reports_licoes"
        columns = ["id_report", "id_aluno"]
        postquery = "WHERE id_licao=%(id_licao)s"
        params = {
            "id_licao": id_licao
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getReportsByAutor(self, id_aluno):
        reports = self.simpleSelect("reports_licoes", ["id_licao"])
        count = 0
        for report in reports:
            licao = self.getLicaoById(report["id_licao"])
            if licao["id_aluno"] == id_aluno:
                count += 1
        return count

    def banAluno(self, id_aluno):
        query = ("UPDATE alunos SET banido=TRUE WHERE id_aluno=%(id_aluno)s;")
        params = {
            "id_aluno": id_aluno
        }
        self.doChange(query, params)
        return True

    def toggleBanido(self, matricula):
        gtable = "alunos"
        gcolumns = ["banido"]
        gpostquery = "WHERE matricula=%(mat)s"
        gparams = {
            "mat": matricula
        }
        sel = self.simpleSelect(gtable, gcolumns, gpostquery, gparams)
        banido = sel["banido"]
        if row != None:
            newBanido = not banido
            setQuery = ("UPDATE alunos SET banido=%(b)s WHERE matricula=%(m)s;")
            setParams = {
                "b": newBanido,
                "m": matricula
            }
            self.doChange(setQuery, setParams)
            return newBanido
        return None

    def reportLicao(self, id_reporter, id_licao):
        licao = self.getLicaoById(id_licao)
        id_autor = licao["id_aluno"]
        autor = self.getAlunoById(id_autor)
        reporter = self.getAlunoById(id_reporter)

        if reporter["banido"]:
            return {
                "status": "FAILED",
                "message": "Você está banido, lembra?"
            }

        if (autor["ano"] + autor["sala"]) != (reporter["ano"] + reporter["sala"]):
            return {
                "status": "FAILED",
                "message": "Você só pode denunciar lições da sua sala!"
            }

        if licao["id_aluno"] == id_reporter:
            return {
                "status": "FAILED",
                "message": "Você realmente quer denunciar a própria lição?!"
            }

        reports = self.getReportsByLicao(id_licao)
        for report in reports:
            if report["id_aluno"] == id_reporter:
                return {
                    "status": "FAILED",
                    "message": "Você já denunciou essa lição!"
                }

        query = ("INSERT INTO reports_licoes (id_aluno, id_licao) "
            " VALUES (%(id_aluno)s, %(id_licao)s);")
        params = {
            "id_aluno": id_reporter,
            "id_licao": id_licao
        }
        self.doChange(query, params)

        reports = self.getReportsByLicao(id_licao)
        message = "Essa lição tem agora " + str(len(reports)) + " denúncia(s)!"

        if (len(reports) >= 10):
            message = "Acabamos de remover essa lição, pois ela foi denunciada dez vezes!"
            self.removeLicao(id_licao)

        autorReports = self.getReportsByAutor(id_autor)
        if autorReports >= 30:
            self.banAluno(id_autor)
            message = "O autor dessa lição foi alvo de 30 denúncias, então foi banido!"

        return {
            "status": "OK",
            "message": (message + "<br>Obrigado por ajudar a manter o site organizado, "
                + reporter["nome"].split(" ")[0] + "!")
        }

    def criarLinkEsqueci(self, id_aluno):
        token = random_hash()[:32]
        query = ("INSERT INTO esqueci_links (id_aluno, url_token, valid_until)"
            " VALUES (%(id_aluno)s, %(token)s, NOW() + INTERVAL 1 DAY);")
        params = {
            "id_aluno": id_aluno,
            "token": token
        }
        self.doChange(query, params)
        return token

    def getValidRecoverToken(self, recover_token):
        table = "esqueci_links"
        columns = ["id_link", "id_aluno"]
        postquery = ("WHERE url_token=%(t)s AND valid_until >= NOW() "
            "AND used=FALSE")
        params = {
            "t": recover_token
        }
        return self.singleSelect(table, columns, postquery, params)

    def hasActiveRecovery(self, id_aluno):
        table = "esqueci_links"
        columns = ["id_link"]
        postquery = ("WHERE id_aluno=%(id)s AND valid_until >= NOW() "
            "AND used=FALSE")
        params = {
            "id": id_aluno
        }
        return bool(self.singleSelect(table, columns, postquery, params))

    def alterarSenha(self, id_aluno, newpass, id_link):
        newsalt = make_salt()
        opaque = hashstr(newpass + newsalt)
        alunoquery = ("UPDATE alunos SET opaque=%(opaque)s, salt=%(salt)s WHERE "
            "id_aluno=%(id)s;")
        alunoparams = {
            "opaque": opaque,
            "salt": newsalt,
            "id": id_aluno
        }
        tokenquery = ("UPDATE esqueci_links SET used=TRUE WHERE id_link=%(i)s;")
        tokenparams = {
            "i": id_link
        }
        loginsquery = ("DELETE FROM aluno_tokens WHERE matricula=%(matricula)s AND "
            "valid_until >= NOW();")
        loginsparams = {
            "matricula": self.getAlunoById(id_aluno)["matricula"]
        }
        self.doChange(alunoquery, alunoparams)
        self.doChange(tokenquery, tokenparams)
        self.doChange(loginsquery, loginsparams)

    def isAdmin(self, user, senha):
        admins = self.getConfig()["admins"]
        if user in admins:
            if admins[user] == senha:
                return True
        return False

    def iniciarVotacao(self, periodo, etapa, prim, ult, dias, neo,
                       responsavel_neo):
        vot = {
            "periodo": periodo,
            "etapa": etapa,
            "inicio": prim,
            "fim": ult,
            "dias": dias,
            "neo": neo,
            "responsavel_neo": responsavel_neo,
        }
        self.simpleInsert("votacoes", vot)

    def getVotacoesBasic(self):
        query = ("SELECT id_votacao, fim, periodo, etapa FROM votacoes"
            " WHERE inicio <= CURDATE();")
        params = {}
        rows = self.getRows(query, params)
        votacoes = []
        for row in rows:
            votacoes.append({
                "id_votacao": row[0],
                "fim": row[1].isoformat(),
                "periodo": row[2],
                "etapa": row[3],
                "acabou": row[1] < datetime.date.today()
            })
        return votacoes

    def jaTerminou(self, id_votacao):
        query = ("SELECT fim, periodo, etapa FROM votacoes"
            " WHERE fim >= CURDATE() AND id_votacao=%(id)s;")
        params = {
            "id": id_votacao
        }
        rows = self.getRows(query, params)
        return len(rows) == 0

    def getVotos(self, id_calendario):
        query = ("SELECT id_voto FROM votos WHERE id_calendario=%(id)s;")
        params = {
            "id": id_calendario
        }
        return len(self.getRows(query, params))

    def getVotacaoFull(self, id_votacao, ano):
        votacao = {
            "calendarios": [],
            "acabou": self.jaTerminou(id_votacao),
            "totalalunos": len(self.getAlunosBy("ano", ano)),
            "totalvotos": 0
        }
        votQuery = ("SELECT periodo, etapa, dias, neo, responsavel_neo FROM "
                    "votacoes WHERE id_votacao=%(idvot)s;")
        votParams = {
            "idvot": id_votacao
        }
        votRow = self.getRow(votQuery, votParams)
        if votRow == None:
            return None
        votacao["periodo"] = votRow[0]
        votacao["etapa"] = votRow[1]
        votacao["dias"] = json.loads(votRow[2])
        votacao["neo"] = bool(votRow[3])
        votacao["responsavel_neo"] = votRow[4]

        calQuery = ("SELECT id_calendario, id_autor, calendario, enviado_em, "
                    "neo_ano FROM calendarios WHERE id_votacao=%(idvot)s;")
        calRows = self.getRows(calQuery, votParams)

        for calRow in calRows:
            autor = self.getAlunoById(calRow[1]);
            if calRow[4] == "0":
                if autor["ano"] != ano:
                    continue
            else:
                if calRow[4] != ano:
                    continue
            calendario = {
                "id_calendario": calRow[0],
                "id_autor": calRow[1],
                "calendario": calRow[2],
                "enviado_em": calRow[3].isoformat()
            }
            votos = self.getVotos(calendario["id_calendario"])
            votacao["totalvotos"] += votos
            if votacao["acabou"]:
                calendario["nome_autor"] = autor["nome"]
                calendario["ano_autor"] = autor["ano"]
                calendario["sala_autor"] = autor["sala"]
                calendario["votos"] = votos

            votacao["calendarios"].append(calendario)
        return votacao

    def getMaterias(self):
        table = "materias"
        columns = ["id_materia", "nome", "tipo", "curta", "artes", "anos"]
        return self.simpleSelect(table, columns)

    def getMateriasByAno(self, ano):
        table = "materias"
        columns = ["id_materia", "nome", "tipo", "curta", "artes", "anos"]
        postquery = "WHERE FIND_IN_SET(%(ano)s, anos) > 0"
        params = {
            "ano": ano
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getCalendarios(self, id_votacao, ano):
        table = "calendarios"
        columns = ["id_calendario", "id_autor", "calendario", "enviado_em", "neo_ano"]
        postquery = "WHERE id_votacao=%(id)s"
        params = {
            "id": id_votacao
        }
        precalendarios = self.simpleSelect(table, columns, postquery, params)
        calendarios = []
        for index in range(0, len(precalendarios)):
            cal = precalendarios[index]
            if cal["neo_ano"] == "0":
                autor = self.getAlunoById(cal["id_autor"])
                if str(autor["ano"]) == str(ano):
                    cal["calendario"] = json.loads(cal["calendario"])
                    calendarios.append(cal)
            else:
                if str(ano) == str(cal["neo_ano"]):
                    cal["calendario"] = json.loads(cal["calendario"])
                    calendarios.append(cal)
        return calendarios

    def getSpecificCalendario(self, id_votacao, id_autor):
        table = "calendarios"
        columns = ["id_calendario"]
        postquery = "WHERE id_votacao=%(vot)s AND id_autor=%(aut)s"
        params = {
            "vot": id_votacao,
            "aut": id_autor
        }
        return self.singleSelect(table, columns, postquery, params)

    def getDuplicata(self, id_votacao, ano, suspeito):
        suspeito = json.dumps(suspeito)
        calendarios = self.getCalendarios(id_votacao, ano)
        for calendario in calendarios:
            alvo_json = json.dumps(calendario["calendario"])
            if suspeito == alvo_json:
                return (True, calendario["id_calendario"])
        return (False, None)

    def adicionarCalendario(self, id_votacao, id_autor, calendario, neo_ano):
        cal = {
            "id_autor": id_autor,
            "calendario": json.dumps(calendario),
            "id_votacao": id_votacao,
            "enviado_em": datetime.datetime.now(),
            "neo_ano": neo_ano
        }
        self.simpleInsert("calendarios", cal)
        return self.getSpecificCalendario(id_votacao, id_autor)

    def getMeuVoto(self, id_votacao, id_aluno):
        query = ("SELECT id_calendario FROM votos WHERE id_votacao=%(vot)s "
            "AND id_aluno=%(alu)s;")
        params = {
            "vot": id_votacao,
            "alu": id_aluno
        }
        row = self.getRow(query, params)
        if row == None:
            return None
        else:
            return row[0]

    def setVoto(self, id_votacao, id_aluno, id_calendario):
        votei = self.getMeuVoto(id_votacao, id_aluno)
        if votei == None:
            query = ("INSERT INTO votos (id_aluno, id_calendario, id_votacao) "
                "VALUES (%(alu)s, %(cal)s, %(vot)s);")
        else:
            query = ("UPDATE votos SET id_calendario=%(cal)s WHERE "
                "id_aluno=%(alu)s AND id_votacao=%(vot)s;")
        params = {
            "alu": id_aluno,
            "cal": id_calendario,
            "vot": id_votacao
        }
        self.doChange(query, params)

    def addProfessor(self, nome, senha, id_materia):
        salt = make_salt()
        prof = {
            "nome": nome,
            "id_materia": id_materia,
            "salt": salt,
            "opaque": hashstr(senha + salt)
        }
        self.simpleInsert("professores", prof)

    def getProfessor(self, id_professor):
        table = "professores"
        columns = ["nome", "id_materia", "salt", "opaque"]
        postquery = "WHERE id_professor=%(id)s"
        params = {
            "id": id_professor
        }
        return self.singleSelect(table, columns, postquery, params)

    def getProfessores(self):
        table = "professores"
        columns = ["id_professor", "nome", "id_materia"]
        return self.simpleSelect(table, columns)

    def editProfessor(self, id_professor, nome, senha, id_materia):
        professor = self.getProfessor(id_professor)
        if professor == None:
            return False
        if not nome:
            nome = professor["nome"]
        if not senha:
            salt = professor["salt"]
            opaque = professor["opaque"]
        else:
            salt = make_salt()
            opaque = hashstr(senha + salt)
        if not id_materia:
            id_materia = professor["id_materia"]

        table = "professores"
        obj = {
            "id_materia": id_materia,
            "nomeome": nome,
            "salt": salt,
            "opaque": opaque
        }
        postquery = "WHERE id_professor=%(id)s"
        params = {
            "id_professor": id_professor
        }
        self.simpleUpdate(table, obj, postquery, params)

    def removerProfessor(self, id_professor):
        query = "DELETE FROM professores WHERE id_professor=%(id)s;"
        params = {
            "id": id_professor
        }
        self.doChange(query, params)

    def isProfessor(self, nome, senha):
        query = ("SELECT salt, opaque, id_professor FROM professores WHERE "
            "nome=%(nome)s;")
        params = {
            "nome": nome
        }
        rows = self.getRows(query, params)
        for row in rows:
            salt = row[0]
            opaque = row[1]
            id_professor = row[2]
            if hashstr(senha + salt) == opaque:
                return id_professor
        return None

    def getArquivoByHash(self, hash):
        table = "uploads"
        columns = ["id_arquivo", "filename", "id_enviou", "aluno"]
        postquery = "WHERE hash=%(h)s AND removido=0"
        params = {
            "h": hash
        }
        return self.singleSelect(table, columns, postquery, params)

    def getArquivoByID(self, id_arquivo):
        table = "uploads"
        columns = ["id_arquivo", "filename", "id_enviou", "hash", "aluno"]
        postquery = "WHERE id_arquivo=%(i)s AND removido=0"
        params = {
            "i": id_arquivo
        }
        return self.singleSelect(table, columns, postquery, params)

    def addArquivo(self, filename, contents, id_enviou, is_aluno):
        hash = random_hash()[:32]
        upl = {
            "filename": filename,
            "hash": hash,
            "id_enviou": id_enviou,
            "aluno": bool(is_aluno)
        }
        path = uploadsFolder + hash
        os.mkdir(path)
        path += "/" + filename
        file = open(path, "wb")
        file.write(contents)
        file.close()
        self.simpleInsert("uploads", upl)
        arq = self.getArquivoByHash(hash)
        return arq["id_arquivo"]

    def deleteArquivo(self, hash):
        query = "DELETE FROM uploads WHERE hash=%(h)s;"
        params = {
            "h": hash
        }
        self.doChange(query, params)
        shutil.rmtree(uploadsFolder + hash)

    def criarAviso(self, id_professor, titulo, texto, id_arquivo, ano):
        aviso = {
            "id_professor": id_professor,
            "titulo": titulo,
            "texto": texto,
            "id_arquivo": id_arquivo,
            "ano": ano
        }
        self.simpleInsert("avisos", aviso)

    def getMeusAvisos(self, id_professor):
        table = "avisos"
        columns = ["titulo", "texto", "id_arquivo", "id_aviso", "ano"]
        postquery = "WHERE id_professor=%(i)s"
        params = {
            "i": id_professor
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getAvisoById(self, id_aviso):
        table = "avisos"
        columns = ["titulo", "texto", "id_arquivo", "id_aviso", "ano", "id_professor"]
        postquery = "WHERE id_aviso=%(id_aviso)s"
        params = {
            "id_aviso": id_aviso
        }
        return self.singleSelect(table, columns, postquery, params)

    def editAviso(self, id_aviso, titulo, texto, ano):
        table = "avisos"
        obj = {
            "titulo": titulo,
            "texto": texto,
            "ano": ano,
        }
        query = "WHERE id_aviso=%(id_aviso)s"
        params = {
            "id_aviso": id_aviso
        }
        self.simpleUpdate(table, obj, query, params)

    def deleteAviso(self, id_aviso):
        query = "DELETE FROM avisos WHERE id_aviso=%(i)s;"
        params = {
            "i": id_aviso
        }
        self.doChange(query, params)

    def getAvisosByAno(self, ano):
        table = "avisos"
        columns = ["id_aviso", "titulo", "texto", "id_arquivo", "id_professor",
            "ano"]
        postquery = "WHERE ano=%(a)s OR ano='0' ORDER BY id_aviso DESC"
        params = {
            "a": ano
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getAllAvisos(self):
        table = "avisos"
        columns = ["id_aviso", "titulo", "texto", "id_arquivo", "id_professor",
            "ano"]
        postquery = "ORDER BY id_aviso DESC"
        return self.simpleSelect(table, columns, postquery)

    def getResumosSemTexto(self):
        table = "resumos"
        columns = ["id_resumo", "nome_autor", "id_materia", "miniurl", "ano",
            "removido", "enviado_em", "periodo", "etapa", "assunto", "id_autor"]
        postquery = "WHERE removido=0"
        return self.simpleSelect(table, columns, postquery)

    def getResumosSemTextoByMateria(self, id_materia):
        table = "resumos"
        columns = ["id_resumo", "nome_autor", "id_materia", "miniurl", "ano",
            "removido", "enviado_em", "periodo", "etapa", "assunto", "id_autor"]
        postquery = "WHERE id_materia=%(mat)s AND removido=0"
        params = {
            "mat": id_materia
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getResumosSemTextoOld(self, id_materia, ano):
        table = "resumos"
        columns = ["id_resumo", "nome_autor", "id_materia", "miniurl", "ano",
            "removido", "enviado_em", "periodo", "etapa", "assunto", "id_autor"]
        postquery = "WHERE id_materia=%(mat)s AND removido=0 AND ano=%(ano)s"
        params = {
            "mat": id_materia,
            "ano": ano
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getResumoByMini(self, mini):
        table = "resumos"
        columns = ["id_resumo", "nome_autor", "id_materia", "miniurl", "ano",
            "removido", "enviado_em", "periodo", "etapa", "assunto",
            "id_autor", "texto"]
        postquery = "WHERE miniurl=%(mini)s"
        params = {
            "mini": mini.lower()
        }
        resumo = self.singleSelect(table, columns, postquery, params)
        return resumo

    def addResumo(self, autor, id_materia, mini, texto, ano, etapa, periodo,
        assunto):
        resumo = {
            "nome_autor": autor["nome"],
            "id_materia": id_materia,
            "miniurl": mini.lower(),
            "texto": texto,
            "etapa": etapa,
            "periodo": periodo,
            "id_autor": autor["id_aluno"],
            "assunto": assunto,
            "ano": ano,
            "removido": 0
        }
        self.simpleInsert("resumos", resumo)

    def updateResumo(self, id_materia, mini, texto, ano, etapa, periodo,
        assunto):
        query = ("UPDATE resumos SET id_materia=%(mat)s, texto=%(txt)s, "
            "etapa=%(e)s, periodo=%(p)s, ano=%(ano)s, assunto=%(as)s "
            "WHERE miniurl=%(mini)s;")
        params = {
            "mat": id_materia,
            "mini": mini,
            "txt": texto,
            "e": etapa,
            "p": periodo,
            "as": assunto,
            "ano": ano
        }
        self.doChange(query, params)

    def getLogin(self, token):
        matricula = self.isValidToken(token)
        if not matricula:
            return None
        else:
            return self.getAlunoByMatricula(matricula)

    def toggleRemoverResumo(self, resumo):
        mini = resumo["miniurl"]
        novoRemovido = not bool(resumo["removido"])
        query = ("UPDATE resumos SET removido=%(r)s WHERE miniurl=%(m)s;")
        params = {
            "r": novoRemovido,
            "m": mini
        }
        self.doChange(query, params)
        return novoRemovido

    def listResumosByAutor(self, nome):
        table = "resumos"
        columns = ["id_materia", "assunto", "ano", "etapa", "periodo",
            "miniurl", "removido"]
        postquery = "WHERE nome_autor=%(n)s"
        params = {
            "n": nome
        }
        return self.simpleSelect(table, columns, postquery, params)


    def addReforco(self, hora_inicio, hora_fim, sala, ano, nomemateria,
        professor, dias_array):
        ref = {
            "hora_inicio": hora_inicio,
            "hora_fim": hora_fim,
            "sala": sala,
            "ano": ano,
            "nomemateria": nomemateria,
            "professor": professor,
            "dias_array": dias_array
        }
        self.simpleInsert("reforcos", ref)

    def editReforco(self, id_reforco, hora_inicio, hora_fim, sala, ano,
        nomemateria, professor, dias_array):
        query = ("UPDATE reforcos SET hora_inicio=%(i)s, hora_fim=%(f)s, "
            "sala=%(s)s, ano=%(a)s, nomemateria=%(n)s, professor=%(p)s, "
            "dias_array=%(d)s WHERE id_reforco=%(id)s;")
        params = {
            "i": hora_inicio,
            "f": hora_fim,
            "s": sala,
            "a": ano,
            "n": nomemateria,
            "p": professor,
            "d": dias_array,
            "id": id_reforco
        }
        self.doChange(query, params)

    def removeReforco(self, id_reforco):
        query = ("DELETE FROM reforcos WHERE id_reforco=%(id)s;")
        params = {
            "id": id_reforco
        }
        self.doChange(query, params)

    def getReforcos(self):
        table = "reforcos"
        columns = ["id_reforco", "hora_inicio", "hora_fim", "sala", "ano",
            "nomemateria", "professor", "dias_array"]
        return self.simpleSelect(table, columns)

    def getHorarioBySala(self, ano, sala):
        table = "horarios"
        columns = ["horario", "ultimo_editor", "lastedit"]
        postquery = "WHERE ano=%(a)s AND sala=%(s)s"
        params = {
            "a": ano,
            "s": sala
        }
        return self.singleSelect(table, columns, postquery, params)

    def setHorario(self, aluno, arr):
        curr = self.getHorarioBySala(aluno["ano"], aluno["sala"])
        if curr:
            query = ("UPDATE horarios SET horario=%(h)s, ultimo_editor=%(u)s,"
                " lastedit=NOW() WHERE ano=%(a)s AND sala=%(s)s;")
        else:
            query = ("INSERT INTO horarios (ano, sala, horario, "
                "ultimo_editor, lastedit) VALUES (%(a)s, %(s)s, "
                "%(h)s, %(u)s, NOW());")
        params = {
            "a": aluno["ano"],
            "s": aluno["sala"],
            "h": arr,
            "u": aluno["id_aluno"],
        }
        self.doChange(query, params)

    def getFeitas(self, id_aluno):
        table = "feitas"
        columns = ["id_feita", "feitas"]
        postquery = "WHERE id_aluno=%(i)s"
        params = {
            "i": id_aluno
        }
        return self.singleSelect(table, columns, postquery, params)

    def setFeitas(self, id_aluno, arr):
        curr = self.getFeitas(id_aluno)
        if curr:
            query = ("UPDATE feitas SET feitas=%(f)s WHERE id_aluno=%(i)s;")
        else:
            query = ("INSERT INTO feitas (id_aluno, feitas) "
                "VALUES (%(i)s, %(f)s);")
        params = {
            "f": arr,
            "i": id_aluno
        }
        self.doChange(query, params)

    def getMsgSala(self, ano, sala):
        table = "sala_msgs"
        self.simpleDelete(table, "WHERE YEAR(lastedit)!=YEAR(CURDATE())", {})
        columns = ["id_msg", "texto", "ultimo_editor", "lastedit"]
        postquery = "WHERE ano=%(a)s AND sala=%(s)s"
        params = {
            "a": ano,
            "s": sala
        }
        return self.singleSelect(table, columns, postquery, params)

    def setMsgSala(self, aluno, texto):
        self.simpleDelete("sala_msgs", "WHERE YEAR(lastedit)!=YEAR(CURDATE())", {})
        curr = self.getMsgSala(aluno["ano"], aluno["sala"])
        if curr:
            query = ("UPDATE sala_msgs SET texto=%(t)s, ultimo_editor=%(u)s,"
                " lastedit=NOW() WHERE ano=%(a)s AND sala=%(s)s;")
        else:
            query = ("INSERT INTO sala_msgs (ano, sala, texto, "
                "ultimo_editor, lastedit) VALUES (%(a)s, %(s)s, "
                "%(t)s, %(u)s, NOW());")
        params = {
            "a": aluno["ano"],
            "s": aluno["sala"],
            "t": texto,
            "u": aluno["id_aluno"],
        }
        self.doChange(query, params)

    def pesquisarLicoes(self, **args):
        table = "licoes"
        columns = ["id_licao", "id_aluno", "passada", "para", "id_aula", "info"]
        postquery = "WHERE ano=%(ano)s AND removida=FALSE"

        if args["sala"] != "null":
            postquery += " AND sala=%(sala)s"
        if args["aula"] != "null":
            postquery += " AND id_aula=%(aula)s"
        if args["envio_start"] != "null" and args["envio_end"] != "null":
            postquery += " AND (passada BETWEEN %(envio_start)s AND %(envio_end)s)"
        if args["entrega_start"] != "null" and args["entrega_end"] != "null":
            postquery += " AND (para BETWEEN %(entrega_start)s AND %(entrega_end)s)"

        postquery += " ORDER BY id_licao DESC;"
        return self.simpleSelect(table, columns, postquery, args)

    def getCursos(self):
        table = "cursos"
        columns = ["id_curso", "nome"]
        return self.simpleSelect(table, columns)

    def getLicoesAtivasCursos(self):
        table = "licoes_cursos"
        columns = ["id_licao", "id_aluno", "id_curso", "passada", "para", "info",
            "removida"]
        postquery = "WHERE para >= CURDATE() AND removida=FALSE"
        licoes = self.simpleSelect(table, columns, postquery)
        for index in range(0, len(licoes)):
            autor = self.getAlunoById(licoes[index]["id_aluno"])
            licoes[index]["nome_autor"] = autor["nome"]
            licoes[index]["sala_autor"] = autor["sala"]
            licoes[index]["chamada_autor"] = autor["chamada"]
        return licoes

    def adicionarLicaoCurso(self, id_curso, id_aluno, para, info):
        query = ("INSERT INTO licoes_cursos (id_curso, id_aluno, passada, "
            "para, info, removida) VALUES (%(c)s, %(a)s, CURDATE(), %(p)s, "
            "%(i)s, FALSE);")
        params = {
            "c": id_curso,
            "a": id_aluno,
            "p": para,
            "i": info
        }
        self.doChange(query, params)

    def editarLicaoCurso(self, id_licao, id_curso, id_aluno, para, info):
        table = "licoes_cursos"
        obj = {
            "id_curso": id_curso,
            "para": para,
            "info": info
        }
        postquery = "WHERE id_licao=%(id_licao)s AND id_aluno=%(id_aluno)s"
        params = {
            "id_aluno": id_aluno,
            "id_licao": id_licao
        }
        self.simpleUpdate(table, obj, postquery, params)

    def removerLicaoCurso(self, id_licao, id_aluno):
        table = "licoes_cursos"
        obj = {
            "removida": 1
        }
        postquery = "WHERE id_licao=%(id_licao)s AND id_aluno=%(id_aluno)s"
        params = {
            "id_licao": id_licao,
            "id_aluno": id_aluno
        }
        self.simpleUpdate(table, obj, postquery, params)

    def getMateriasProva(self):
        table = "materias_prova"
        columns = ["id_materia_prova", "id_materia", "etapa", "periodo", "info",
            "ultimo_editor"]
        materias_prova = self.simpleSelect(table, columns)
        for index in range(0, len(materias_prova)):
            editor = self.getAlunoById(materias_prova[index]["ultimo_editor"])
            materias_prova[index]["nome_ultimo_editor"] = editor["nome"]
            materias_prova[index]["sala_ultimo_editor"] = editor["sala"]
            materias_prova[index]["chamada_ultimo_editor"] = editor["chamada"]
            materias_prova[index]["ano"] = editor["ano"]
        return materias_prova

    def getSpecificMateriaProva(self, id_materia_prova):
        table = "materias_prova"
        columns = ["id_materia_prova", "id_materia", "etapa", "periodo", "info",
            "ultimo_editor"]
        postquery = "WHERE id_materia_prova=%(i)s"
        params = {
            "i": id_materia_prova
        }
        mat = self.singleSelect(table, columns, postquery, params)
        editor = self.getAlunoById(mat["ultimo_editor"])
        mat["nome_ultimo_editor"] = editor["nome"]
        mat["sala_ultimo_editor"] = editor["sala"]
        mat["chamada_ultimo_editor"] = editor["chamada"]
        mat["ano"] = editor["ano"]
        return mat

    def setMateriaProva(self, id_materia, etapa, periodo, info, id_aluno, ano):
        table = "materias_prova"
        columns = ["id_materia_prova"]
        postquery = ("WHERE id_materia=%(id_materia)s AND etapa=%(etapa)s AND "
            "periodo=%(periodo)s AND ano=%(ano)s")
        obj = {
            "ano": ano,
            "id_materia": id_materia,
            "etapa": etapa,
            "periodo": periodo,
            "info": info,
            "ultimo_editor": id_aluno
        }
        params = {
            "ano": ano,
            "id_materia": id_materia,
            "etapa": etapa,
            "periodo": periodo
        }
        found = self.singleSelect(table, columns, postquery, params)
        if found:
            self.simpleUpdate(table, obj, postquery, obj)
        else:
            self.simpleInsert(table, obj)

    def queryAlunos(self, qstr):
        table = "alunos"
        columns = ["nome", "matricula"]
        postquery = "WHERE ano='1' AND em=" + str(datetime.datetime.now().year)
        todosalunos = self.simpleSelect(table, columns, postquery)
        alunos = []
        for aluno in todosalunos:
            if qstr in str_simplify(aluno["nome"]):
                alunos.append(aluno)
        return alunos

    def getSalasProvaPeriodo(self):
        table = "salas_prova"
        columns = ["periodo"]
        pares = self.simpleSelect(table, columns)
        return [dict(tupleized) for tupleized in set(tuple(item.items()) for item in pares)]

    def getAllSalasProva(self, periodo):
        table = "salas_prova"
        columns = ["id_sala_prova", "fileiras", "posicao_porta", "lousa_acima",
            "numsala", "periodo"]
        postquery = "WHERE periodo=%(p)s"
        params = {
            "p": periodo
        }
        return self.simpleSelect(table, columns, postquery, params)

    def getMinhaSalaProva(self, periodo, ano, letra, chamada):
        salas = self.getAllSalasProva(periodo)
        for sala in salas:
            fileiras = json.loads(sala["fileiras"])
            for fileira in fileiras:
                if str(fileira["ano"]) == str(ano):
                    for lugar in fileira["lugares"]:
                        if not lugar:
                            continue
                        if (int(lugar["chamada"]) == int(chamada)
                            and str(lugar["sala"]) == str(letra)):
                            return sala["id_sala_prova"]
        return None

    def getSalaProva(self, id_sala_prova):
        table = "salas_prova"
        columns = ["id_sala_prova", "fileiras", "posicao_porta", "lousa_acima",
            "numsala", "periodo"]
        postquery = "WHERE id_sala_prova=%(i)s"
        params = {
            "i": id_sala_prova
        }
        return self.singleSelect(table, columns, postquery, params)

    def getCurrentMOTD(self):
        table = "motd"
        columns = ["texto"]
        postquery = "ORDER BY id_motd DESC"
        params = {}
        obj = self.singleSelect(table, columns, postquery, params)
        if obj:
            return obj["texto"]
        else:
            return ""

    def setMOTD(self, motd, user):
        table = "motd"
        obj = {
            "set_by": user,
            "texto": motd,
            "enviado_em": datetime.datetime.now()
        }
        self.simpleInsert(table, obj)

    def getSeminarios(self):
        table = "seminarios"
        columns = ["id_seminario", "id_aluno", "tema", "link", "id_arquivo"]
        postquery = "WHERE removido=0"
        params = {}
        seminarios = self.simpleSelect(table, columns, postquery, params)
        for index, seminario in enumerate(seminarios):
            aluno = self.getAlunoById(seminario["id_aluno"])
            seminarios[index]["sala"] = aluno["sala"]
            seminarios[index]["nome_autor"] = aluno["nome"]
            if seminario["id_arquivo"]:
                arquivo = self.getArquivoByID(seminario["id_arquivo"])
                seminarios[index]["arquivo"] = arquivo
        return seminarios

    def criarSeminario(self, id_aluno, tema, link, id_arquivo):
        table = "seminarios"
        obj = {
            "id_aluno": id_aluno,
            "tema": tema,
            "link": link,
            "id_arquivo": id_arquivo
        }
        self.simpleInsert(table, obj)

    def removerSeminario(self, id_seminario):
        table = "seminarios"
        obj = {
            "removido": 1
        }
        postquery = "WHERE id_seminario=%(i)s"
        params = {
            "i": id_seminario
        }
        self.simpleUpdate(table, obj, postquery, params)

    def getIniciais(self):
        table = "iniciais"
        columns = ["id_link", "link", "visivel", "nome"]
        postquery = ""
        params = {}
        return self.simpleSelect(table, columns, postquery, params)

    def setInicial(self, id_link, val):
        table = "iniciais"
        obj = {
            "visivel": bool(val)
        }
        postquery = "WHERE id_link=%(i)s"
        params = {
            "i": id_link
        }
        self.simpleUpdate(table, obj, postquery, params)

    def getAprovacoesAluno(self, id_aluno):
        table = "aprovacoes"
        columns = ["id_aprovacao", "id_aluno", "nome_uni", "nome_curso",
                   "publica", "rank", "treineiro", "meio_de_ano", "sisu",
                   "categoria"]
        postquery = "WHERE id_aluno=%(i)s AND ano_diretor=%(ad)s"
        params = {
            "i": id_aluno,
            "ad": brutils.get_ano_diretor()
        }
        return self.simpleSelect(table, columns, postquery, params)

    def addAprovacao(self, id_aluno, nome_uni, nome_curso,
                     publica, rank, treineiro, meio_de_ano, sisu,
                     categoria):
        table = "aprovacoes"
        obj = {
            "id_aluno": id_aluno,
            "nome_uni": nome_uni,
            "nome_curso": nome_curso,
            "publica": publica,
            "rank": rank,
            "treineiro": treineiro,
            "meio_de_ano": meio_de_ano,
            "sisu": sisu,
            "categoria": categoria,
            "ano_diretor": brutils.get_ano_diretor()
        }
        self.simpleInsert(table, obj)

    def editAprovacao(self, id_aprovacao, nome_uni, nome_curso,
                      publica, rank, treineiro, meio_de_ano, sisu,
                      categoria):
        table = "aprovacoes"
        obj = {
            "nome_uni": nome_uni,
            "nome_curso": nome_curso,
            "publica": publica,
            "rank": rank,
            "treineiro": treineiro,
            "meio_de_ano": meio_de_ano,
            "sisu": sisu,
            "categoria": categoria
        }
        postquery = "WHERE id_aprovacao=%(i)s AND ano_diretor=%(ad)s"
        params = {
            "i": id_aprovacao,
            "ad": brutils.get_ano_diretor()
        }
        self.simpleUpdate(table, obj, postquery, params)

    def removerAprovacao(self, id_aprovacao):
        table = "aprovacoes"
        postquery = "WHERE id_aprovacao=%(i)s"
        params = {
            "i": id_aprovacao
        }
        self.simpleDelete(table, postquery, params)

    def aprovacaoPertence(self, id_aprovacao, id_aluno):
        aprovacoes = self.getAprovacoesAluno(id_aluno)
        for aprovacao in aprovacoes:
            if str(aprovacao["id_aprovacao"]) == str(id_aprovacao):
                return True
        return False

    def getAllAprovacoes(self):
        table = "aprovacoes"
        postquery = "WHERE ano_diretor=%(ad)s"
        params = {
            "ad": brutils.get_ano_diretor()
        }
        columns = ["id_aprovacao", "id_aluno", "nome_uni", "nome_curso",
                   "publica", "rank", "treineiro", "meio_de_ano", "sisu",
                   "categoria", "ano_diretor"]

        return self.simpleSelect(table, columns)

    def getRelatorioAprovacoes(self):
        aprovacoes = self.getAllAprovacoes()
        publicas = 0
        privadas = 0
        meio_de_ano = 0
        sisu = 0
        aprovados_vdd = set()
        aprovados_treineiros = set()
        porano_sets = {
            "1": set(),
            "2": set(),
            "3": set()
        }
        for aprovacao in aprovacoes:
            if aprovacao["publica"]:
                publicas += 1
            else:
                privadas += 1
            if aprovacao["treineiro"]:
                aprovados_treineiros.add(aprovacao["id_aluno"])
            else:
                aprovados_vdd.add(aprovacao["id_aluno"])
            if aprovacao["meio_de_ano"]:
                meio_de_ano += 1
            if aprovacao["sisu"]:
                sisu += 1
            aluno = self.getAlunoById(aprovacao["id_aluno"])
            porano_sets[str(aluno["ano"])].add(aprovacao["id_aluno"])

        porano = {
            "1": len(porano_sets["1"]),
            "2": len(porano_sets["2"]),
            "3": len(porano_sets["3"]),
        }

        return {
            "total_aprovacoes": len(aprovacoes),
            "aprovacoes_publicas": publicas,
            "aprovacoes_privadas": privadas,
            "aprovacoes_mda": meio_de_ano,
            "aprovacoes_via_sisu": sisu,
            "aprovados_pra_valer": len(aprovados_vdd),
            "aprovados_treineiros": len(aprovados_treineiros),
            "aprovados_por_ano": porano,
            "total_aprovados": len(aprovados_vdd | aprovados_treineiros)
        }

    def getAprovacoesPorAluno(self):
        aprovacoes = self.getAllAprovacoes()
        alunos = []
        for aprovacao in aprovacoes:
            id_aluno = aprovacao["id_aluno"]
            aluno = self.getAlunoById(id_aluno)

            found = False
            for can in alunos:
                if can["id_aluno"] == id_aluno:
                    found = True
                    break
            if not found:
                aluno["aprovacoes"] = []
                alunos.append(aluno)

            for index, can in enumerate(alunos):
                if can["id_aluno"] == id_aluno:
                    alunos[index]["aprovacoes"].append(aprovacao)

        alunos.sort(key=lambda a: a["nome"])

        return alunos

    def verificarAlunoAprov(self, matricula, em, ano, sala, chamada):
        table = "alunos"
        columns = ["id_aluno", "nome", "ano", "sala", "chamada", "em",
                   "correcao_nome"]
        postquery = ("WHERE matricula=%(m)s AND ano=%(a)s AND sala=%(s)s AND "
                     "chamada=%(c)s AND em=%(e)s")
        params = {
            "a": ano,
            "s": sala,
            "m": matricula,
            "c": chamada,
            "e": em
        }
        lista = self.simpleSelect(table, columns, postquery, params)
        if len(lista) == 1:
            return lista[0]
        else:
            return None

    def verificarCredenciaisJSON(self, texto):
        obj = json.loads(texto)
        matricula = obj["matricula"]
        em = obj["em"]
        full_sala = obj["full_sala"]
        chamada = obj["chamada"]
        if len(full_sala) != 2:
            return None
        else:
            ano = full_sala[0]
            sala = full_sala[1]
        return self.verificarAlunoAprov(matricula, em, ano, sala, chamada)

    def setCorrecaoNome(self, id_aluno, correcao):
        table = "alunos"
        obj = {
            "correcao_nome": correcao
        }
        postquery = "WHERE id_aluno=%(i)s"
        params = {
            "i": id_aluno
        }
        self.simpleUpdate(table, obj, postquery, params)

    def matarTodosTokens(self, id_aluno):
        aluno = self.getAlunoById(id_aluno)
        table = "aluno_tokens";
        postquery = "WHERE matricula=%(m)s"
        params = {
            "m": aluno["matricula"]
        }
        self.simpleDelete(table, postquery, params)

    def resetarAluno(self, id_aluno):
        table = "alunos"
        obj = {
            "salt": None,
            "opaque": None,
            "email": None,
            "banido": 0
        }
        postquery = "WHERE id_aluno=%(i)s"
        params = {
            "i": id_aluno
        }
        self.simpleUpdate(table, obj, postquery, params)
        self.matarTodosTokens(id_aluno)

    def contarLicoesAluno(self, id_aluno):
        columns = ["id_licao"]
        postquery = "WHERE id_aluno=%(i)s"
        params = {
            "i": id_aluno
        }
        normais = len(self.simpleSelect("licoes", columns, postquery, params))
        curso = len(self.simpleSelect("licoes_cursos", columns, postquery, params))
        return normais + curso

    def contarResumosAluno(self, id_aluno):
        resumos = self.getResumosSemTexto()
        nome_autor = self.getAlunoById(id_aluno)["nome"]
        total = 0
        for resumo in resumos:
            if resumo["nome_autor"] == nome_autor:
                total += 1
        return total

    def pushCrashToDatabase(self, crash_report, emailed):
        table = "crash_reports"
        obj = {
            "crash_report": crash_report,
            "emailed": emailed,
            "received": datetime.datetime.now()
        }
        self.simpleInsert(table, obj)

    def existemCrashesSimilares(self, crash_message):
        table = "crash_reports"
        columns = ["crash_id", "crash_report", "received"]
        postquery = "WHERE DATE(received) = DATE(NOW())"
        params = {
            "m": crash_message
        }
        todays_errors = self.simpleSelect(table, columns, postquery, params)
        for error in todays_errors:
            message = json.loads(error["crash_report"])["Classe do erro"]
            if message == crash_message:
                return True
        return False

    def getRegexes(self):
        table = "regexes"
        columns = ["id_regex", "nome", "detalhes", "pattern"]
        postquery = "ORDER BY nome"
        params = {}
        return self.simpleSelect(table, columns, postquery, params)

    def addRegex(self, nome, detalhes, pattern):
        table = "regexes"
        obj = {
            "nome": nome,
            "detalhes": detalhes,
            "pattern": pattern
        }
        self.simpleInsert(table, obj)

    def editRegex(self, id_regex, nome, detalhes, pattern):
        table = "regexes"
        obj = {
            "nome": nome,
            "detalhes": detalhes,
            "pattern": pattern
        }
        postquery = "WHERE id_regex=%(i)s"
        params = {
            "i": id_regex
        }
        self.simpleUpdate(table, obj, postquery, params)

    def removeRegex(self, id_regex):
        table = "regexes"
        postquery = "WHERE id_regex=%(i)s"
        params = {
            "i": id_regex
        }
        self.simpleDelete(table, postquery, params)

    def alunosParaExtrair(self):
        table = "alunos"
        columns = ["nome", "correcao_nome", "matricula", "em", "ano", "sala"]
        postquery = "ORDER BY nome"
        params = {}
        return self.simpleSelect(table, columns, postquery, params)

    def addSalaProva(self, mapa):
        table = "salas_prova"
        self.simpleInsert(table, mapa)

    def editSalaProva(self, id_mapa, mapa):
        table = "salas_prova"
        postquery = "WHERE id_sala_prova=%(i)s"
        params = {
            "i": id_mapa
        }
        self.simpleUpdate(table, mapa, postquery, params)

    def removeSalaProva(self, id_mapa):
        table = "salas_prova"
        postquery = "WHERE id_sala_prova=%(i)s"
        params = {
            "i": id_mapa
        }
        self.simpleDelete(table, postquery, params)
