Mendelyx é o codinome do projeto que unifica as funcionalidades que eu
disponibilizava no licoes.com entre 2015 e 2016.

Os serviços disponibilizados por meio de várias páginas, bases de código e
bancos incompatíveis entre si eram:

 · Anotação de lições de casa (por alunos selecionados)
 · Resumos (postados por alunos selecionados)
 · Horários de reforço (gerados por um script e postado no site de lições)
 · Lugares de prova (digitados manualmente)
 · Votação para calendários de prova (100% controlado pelos alunos)

O objetivo da base de código Mendelyx é unir todas essas funcionalidades num só
reposítório do Git, agora privado, pelo uso do Bitbucket.

Através de um sistema unificado para estilização, persistência de dados e
controle de contas de usuário, o sistema não vai exigir minha presença na
escola em tempo integral para manter o projeto funcionando adequadamente.

Este projeto, que será auto-administrado (pelos alunos da escola), permitirá
que os serviços que muitos alunos apreciam imensamente continuem por tempo
indefinido, indepententemente de eu estar na escola ou não.

Pretendo oferecer o seguinte:

 · Anotação de lições de casa (por quaisquer alunos da sala)
 · Resumos (postados por quaisquer alunos, sujeitos a aprovação, com melhorias)
 · Horários de reforço (sem a gambiarra do site de lições)
 · Lugares de prova (sem ter que digitar manualmente)
 · Votação para calendários de prova (com melhorias e avanços)
 · Geração de horários de aula (matemágica!)

O lançamento está planejado para a mudança 2016-2017, nas férias, evitando
mudanças bruscas para os usuários durante o período de aulas.

O projeto é mantido por:

 · Bruno "Borginhos" Borges Paschoalinoto (administração geral e programação)


README.txt redigido no dia oito de agosto de 2016 pelo Bruno.