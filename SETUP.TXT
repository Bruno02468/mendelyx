Como configurar o Mendelyx:

Requisitos:
    · Servidor web (recomenda-se Apache);
    · Python 3 e pip;
    · Postfix ou outro servidor SMTP capaz de envio na porta 9993;
    · Servidor MySQL.

Instalação:
    1) Instale os módulos do pip:

       "sudo pip install tornado"
       "sudo pip install mysql-connector"

       Certifique-se de estar usando o pip do Python 3.

    2) Crie um usuário e um esquema de banco de dados no qual esse usuário
       tenha total permissão no seu servidor MySQL.

    3) Crie uma cópia do arquivo de configurações:

       "cp privado/config.json privado/config_private.json"

       E nele entre com o endereço do servidor MySQL, o nome de usuário,
       a senha, o nome do esquema de banco, e a URL onde ficará a raiz das
       páginas do cliente web (pública, será usada em emais).

    4) Rode os seguintes arquivos .sql como esse usuário, nessa ordem:
           · privado/sql/setup_tables.sql
           · privado/sql/materias.sql
           · privado/sql/aulas.sql
           · privado/sql/resumos.sql

    5) Crie a pasta de logs na raiz do repositório: "mkdir logs".
       Também crie a pasta de uploads: "mkdir public/uploads".

    6) Crie uma ProxyPass no seu servidor, redirecionando a pasta
       "/mendelyx_serv" para a porta 9994 do seu servidor.

    7) Inicie um servidor SMTP de saída (preferencialmente Exim) na porta 9993.

    8) O cliente web fica na pasta "publico". Crie um link simbólico ou
       simplesmente mude a DocumentRoot do seu servidor para essa pasta, para
       permitir o acesso pelos usuários.

    9) Faça uso dos programas UNIX Shell na raiz do repositório para iniciar,
       fechar ou reiniciar o programa da API. Os nomes são descritivos.
