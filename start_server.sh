#!/bin/sh

cd privado/
FILENAME="../logs/stdout_$(date +%F_%T).log"
./site.py >$FILENAME &> $FILENAME &
cd ../
disown $!
echo "PID: $!"
echo "$!" > "server.pid"
echo "logfile: logs/stdout_$(date +%F_%T).log"
